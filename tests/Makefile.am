# This file is part of libtrilateration
#
# Copyright 2014 by Marcel Kyas
#
# libtrilateration is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# libtrilateration is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libtrilateration.  If not, see
# <http://www.gnu.org/licenses/>.
#

TESTS = check_combinations check_select check_sort \
	check_collinear check_distance check_circle check_disc \
	check_aml check_aml_3d check_bilateration check_bilateration_3d \
	check_centroid check_centroid_3d check_clurol check_clurol_3d \
	check_const check_const_3d check_cpf check_cpf_3d \
        check_eminmax_w2 check_eminmax_w4 check_geo_3 check_geo_n \
	check_icla check_lls check_lls_3d check_lms check_md_minmax \
	check_md_minmax_abs \
	check_minmax check_mle_gamma check_mle_gauss check_nlls check_nlls_3d \
	check_rlsm check_rlsm_3d check_rwgh check_rwgh_3d \
	check_trilateration check_trilateration_3d \
        check_weighted_centroid check_weighted_lls

check_PROGRAMS = check_combinations check_select check_sort \
	check_collinear check_distance check_circle check_disc \
	check_aml check_aml_3d check_bilateration check_bilateration_3d \
        check_centroid check_centroid_3d check_clurol check_clurol_3d \
	check_const check_const_3d check_cpf check_cpf_3d \
        check_eminmax_w2 check_eminmax_w4 check_geo_3 check_geo_n \
	check_icla check_lls check_lls_3d check_lms check_md_minmax \
	check_md_minmax_abs \
	check_minmax check_mle_gamma check_mle_gauss check_nlls check_nlls_3d \
	check_rlsm check_rlsm_3d check_rwgh check_rwgh_3d \
	check_trilateration check_trilateration_3d \
        check_weighted_centroid check_weighted_lls

check_collinear_SOURCES = check_collinear.c
	$(top_srcdir)/src/libtrilateration/geometry.h
check_collinear_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_collinear_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_distance_SOURCES = check_distance.c \
	$(top_srcdir)/src/libtrilateration/geometry.h
check_distance_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_distance_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_circle_SOURCES = check_circle.c \
	$(top_srcdir)/src/libtrilateration/geometry.h
check_circle_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_circle_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_combinations_SOURCES = check_combinations.c
check_combinations_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_combinations_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_disc_SOURCES = check_disc.c \
	$(top_srcdir)/src/libtrilateration/geometry.h
check_disc_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_disc_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_select_SOURCES = check_select.c
check_select_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_select_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_sort_SOURCES = check_sort.c
check_sort_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_sort_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_aml_SOURCES = check_aml.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_aml_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_aml_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_aml_3d_SOURCES = check_aml_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_aml_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_aml_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_bilateration_SOURCES = check_bilateration.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_bilateration_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_bilateration_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_bilateration_3d_SOURCES = check_bilateration_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_bilateration_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_bilateration_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_centroid_SOURCES = check_centroid.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_centroid_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_centroid_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_centroid_3d_SOURCES = check_centroid_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_centroid_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_centroid_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_clurol_SOURCES = check_clurol.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_clurol_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_clurol_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_clurol_3d_SOURCES = check_clurol_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_clurol_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_clurol_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_const_SOURCES = check_const.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_const_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_const_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_const_3d_SOURCES = check_const_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_const_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_const_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_cpf_SOURCES = check_cpf.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_cpf_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_cpf_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_cpf_3d_SOURCES = check_cpf.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_cpf_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_cpf_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_eminmax_w2_SOURCES = check_eminmax_w2.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_eminmax_w2_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_eminmax_w2_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_eminmax_w4_SOURCES = check_eminmax_w4.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_eminmax_w4_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_eminmax_w4_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_geo_3_SOURCES = check_geo_3.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_geo_3_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_geo_3_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_geo_n_SOURCES = check_geo_n.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_geo_n_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_geo_n_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_icla_SOURCES = check_icla.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_icla_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_icla_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_lls_SOURCES = check_lls.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_lls_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_lls_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_lls_3d_SOURCES = check_lls_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_lls_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_lls_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_lms_SOURCES = check_lms.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_lms_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_lms_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_md_minmax_SOURCES = check_md_minmax.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_md_minmax_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_md_minmax_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_md_minmax_abs_SOURCES = check_md_minmax_abs.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_md_minmax_abs_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_md_minmax_abs_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_minmax_SOURCES = check_minmax.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_minmax_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_minmax_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_mle_gamma_SOURCES = check_mle_gamma.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_mle_gamma_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_mle_gamma_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_mle_gauss_SOURCES = check_mle_gauss.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_mle_gauss_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_mle_gauss_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_nlls_SOURCES = check_nlls.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_nlls_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_nlls_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_nlls_3d_SOURCES = check_nlls_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_nlls_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_nlls_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_rlsm_SOURCES = check_rlsm.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_rlsm_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_rlsm_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_rlsm_3d_SOURCES = check_rlsm_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_rlsm_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_rlsm_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_rwgh_SOURCES = check_rwgh.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_rwgh_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_rwgh_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_rwgh_3d_SOURCES = check_rwgh_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_rwgh_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_rwgh_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_trilateration_SOURCES = check_trilateration.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_trilateration_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_trilateration_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_trilateration_3d_SOURCES = check_trilateration_3d.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_trilateration_3d_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_trilateration_3d_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_weighted_centroid_SOURCES = check_weighted_centroid.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_weighted_centroid_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_weighted_centroid_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@

check_weighted_lls_SOURCES = check_weighted_lls.c check_data.h \
	$(top_srcdir)/src/libtrilateration/algorithms.h
check_weighted_lls_CFLAGS = -Wall -Werror @CHECK_CFLAGS@ -I$(top_srcdir)/src
check_weighted_lls_LDADD = $(top_builddir)/src/libtrilateration.la @CHECK_LIBS@
