/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>

#include <check.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>

#include "check_data.h"

START_TEST(test_collinear_not)
{

  int r = trilateration_collinear_p(noncollinear_anchors, 3);

  ck_assert(r == 0);
}
END_TEST

START_TEST(test_collinear_not_alt)
{

  int r = trilateration_collinear_p(alt_noncollinear_anchors, 3);

  ck_assert(r == 0);
}
END_TEST

START_TEST(test_collinear_horizontal)
{
  int r = trilateration_collinear_p(collinear_horizontal_anchors, 3);

  ck_assert(r != 0);
}
END_TEST

START_TEST(test_collinear_vertical)
{
  int r = trilateration_collinear_p(collinear_vertical_anchors, 3);

  ck_assert(r != 0);
}
END_TEST

START_TEST(test_collinear_diagonal)
{
  int r = trilateration_collinear_p(collinear_diagonal_anchors, 3);

  ck_assert(r != 0);
}
END_TEST

Suite *
collinear_suite(void)
{
  Suite *s = suite_create("Collinearity");
  TCase *tc = tcase_create("not collinear 1");
  tcase_add_test(tc, test_collinear_not);
  suite_add_tcase(s, tc);
  tc = tcase_create("not collinear 2");
  tcase_add_test(tc, test_collinear_not_alt);
  suite_add_tcase(s, tc);
  tc = tcase_create("horizontal");
  tcase_add_test(tc, test_collinear_horizontal);
  suite_add_tcase(s, tc);
  tc = tcase_create("vertical");
  tcase_add_test(tc, test_collinear_vertical);
  suite_add_tcase(s, tc);
  tc = tcase_create("diagonal");
  tcase_add_test(tc, test_collinear_diagonal);
  suite_add_tcase(s, tc);
  return s;
}

int
main(void)
{
  int number_failed;
  Suite *s = collinear_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
