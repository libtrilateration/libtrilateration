/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <check.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>

#include "check_data.h"

#define ck_assert_eq_double(x, y)                                       \
     ck_assert_msg(fabs((x) - (y)) < 1e-5, "Expexted %s == %g, but it is %g", #x, (y), (x));

static const tl_point3d p1[1] = { { 1.0, 1.0, 0.0 } };
static const tl_point3d p2[1] = { { 2.0, 2.0, 0.0 } };
static const tl_point3d p3[1] = { { 1.0, 4.0, 0.0 } };
static const tl_point3d p4[1] = { { 1.0, 3.0, 0.0 } };
static const tl_point3d p5[1] = { { 0.0, 0.0, 0.0 } };
static const tl_point3d p6[1] = { { 2.0, 0.0, 0.0 } };


START_TEST(test_circle_disjoint)
{
     tl_point3d res[2];
     int r;
     r = trilateration_circle_intersections(res, p1, 0.5, p2, 0.5);
     ck_assert(r == 0);
}
END_TEST


START_TEST(test_circle_enclosed)
{
     tl_point3d res[2];
     int r;
     r = trilateration_circle_intersections(res, p1, 0.25, p2, 2.0);
     ck_assert(r == 0);
}
END_TEST

START_TEST(test_circle_touching_outer)
{
     tl_point3d res[2];
     int r;
     r = trilateration_circle_intersections(res, p1, 1.0, p3, 2.0);
     ck_assert(r == 1);
     ck_assert_eq_2d(1.0, 2.0, res[0]);
}
END_TEST

START_TEST(test_circle_touching_inner)
{
     tl_point3d res[2];
     int r;
     r = trilateration_circle_intersections(res, p4, 1.0, p3, 2.0);
     ck_assert(r == 1);
     ck_assert_eq_2d(1.0, 2.0, res[0]);
}
END_TEST

START_TEST(test_circle_intersecting_outer)
{
     tl_point3d res[2];
     int r;
     r = trilateration_circle_intersections(res, p5, M_SQRT2, p6, M_SQRT2);
     ck_assert(r == 2);
     ck_assert_eq_2d(1.0, 1.0, res[0]);
     ck_assert_eq_2d(1.0, -1.0, res[1]);
}
END_TEST

START_TEST(test_circle_intersecting_inner)
{
     tl_point3d res[2];
     int r;
     r = trilateration_circle_intersections(res, p5, 2.0, p6, 1.0);
     ck_assert(r == 2);
     ck_assert_eq_2d(1.75, 0.968246, res[0]);
     ck_assert_eq_2d(1.75, -0.968246, res[1]);
}
END_TEST

static const tl_point3d pa[] = {
     { 0.0, 0.0, 0.0 },
     { 2.0, 0.0, 0.0 },
     { 1.0, 0.0, 0.0 }
};

static const double ra[] = { 2.0, 1.0, 1.5 };

START_TEST(test_circles_intersections_two)
{
     tl_point3d res[2];
     int r;
     r = trilateration_circles_intersections(res, 2, pa, ra, 2);
     ck_assert(r == 2);
     ck_assert_eq_2d(1.75, 0.968246, res[0]);
     ck_assert_eq_2d(1.75, -0.968246, res[1]);
}
END_TEST

START_TEST(test_circles_intersections_three)
{
     tl_point3d res[6];
     int r;
     r = trilateration_circles_intersections(res, 6, pa, ra, 3);
     ck_assert(r == 6);
     ck_assert_eq_2d(1.75, 0.968246, res[0]);
     ck_assert_eq_2d(1.75, -0.968246, res[1]);
     ck_assert_eq_2d(1.375, 1.45237, res[2]);
     ck_assert_eq_2d(1.375, -1.45237, res[3]);
     ck_assert_eq_2d(2.125, -0.992157, res[4]);
     ck_assert_eq_2d(2.125, 0.992157, res[5]);
}
END_TEST

Suite *
circle_suite(void)
{
     Suite *s = suite_create("Circle");
     TCase * tc;
     tc = tcase_create("disjoint");
     tcase_add_test(tc, test_circle_disjoint);
     suite_add_tcase(s, tc);

     tc = tcase_create("enclosed");
     tcase_add_test(tc, test_circle_enclosed);
     suite_add_tcase(s, tc);

     tc = tcase_create("touching outer");
     tcase_add_test(tc, test_circle_touching_outer);
     suite_add_tcase(s, tc);

     tc = tcase_create("touching inner");
     tcase_add_test(tc, test_circle_touching_inner);
     suite_add_tcase(s, tc);

     tc = tcase_create("intersect outer");
     tcase_add_test(tc, test_circle_intersecting_outer);
     suite_add_tcase(s, tc);

     tc = tcase_create("intersect inner");
     tcase_add_test(tc, test_circle_intersecting_inner);
     suite_add_tcase(s, tc);

     tc = tcase_create("intersections two");
     tcase_add_test(tc, test_circles_intersections_two);
     suite_add_tcase(s, tc);

     tc = tcase_create("intersections three");
     tcase_add_test(tc, test_circles_intersections_three);
     suite_add_tcase(s, tc);

     return s;
}

int
main(void)
{
     int number_failed;
     Suite *s = circle_suite();
     SRunner *sr = srunner_create(s);
     srunner_run_all (sr, CK_NORMAL);
     number_failed = srunner_ntests_failed(sr);
     srunner_free(sr);
     return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
