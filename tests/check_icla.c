/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <check.h>
#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>

#include "check_data.h"

START_TEST(test_icla)
{
     tl_point3d result[1];

     int r = trilateration_icla_2d(result, noncollinear_anchors,
                                   noncollinear_distances, 3, NULL);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(1.5, 1.5, result[0]);
}
END_TEST

START_TEST(test_icla_same)
{
     tl_point3d result[1];

     int r = trilateration_icla_2d(result, alt_noncollinear_anchors,
                                   alt_noncollinear_distances, 3, NULL);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(1.5, 1.5, result[0]);
}
END_TEST

START_TEST(test_icla_all_same)
{
     tl_point3d result[1];

     int r = trilateration_icla_2d(result, collinear_vertical_anchors,
                                   collinear_vertical_distances, 3, NULL);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(1.0, 2.0, result[0]);
}
END_TEST

START_TEST(test_icla_colinear)
{
     tl_point3d result[1];

     int r = trilateration_icla_2d(result, collinear_diagonal_anchors,
                                   collinear_diagonal_distances, 3, NULL);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(1.8, 2.2, result[0]);
}
END_TEST

START_TEST(test_icla_ls2)
{
     tl_point3d result[1];

     trilateration_icla_params params = { .alpha = 1.5,.move_step = 2.0 };

     int r = trilateration_icla_2d(result, ls2_anchors, ls2_distances, 8,
                                   &params);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(313.42643, 683.77547, result[0]);
}
END_TEST

Suite *
trilateration_suite(void)
{
     Suite *s = suite_create("ICLA");
     TCase *tc_icla = tcase_create("ICLA");
     tcase_add_test(tc_icla, test_icla);
     suite_add_tcase(s, tc_icla);
     tc_icla = tcase_create("ICLA (same x)");
     tcase_add_test(tc_icla, test_icla_same);
     suite_add_tcase(s, tc_icla);
     tc_icla = tcase_create("ICLA (all same)");
     tcase_add_test(tc_icla, test_icla_all_same);
     suite_add_tcase(s, tc_icla);
     tc_icla = tcase_create("ICLA (colinear)");
     tcase_add_test(tc_icla, test_icla_colinear);
     suite_add_tcase(s, tc_icla);
     tc_icla = tcase_create("ICLA (LS2 test)");
     tcase_add_test(tc_icla, test_icla_ls2);
     suite_add_tcase(s, tc_icla);
     return s;
}

int
main(void)
{
     int number_failed;
     Suite *s = trilateration_suite();
     SRunner *sr = srunner_create(s);
     srunner_run_all (sr, CK_NORMAL);
     number_failed = srunner_ntests_failed(sr);
     srunner_free(sr);
     return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
