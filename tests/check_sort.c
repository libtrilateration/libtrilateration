/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>

#include <check.h>

#include <libtrilateration/util.h>

START_TEST(test_sort_one)
{
  double a[] = { 5.0, 2.0, 4.0, 1.0, 3.0 };
  trilateration_fsort(a, 5);
  ck_assert(a[0] == 1.0);
  ck_assert(a[1] == 2.0);
  ck_assert(a[2] == 3.0);
  ck_assert(a[3] == 4.0);
  ck_assert(a[4] == 5.0);
}
END_TEST

Suite *
collinear_suite(void)
{
  Suite *s = suite_create("Sort");
  TCase *tc;
  tc = tcase_create("one");
  tcase_add_test(tc, test_sort_one);
  suite_add_tcase(s, tc);
  return s;
}

int
main(void)
{
  int number_failed;
  Suite *s = collinear_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
