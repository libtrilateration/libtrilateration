/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>

#include <check.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>


#define ck_assert_eq_double(x, y) \
  ck_assert_msg(fabs((x) - (y)) < 1e-5, "Expexted %s == %g, but it is %g", #x, (y), (x));

START_TEST(test_distance_squared_zero)
{
  double d = trilateration_distance_squared(1.0, 1.0, 1.0, 1.0);
  ck_assert_eq_double(d, 0.0);
}
END_TEST

START_TEST(test_distance_squared_half)
{
  double d = trilateration_distance_squared(1.0, 1.0, 1.5, 1.0);
  ck_assert_eq_double(d, 0.25);
}
END_TEST

START_TEST(test_distance_squared_one)
{
  double d = trilateration_distance_squared(1.0, 1.0, 2.0, 1.0);
  ck_assert_eq_double(d, 1.0);
}
END_TEST

START_TEST(test_distance_squared_two)
{
  double d = trilateration_distance_squared(1.0, 1.0, 1.0, 3.0);
  ck_assert_eq_double(d, 4.0);
}
END_TEST

START_TEST(test_distance_zero)
{
  double d = trilateration_distance(1.0, 1.0, 1.0, 1.0);
  ck_assert_eq_double(d, 0.0);
}
END_TEST

START_TEST(test_distance_half)
{
  double d = trilateration_distance(1.0, 1.0, 1.5, 1.0);
  ck_assert_eq_double(d, 0.5);
}
END_TEST

START_TEST(test_distance_one)
{
  double d = trilateration_distance(1.0, 1.0, 1.0, 2.0);
  ck_assert_eq_double(d, 1.0);
}
END_TEST

START_TEST(test_distance_two)
{
  double d = trilateration_distance(1.0, 1.0, 1.0, 3.0);
  ck_assert_eq_double(d, 2.0);
}
END_TEST

Suite *
distance_suite(void)
{
  Suite *s = suite_create("Distance");
  TCase * tc;
  tc = tcase_create("squared zero");
  tcase_add_test(tc, test_distance_squared_zero);
  suite_add_tcase(s, tc);
  tc = tcase_create("squared half");
  tcase_add_test(tc, test_distance_squared_half);
  suite_add_tcase(s, tc);
  tc = tcase_create("squared one");
  tcase_add_test(tc, test_distance_squared_one);
  suite_add_tcase(s, tc);
  tc = tcase_create("squared two");
  tcase_add_test(tc, test_distance_squared_two);
  suite_add_tcase(s, tc);

  tc = tcase_create("zero");
  tcase_add_test(tc, test_distance_zero);
  suite_add_tcase(s, tc);
  tc = tcase_create("half");
  tcase_add_test(tc, test_distance_half);
  suite_add_tcase(s, tc);
  tc = tcase_create("one");
  tcase_add_test(tc, test_distance_one);
  suite_add_tcase(s, tc);
  tc = tcase_create("two");
  tcase_add_test(tc, test_distance_two);
  suite_add_tcase(s, tc);
  return s;
}

int
main(void)
{
  int number_failed;
  Suite *s = distance_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
