/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <float.h>
#include <math.h>

#include <check.h>
#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>

#include "check_data.h"

START_TEST(test_rwgh)
{
  tl_point3d result[1];

  int r = trilateration_rwgh_2d(result, noncollinear_anchors,
                                noncollinear_distances, 3, NULL);

  ck_assert(r == TL_OK);
  ck_assert_eq_2d(2.0, 2.0, result[0]);
}
END_TEST

START_TEST(test_rwgh_same)
{
  tl_point3d result[1];

  int r = trilateration_rwgh_2d(result, alt_noncollinear_anchors,
                                alt_noncollinear_distances, 3, NULL);

  ck_assert(r == TL_OK);
  ck_assert_eq_2d(2.0, 2.0, result[0]);
}
END_TEST

START_TEST(test_rwgh_all_same)
{
  tl_point3d result[1];

  int r = trilateration_rwgh_2d(result, collinear_vertical_anchors,
                                collinear_vertical_distances, 3, NULL);

  ck_assert(r == TL_OK);
  ck_assert(isnan(RX) && isnan(RY));
}
END_TEST

START_TEST(test_rwgh_colinear)
{
  tl_point3d result[1];

  int r = trilateration_rwgh_2d(result, collinear_diagonal_anchors,
                                collinear_vertical_distances, 3, NULL);

  ck_assert(r == TL_OK);
  ck_assert(isnan(RX) && isnan(RY));
}
END_TEST

START_TEST(test_rwgh_ls2)
{
  tl_point3d result[1];

  int r = trilateration_rwgh_2d(result, ls2_anchors, ls2_distances, 8, NULL);

  ck_assert(r == TL_OK);

  ck_assert_eq_2d(380.45310, 536.57631, result[0]);
  /* latmath satisfies:
   * ck_assert_eq_2d(381.82767, 536.01998, result[0]);
   * LS² satisfies:
   * ck_assert_eq_2d(380.50998, 536.53912, result[0]);
   */
}
END_TEST

Suite *
trilateration_suite(void)
{
  Suite *s = suite_create("RWGH");
  TCase *tc_rwgh = tcase_create("RWGH");
  tcase_add_test(tc_rwgh, test_rwgh);
  suite_add_tcase(s, tc_rwgh);
  tc_rwgh = tcase_create("RWGH (same x)");
  tcase_add_test(tc_rwgh, test_rwgh_same);
  suite_add_tcase(s, tc_rwgh);
  tc_rwgh = tcase_create("RWGH (all same)");
  tcase_add_test(tc_rwgh, test_rwgh_all_same);
  suite_add_tcase(s, tc_rwgh);
  tc_rwgh = tcase_create("RWGH (colinear)");
  tcase_add_test(tc_rwgh, test_rwgh_colinear);
  suite_add_tcase(s, tc_rwgh);
  tc_rwgh = tcase_create("RWGH (LS2 test)");
  tcase_add_test(tc_rwgh, test_rwgh_ls2);
  suite_add_tcase(s, tc_rwgh);
  return s;
}

int
main(void)
{
  int number_failed;
  Suite *s = trilateration_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
