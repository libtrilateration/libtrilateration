/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <check.h>
#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>

#include "check_data.h"

START_TEST(test_md_minmax_abs)
{
     tl_point3d result[1];

     int r = trilateration_md_minmax_abs_2d(result, noncollinear_anchors,
                                            noncollinear_distances, 3, NULL);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(1.63879, 1.63879, result[0]);
}
END_TEST

START_TEST(test_md_minmax_abs_same)
{
     tl_point3d result[1];

     int r = trilateration_md_minmax_abs_2d(result, alt_noncollinear_anchors,
                                            alt_noncollinear_distances, 3,
                                            NULL);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(1.63880, 1.63880, result[0]);  
}
END_TEST

START_TEST(test_md_minmax_abs_all_same)
{
     tl_point3d result[1];

     int r = trilateration_md_minmax_abs_2d(result, collinear_vertical_anchors,
                                            collinear_vertical_distances, 3, NULL);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(1.00086, 2.00207, result[0]);  
}
END_TEST

START_TEST(test_md_minmax_abs_colinear)
{
     tl_point3d result[1];

     int r = trilateration_md_minmax_abs_2d(result, collinear_diagonal_anchors,
                                            collinear_diagonal_distances, 3, NULL);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(1.99528, 2.00472, result[0]);  
}
END_TEST

START_TEST(test_md_minmax_abs_ls2)
{
     tl_point3d result[1];

     int r = trilateration_md_minmax_abs_2d(result,ls2_anchors, ls2_distances,
                                            8, NULL);

     ck_assert(r == TL_OK);
     ck_assert_eq_2d(462.94435, 558.00894, result[0]);  
}
END_TEST

Suite *
trilateration_suite(void)
{
     Suite *s = suite_create("MD-MIN-MAX-ABS");
     TCase *tc_md_minmax_abs = tcase_create("MD-MIN-MAX-ABS");
     tcase_add_test(tc_md_minmax_abs, test_md_minmax_abs);
     suite_add_tcase(s, tc_md_minmax_abs);
     tc_md_minmax_abs = tcase_create("MD-MIN-MAX-ABS (same x)");
     tcase_add_test(tc_md_minmax_abs, test_md_minmax_abs_same);
     suite_add_tcase(s, tc_md_minmax_abs);
     tc_md_minmax_abs = tcase_create("MD-MIN-MAX-ABS (all same)");
     tcase_add_test(tc_md_minmax_abs, test_md_minmax_abs_all_same);
     suite_add_tcase(s, tc_md_minmax_abs);
     tc_md_minmax_abs = tcase_create("MD-MIN-MAX-ABS (colinear)");
     tcase_add_test(tc_md_minmax_abs, test_md_minmax_abs_colinear);
     suite_add_tcase(s, tc_md_minmax_abs);
     tc_md_minmax_abs = tcase_create("MD-MIN-MAX-ABS (LS2)");
     tcase_add_test(tc_md_minmax_abs, test_md_minmax_abs_ls2);
     suite_add_tcase(s, tc_md_minmax_abs);
     return s;
}

int
main(void)
{
     int number_failed;
     Suite *s = trilateration_suite();
     SRunner *sr = srunner_create(s);
     srunner_run_all (sr, CK_NORMAL);
     number_failed = srunner_ntests_failed(sr);
     srunner_free(sr);
     return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
