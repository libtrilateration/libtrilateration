/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef LIBTRILATERATION_CHECK_DATA_H
#define LIBTRILATERATION_CHECK_DATA_H

#define ck_assert_eq_2d(ex, ey, result) \
        do {                                                    \
                const double _ex = (ex), _ey = (ey), _rx = (result).x, _ry = (result).y; \
                ck_assert_msg((fabs(_ex - _rx) < 1e-5) && (fabs(_ey - _ry) < 1e-5), "Expected (%.*g; %.*g) , but it is (%.*g; %.*g)", DBL_DIG, _ex, DBL_DIG, _ey, DBL_DIG, _rx, DBL_DIG, _ry); \
        } while (0)

#define ck_assert_eq_3d(ex, ey, ez, result)                             \
        do {                                                            \
          const double _ex = (ex), _ey = (ey), _ez = (ez),              \
                  _rx = (result).x, _ry = (result).y, _rz = (result).z; \
          ck_assert_msg((fabs(_ex - _rx) < 1e-5) && (fabs(_ey - _ry) < 1e-5) && (fabs(_ez - _rz) < 1e-5), "Expected (%.*g; %.*g; %.*g) , but it is (%.*g; %.*g; %.*g)", DBL_DIG, _ex, DBL_DIG, _ey, DBL_DIG, _ez, DBL_DIG, _rx, DBL_DIG, _ry, DBL_DIG, _rz); \
        } while (0)




/***********************************************************************
 * Test cases.
 ***********************************************************************/


/* Solution is (2.0, 2.0) resp. (2.0, 2.0, 0.0) */

static const tl_point3d noncollinear_anchors[] = {
        { 1.0, 1.0, 0.0 },
        { 2.0, 1.0, 0.0 },
        { 1.0, 2.0, 0.0 },
        { 2.0, 2.0, 1.0 }
};

static const double noncollinear_distances[]  = { M_SQRT2, 1.0, 1.0, 1.0 };

static const tl_point3d alt_noncollinear_anchors[] = {
        { 1.0, 1.0, 0.0 },
        { 1.0, 2.0, 0.0 },
        { 2.0, 1.0, 0.0 },
        { 2.0, 2.0, 1.0 }
};

static const double alt_noncollinear_distances[] = { M_SQRT2, 1.0, 1.0, 1.0 };

static const tl_point3d collinear_horizontal_anchors[] = {
        { 1.0, 1.0, 0.0 },
        { 2.0, 1.0, 0.0 },
        { 3.0, 1.0, 0.0 },
        { 4.0, 1.0, 0.0 }
};

static const double collinear_horizontal_distances[] = {
        M_SQRT2, 1.0, 1.0
};

static const tl_point3d collinear_vertical_anchors[] = {
        { 1.0, 1.0, 0.0 },
        { 1.0, 2.0, 0.0 },
        { 1.0, 3.0, 0.0 },
        { 1.0, 4.0, 0.0 }
};

static const double collinear_vertical_distances[] = {
        M_SQRT2, 1.0, M_SQRT2, 2.0
};

static const tl_point3d collinear_diagonal_anchors[] = {
        { 1.0, 1.0, 0.0 },
        { 2.0, 2.0, 0.0 },
        { 3.0, 3.0, 0.0 },
        { 4.0, 4.0, 0.0 }
};

static const double collinear_diagonal_distances[] = {
        2.0, M_SQRT2, 2.0, 2.0
};


static const tl_point3d ls2_anchors[] = {
        { 100.0, 100.0, 0.0 },
        { 100.0, 300.0, 0.0 },
        { 400.0, 600.0, 0.0 },
        { 800.0, 800.0, 0.0 },
        { 110.0, 90.0, 0.0 },
        { 110.0, 290.0, 0.0 },
        { 410.0, 590.0, 0.0 },
        { 810.0, 790.0, 0.0 }
};
                                    
        
static const double ls2_distances[] = { 607.0, 452.0, 253.0, 461.0, 628.0,
                                        484.0, 233.0, 508.0 };

#endif
