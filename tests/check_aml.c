/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <check.h>

#include "check_data.h"

static trilateration_aml_params params;

START_TEST(test_aml)
{
  tl_point3d result[1];

  int r = trilateration_aml_2d(result, noncollinear_anchors,
                               noncollinear_distances, 3, &params);

  ck_assert(r == TL_OK);
  ck_assert_eq_2d(2.0, 2.0, result[0]);
}
END_TEST

START_TEST(test_aml_same)
{
  tl_point3d result[1];

  int r = trilateration_aml_2d(result, alt_noncollinear_anchors,
                               alt_noncollinear_distances, 3, &params);

  ck_assert(r == TL_OK);
  ck_assert_eq_2d(2.0, 2.0, result[0]);
}
END_TEST

START_TEST(test_aml_all_same)
{
  tl_point3d result[1];

  int r = trilateration_aml_2d(result, collinear_vertical_anchors,
                               collinear_vertical_distances, 3, &params);

  ck_assert(r == TL_OK);
  ck_assert_eq_2d(2.0, 2.0, result[0]);
}
END_TEST

START_TEST(test_aml_colinear)
{
  tl_point3d result[1];

  int r = trilateration_aml_2d(result, collinear_diagonal_anchors,
                               collinear_diagonal_distances, 3, &params);

  ck_assert(r == TL_OK);
  ck_assert_eq_2d(3.0, 1.0, result[0]);
}
END_TEST

START_TEST(test_aml_ls2)
{
  tl_point3d result[1];

  int r = trilateration_aml_2d(result, ls2_anchors, ls2_distances, 8, &params);

  ck_assert(r == TL_OK);
#if 0
  /* Results are random, no need to check the values. */
  ck_assert_eq_2d(469.01942, 504.00811, result[0]);
#endif
}
END_TEST

Suite *
trilateration_suite(void)
{
  Suite *s = suite_create("AML");
  TCase *tc_aml = tcase_create("AML");
  tcase_add_test(tc_aml, test_aml);
  suite_add_tcase(s, tc_aml);
  tc_aml = tcase_create("AML (same x)");
  tcase_add_test(tc_aml, test_aml_same);
  suite_add_tcase(s, tc_aml);
  tc_aml = tcase_create("AML (all same)");
  tcase_add_test(tc_aml, test_aml_all_same);
  suite_add_tcase(s, tc_aml);
  tc_aml = tcase_create("AML (colinear)");
  tcase_add_test(tc_aml, test_aml_colinear);
  suite_add_tcase(s, tc_aml);
  tc_aml = tcase_create("AML (LS2)");
  tcase_add_test(tc_aml, test_aml_ls2);
  suite_add_tcase(s, tc_aml);
  return s;
}

int
main(void)
{
  int number_failed;
  srand48_r(42, &(params.state));
  Suite *s = trilateration_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
