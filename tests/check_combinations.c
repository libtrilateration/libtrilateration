/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#include <check.h>

#include <libtrilateration/util.h>

START_TEST(test_combinations_binomial)
{
     ck_assert_int_eq(trilateration_binomial(1, 1), 1);
     ck_assert_int_eq(trilateration_binomial(2, 1), 2);
     ck_assert_int_eq(trilateration_binomial(5, 2), 10);
     ck_assert_int_eq(trilateration_binomial(5, 3), 10);
     ck_assert_int_eq(trilateration_binomial(6, 2), 15);
}
END_TEST

#define VALID_SUBSET(subset, K, N) \
     do { \
         const int _N = (N); const int _K = (K); const int *_s = subset; \
         for (int k = 0; k < _K; k++) { \
              ck_assert(0 <= _s[k] && _s[k] < _N); \
              for (int j = 0; j < k; j++) { \
                   ck_assert(_s[j] != _s[k]); \
              } \
         } \
     } while (0)

START_TEST(test_combinations_random_subset)
{
     struct drand48_data buffer;
     srand48_r(time(NULL), &buffer);


     for (int N = 4; N < 12; N++) {
          for (int K = 1; K < N; K++) {
               int subset[K];
               for (int i = 0; i < 100; i++) {
                    trilateration_random_subset(subset, K, N, &buffer);
                    VALID_SUBSET(subset, K, N);
               }
          }
     }
}
END_TEST

Suite *
collinear_suite(void)
{
  Suite *s = suite_create("Combinations");
  TCase *tc;
  tc = tcase_create("binomial");
  tcase_add_test(tc, test_combinations_binomial);
  suite_add_tcase(s, tc);
  tc = tcase_create("random subset");
  tcase_add_test(tc, test_combinations_random_subset);
  suite_add_tcase(s, tc);
  return s;
}

int
main(void)
{
  int number_failed;
  Suite *s = collinear_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
