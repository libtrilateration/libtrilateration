/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <float.h>
#include <math.h>

#include <check.h>
#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>

#include "check_data.h"

trilateration_cpf_params *params;

START_TEST(test_cpf)
{
        tl_point3d result[1];

	tl_point3d_set(&(params->pre), NAN, NAN, NAN);
        int r = trilateration_cpf_2d(result, noncollinear_anchors,
                                     noncollinear_distances, 3, params);

        ck_assert(r == TL_OK);
        ck_assert_eq_2d(1.55717, 1.52479, result[0]);
}
END_TEST

START_TEST(test_cpf_same)
{
        tl_point3d result[1];

	tl_point3d_set(&(params->pre), NAN, NAN, NAN);
        int r = trilateration_cpf_2d(result, alt_noncollinear_anchors,
                                     alt_noncollinear_distances, 3, params);

        ck_assert(r == TL_OK);
        ck_assert_eq_2d(1.55717, 1.52479, result[0]);
}
END_TEST

START_TEST(test_cpf_all_same)
{
        tl_point3d result[1];

	tl_point3d_set(&(params->pre), NAN, NAN, NAN);
        int r = trilateration_cpf_2d(result, collinear_vertical_anchors,
                                     collinear_vertical_distances, 3, params);

        ck_assert(r == TL_OK);
        ck_assert_eq_2d(1.07198, 1.99726, result[0]);
}
END_TEST

START_TEST(test_cpf_colinear)
{
        tl_point3d result[1];

	tl_point3d_set(&(params->pre), NAN, NAN, NAN);
        int r = trilateration_cpf_2d(result, collinear_diagonal_anchors,
                                     collinear_diagonal_distances, 3, params);

        ck_assert(r == TL_OK);
        ck_assert_eq_2d(1.92580, 2.15528, result[0]);
}
END_TEST

START_TEST(test_cpf_ls2)
{
        tl_point3d result[1];

	tl_point3d_set(&(params->pre), NAN, NAN, NAN);
        int r = trilateration_cpf_2d(result, ls2_anchors, ls2_distances, 8,
                                     params);

        ck_assert(r == TL_OK);
        ck_assert_eq_2d(470.88105, 499.16784, result[0]);
}
END_TEST

Suite *
trilateration_suite(void)
{
  Suite *s = suite_create("CPF");
  TCase *tc_cpf = tcase_create("CPF");
  tcase_add_test(tc_cpf, test_cpf);
  suite_add_tcase(s, tc_cpf);
  tc_cpf = tcase_create("CPF (same x)");
  tcase_add_test(tc_cpf, test_cpf_same);
  suite_add_tcase(s, tc_cpf);
  tc_cpf = tcase_create("CPF (all same)");
  tcase_add_test(tc_cpf, test_cpf_all_same);
  suite_add_tcase(s, tc_cpf);
  tc_cpf = tcase_create("CPF (colinear)");
  tcase_add_test(tc_cpf, test_cpf_colinear);
  suite_add_tcase(s, tc_cpf);
  tc_cpf = tcase_create("CPF (LS2 test)");
  tcase_add_test(tc_cpf, test_cpf_ls2);
  suite_add_tcase(s, tc_cpf);
  return s;
}

int
main(void)
{
  int number_failed;
  trilateration_cpf_2d_init((void **) &params);
  Suite *s = trilateration_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
