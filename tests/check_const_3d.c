/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <check.h>
#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>

#include "check_data.h"

START_TEST(test_const_3d)
{
  tl_point3d result[1];

  int r = trilateration_const_3d(result, noncollinear_anchors,
                                 noncollinear_distances, 3, NULL);

  ck_assert(r == TL_OK);
  ck_assert_eq_3d(0.0, 0.0, 0.0, result[0]);
}
END_TEST

START_TEST(test_const_3d_same)
{
  tl_point3d result[1];

  int r = trilateration_const_3d(result, alt_noncollinear_anchors,
                                 alt_noncollinear_distances, 3, NULL);

  ck_assert(r == TL_OK);
  ck_assert_eq_3d(0.0, 0.0, 0.0, result[0]);
}
END_TEST

START_TEST(test_const_3d_all_same)
{
  tl_point3d result[1];

  int r = trilateration_const_3d(result, collinear_vertical_anchors,
                                 collinear_vertical_distances, 3, NULL);

  ck_assert(r == TL_OK);
  ck_assert_eq_3d(0.0, 0.0, 0.0, result[0]);
}
END_TEST

START_TEST(test_const_3d_colinear)
{
  tl_point3d result[1];

  int r = trilateration_const_3d(result, collinear_diagonal_anchors,
                                 collinear_diagonal_distances, 3, NULL);

  ck_assert(r == TL_OK);
  ck_assert_eq_3d(0.0, 0.0, 0.0, result[0]);
}
END_TEST

START_TEST(test_const_3d_ls2)
{
  tl_point3d result[1];

  int r = trilateration_const_3d(result, ls2_anchors, ls2_distances, 8, NULL);

  ck_assert(r == TL_OK);
  ck_assert_eq_3d(0.0, 0.0, 0.0, result[0]);
}
END_TEST

Suite *
trilateration_suite(void)
{
  Suite *s = suite_create("Constant-3D");
  TCase *tc_const_3d = tcase_create("Constant-3D");
  tcase_add_test(tc_const_3d, test_const_3d);
  suite_add_tcase(s, tc_const_3d);
  tc_const_3d = tcase_create("Constant-3D (same x)");
  tcase_add_test(tc_const_3d, test_const_3d_same);
  suite_add_tcase(s, tc_const_3d);
  tc_const_3d = tcase_create("Constant-3D (all same)");
  tcase_add_test(tc_const_3d, test_const_3d_all_same);
  suite_add_tcase(s, tc_const_3d);
  tc_const_3d = tcase_create("Constant-3D (colinear)");
  tcase_add_test(tc_const_3d, test_const_3d_colinear);
  suite_add_tcase(s, tc_const_3d);
  tc_const_3d = tcase_create("Constant-3D (LS2)");
  tcase_add_test(tc_const_3d, test_const_3d_ls2);
  suite_add_tcase(s, tc_const_3d);
  return s;
}

int
main(void)
{
  int number_failed;
  Suite *s = trilateration_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
