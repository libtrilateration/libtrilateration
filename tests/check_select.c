/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <check.h>

#include <libtrilateration/util.h>

START_TEST(test_select_one)
{
  double a[] = { 5.0, 2.0, 4.0, 1.0, 3.0 };
  double r;
  r = trilateration_fselect(a, 5, 0);
  ck_assert(fabs(r - 1.0) < 1e-5);
  r = trilateration_fselect(a, 5, 1);
  ck_assert(fabs(r - 2.0) < 1e-5);
  r = trilateration_fselect(a, 5, 2);
  ck_assert(fabs(r - 3.0) < 1e-5);
  r = trilateration_fselect(a, 5, 3);
  ck_assert(fabs(r - 4.0) < 1e-5);
  r = trilateration_fselect(a, 5, 4);
  ck_assert(fabs(r - 5.0) < 1e-5);
}
END_TEST

Suite *
collinear_suite(void)
{
  Suite *s = suite_create("Select");
  TCase *tc;
  tc = tcase_create("one");
  tcase_add_test(tc, test_select_one);
  suite_add_tcase(s, tc);
  return s;
}

int
main(void)
{
  int number_failed;
  Suite *s = collinear_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
