/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>

#include <check.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>

START_TEST(test_disc_disjoint)
{
        tl_point3d p[] = { { 1.0, 1.0, 0.0 }, { 2.0, 2.0, 0.0 } };
        double r[] =  { 0.5, 0.5 };
        tl_point3d result[1];
        int res;
        res = trilateration_point_in_discs(result, p, r, 2);
        ck_assert(res == 0);
}
END_TEST

START_TEST(test_disc_enclosed)
{
        tl_point3d p[] = { { 1.0, 1.0, 0.0 }, { 2.0, 2.0, 0.0 } };
        double r[] =  { 0.25, 2.0 };
        tl_point3d result[1];
        int res;
        res = trilateration_point_in_discs(result, p, r, 2);
        ck_assert(res == 0);
}
END_TEST

START_TEST(test_disc_touching_outer)
{
        tl_point3d p[] = { { 1.0, 1.0, 0.0 }, { 1.0, 4.0, 0.0 } };
        double r[] =  { 1.0, 2.0 };
        tl_point3d result[1];
        int res;
        res = trilateration_point_in_discs(result, p, r, 2);
        ck_assert(res == 1);
        res = trilateration_point_in_discs_p(result, p, r, 2);
        ck_assert(res == 1);
}
END_TEST

START_TEST(test_disc_touching_inner)
{
        tl_point3d p[] = { { 3.0, 1.0, 0.0 }, { 4.0, 1.0, 0.0 } };
        double r[] =  { 1.0, 2.0 };
        tl_point3d result[1];
        int res;
        res = trilateration_point_in_discs(result, p, r, 2);
        ck_assert(res == 1);
        res = trilateration_point_in_discs_p(result, p, r, 2);
        ck_assert(res == 1);
}
END_TEST

START_TEST(test_disc_intersecting_outer)
{
        tl_point3d p[] = { { 0.0, 0.0, 0.0 }, { 2.0, 0.0, 0.0 } };
        double r[] =  { M_SQRT2, M_SQRT2 };
        tl_point3d result[1];
        int res;
        res = trilateration_point_in_discs(result, p, r, 2);
        ck_assert(res == 1);
        res = trilateration_point_in_discs_p(result, p, r, 2);
        ck_assert(res == 1);
}
END_TEST

START_TEST(test_disc_intersecting_inner)
{
        tl_point3d p[] = { { 0.0, 0.0, 0.0 }, { 2.0, 1.0, 0.0 } };
        double r[] =  { 2.0, 1.0 };
        tl_point3d result[1];
        int res;
        res = trilateration_point_in_discs(result, p, r, 2);
        ck_assert(res == 1);
        res = trilateration_point_in_discs_p(result, p, r, 2);
        ck_assert(res == 1);
}
END_TEST

START_TEST(test_discs_intersections_three)
{
        tl_point3d p[] = { { 0.0, 0.0, 0.0 }, { 2.0, 0.0, 0.0 },
                           { 1.0, 0.0, 0.0 } };
        double r[3] = { 2.0, 1.0, 1.5 };
        tl_point3d result[1];
        int res;
        res = trilateration_point_in_discs(result, p, r, 3);
        ck_assert(res == 1);
        res = trilateration_point_in_discs_p(result, p, r, 3);
        ck_assert(res > 0);
}
END_TEST

Suite *
disc_suite(void)
{
  Suite *s = suite_create("Disc");
  TCase * tc;
  tc = tcase_create("disjoint");
  tcase_add_test(tc, test_disc_disjoint);
  suite_add_tcase(s, tc);

  tc = tcase_create("enclosed");
  tcase_add_test(tc, test_disc_enclosed);
  suite_add_tcase(s, tc);

  tc = tcase_create("touching outer");
  tcase_add_test(tc, test_disc_touching_outer);
  suite_add_tcase(s, tc);

  tc = tcase_create("touching inner");
  tcase_add_test(tc, test_disc_touching_inner);
  suite_add_tcase(s, tc);

  tc = tcase_create("intersect outer");
  tcase_add_test(tc, test_disc_intersecting_outer);
  suite_add_tcase(s, tc);

  tc = tcase_create("intersect inner");
  tcase_add_test(tc, test_disc_intersecting_inner);
  suite_add_tcase(s, tc);

  tc = tcase_create("intersections three");
  tcase_add_test(tc, test_discs_intersections_three);
  suite_add_tcase(s, tc);

  return s;
}

int
main(void)
{
  int number_failed;
  Suite *s = disc_suite();
  SRunner *sr = srunner_create(s);
  srunner_run_all (sr, CK_NORMAL);
  number_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
