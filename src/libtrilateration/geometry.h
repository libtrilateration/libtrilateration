/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef LIBTRILATERATION_GEOMETRY_H
#define LIBTRILATERATION_GEOMETRY_H 1

int
trilateration_bounding_box_2d(tl_point3d *restrict result,
                              const tl_point3d *const a,
			      const double *const d, const size_t N);

int
trilateration_bounding_box_3d(tl_point3d *restrict result,
                              const tl_point3d *const a,
			      const double *const d, const size_t N);

int
trilateration_collinear_p(const tl_point3d *restrict p, const size_t N);

double
trilateration_distance_squared(double cx, double cy, double dx, double dy);

double
trilateration_distance_squared_3d(const tl_point3d *const p,
                                  const tl_point3d *const q);

double
trilateration_distance(double cx, double cy, double dx, double dy);

double
trilateration_distance_3d(const tl_point3d *const p, const tl_point3d *const q);

double
trilateration_square_residual_error(const tl_point3d *const anchor,
                                    const double *const d, size_t N,
                                    const tl_point3d *const p);

double
trilateration_square_residual_error_3d(const tl_point3d *const anchor,
                                       const double *const d, const size_t N,
                                       const tl_point3d *const p);

int
trilateration_point_on_circle_p(const tl_point3d *const p,
                                const tl_point3d *const c, double r);

int
trilateration_circle_intersections(tl_point3d *restrict res,
                                   const tl_point3d *const p1, const double r1,
				   const tl_point3d *const p2, const double r2);

int
trilateration_circles_intersections(tl_point3d *restrict result, const size_t K,
                                    const tl_point3d *const p,
                                    const double *const r,
                                    const size_t N);

int
trilateration_circle_line_intersections(tl_point3d *restrict result,
                                        const tl_point3d *const c, double r,
                                        const tl_point3d *const p1,
                                        const tl_point3d *const p2);


int
trilateration_circle_approximate_intersection(tl_point3d *restrict result,
                                              const tl_point3d *const p1,
                                              const double r1,
                                              const tl_point3d *const p2,
                                              const double r2);

int
trilateration_circle_approximate(tl_point3d *restrict result,
                                 const tl_point3d *const p1, const double r1,
                                 const tl_point3d *const p2, const double r2);

int
trilateration_centre_of_mass(tl_point3d *restrict result,
                             const tl_point3d *const a, const size_t N);

int
trilateration_centre_of_mass_3d(tl_point3d *restrict result,
                                const tl_point3d *const a, const size_t N);

int
trilateration_weighted_centre_of_mass(tl_point3d *restrict result,
                                      const tl_point3d *const a,
                                      const double *const w, const size_t N);

int
trilateration_weighted_centre_of_mass_3d(tl_point3d *restrict result,
				         const tl_point3d *const a,
				         const double *const w, const size_t N);

int __attribute__((__pure__,__nonnull__))
trilateration_point_in_disc_p(const tl_point3d *const p,
                              const tl_point3d *const c, const double r);

int __attribute__((__pure__,__nonnull__))
trilateration_point_in_discs_p(const tl_point3d *const p,
                               const tl_point3d *const c,
                               const double *const r, size_t N);

int
trilateration_point_in_discs(tl_point3d *restrict result, const tl_point3d* p,
			     const double *restrict r, const size_t N);

double __attribute__((__pure__))
trilateration_triangle_perimeter(const tl_point3d *const t);

double __attribute__((__pure__))
trilateration_triangle_area(const tl_point3d *const t);


int
trilateration_weighted_geometric_median(tl_point3d *restrict result,
                                        const tl_point3d *const p,
                                        const double *const w, const size_t N);

int
trilateration_weighted_geometric_median_3d(tl_point3d *restrict result,
					   const tl_point3d *const p,
                                           const double *const w,
                                           const size_t N);

int
trilateration_geometric_median(tl_point3d *restrict result,
                               const tl_point3d *const p, const size_t N);

int
trilateration_geometric_median_3d(tl_point3d *restrict result,
                                  const tl_point3d *const p, const size_t N);

#endif
