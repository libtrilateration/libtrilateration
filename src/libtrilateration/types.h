/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef LIBTRILATERATION_TYPES_H
#define LIBTRILATERATION_TYPES_H 1

/*!
 * \brief A three-dimensional point.
 */
struct tl_point_3d {
        double x;
        double y;
        double z;
};

typedef struct tl_point_3d tl_point3d;

static inline void __attribute__((__always_inline__,__nonnull__))
tl_point3d_set(tl_point3d *res, double x, double y, double z)
{
        res->x = x;
        res->y = y;
        res->z = z;
}




static inline void __attribute__((__always_inline__,__nonnull__))
tl_point3d_set_point(tl_point3d *res, const tl_point3d *const src)
{
        res->x = src->x;
        res->y = src->y;
        res->z = src->z;
}




/*!
 * 
 */
enum trilateration_status {
        TL_OK = 0,
        TL_2BIG = -2,
        TL_2FEW = -3,
        TL_DOMAIN = -4,
        TL_ERROR = -254,
        TL_NOT_IMPLEMENTED = -255
};

typedef enum trilateration_status trilateration_status;


/*!
 * \var trilateration_algorithm_t
 *
 * The signature of all trilateration algorithms.
 *
 * \param result Reference to the resulting X coordinate, must not be null.
 * \param anchor Array of coordinates of anchor nodes.
 * \param d  Array of distances to anchor nodes.
 * \param N  Number of anchor nodes and distances.
 * \param params A reference to possible parameters or state
 * information, depending on algorithm.
 */
typedef trilateration_status
(*trilateration_algorithm_t)(tl_point3d *restrict result,
                             const tl_point3d *const anchor,
                             const double *restrict d, const size_t N,
                             void *params);


#define P(p, i) (p + i)
#define X(p, i) p[i].x
#define Y(p, i) p[i].y
#define Z(p, i) p[i].z

#define AX(i) X(anchor, i)
#define AY(i) Y(anchor, i)
#define AZ(i) Z(anchor, i)

#define RX X(result, 0)
#define RY Y(result, 0)
#define RZ Z(result, 0)

#endif
