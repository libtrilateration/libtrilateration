/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef LIBTRILATERATION_ALGEBRA_H
#define LIBTRILATERATION_ALGEBRA_H 1

#define C(row, col, rows, cols) trilateration_matrix_index(row, col, rows, cols)

int
trilateration_matrix_index(size_t row, size_t col, size_t rows, size_t cols);

void
trilateration_matrix_transpose(double *restrict dest,
                               const double *restrict src,
                               const size_t rows, const size_t cols);

void
trilateration_matrix_multiply(double *restrict dest,
                             const double *restrict src1,
                             const size_t rows1, const size_t cols1,
                             const double *restrict src2,
                             const size_t rows2, const size_t cols2);

void
trilateration_matrix_invert_2x2(double *restrict dest,
                                const double *restrict src);

void
trilateration_matrix_invert_3x3(double *restrict dest,
                                const double *restrict src);

void
trilateration_print_matrix(const double *restrict src, size_t rows,
                           size_t cols);

void
trilateration_solve_linear_equation_2d(double *restrict result,
				       const double *restrict a,
				       const double *restrict b, size_t M);

void
trilateration_solve_linear_equation_3d(double *restrict result,
				       double *restrict a,
				       double *restrict b, size_t M);

#endif
