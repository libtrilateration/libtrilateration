/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef LIBTRILATERATION_ALGORITHMS_H
#define LIBTRILATERATION_ALGORITHMS_H 1

/*!
 * \file algorithms.h
 *
 * \brief Interface to the localisation algorithms.
 */

struct trilateration_aml_params {
     struct drand48_data state;
};

typedef struct trilateration_aml_params trilateration_aml_params;

trilateration_status
trilateration_aml_2d_init(void **params);

trilateration_status
trilateration_aml_2d(tl_point3d *restrict result,
		     const tl_point3d *const anchor,
		     const double *const d, const size_t N,
		     void *params);

trilateration_status
trilateration_aml_3d_init(void **params);

trilateration_status
trilateration_aml_3d(tl_point3d *restrict result,
		     const tl_point3d *const anchor,
		     const double *const d, const size_t N,
		     void *params);

trilateration_status
trilateration_bilateration_2d(tl_point3d *restrict result,
                              const tl_point3d *const anchor,
			      const double *const d, const size_t N,
			      void *params);

trilateration_status
trilateration_bilateration_3d(tl_point3d *restrict result,
                              const tl_point3d *const anchor,
			      const double *const d, const size_t N,
			      void *params);

trilateration_status
trilateration_centroid_2d(tl_point3d *restrict result,
			  const tl_point3d *const anchor,
			  const double *const d, const size_t N,
			  void *params);

trilateration_status
trilateration_centroid_3d(tl_point3d *restrict result,
			  const tl_point3d *const anchor,
			  const double *const d, const size_t N,
			  void *params);
/* CluRoL */

struct trilateration_clurol_params {
     double d_max;
};

typedef struct trilateration_clurol_params trilateration_clurol_params;

void
trilateration_clurol_params_default(trilateration_clurol_params *params);

trilateration_status
trilateration_clurol_2d_init(void **params);

trilateration_status
trilateration_clurol_2d(tl_point3d *restrict result,
			const tl_point3d *const anchor,
			const double *const d, const size_t N,
			void *params);

/* CluRoL */

struct trilateration_clurol_3d_params {
     double d_max;
};

typedef struct trilateration_clurol_3d_params trilateration_clurol_3d_params;

trilateration_status
trilateration_clurol_3d_init(void **params);

void
trilateration_clurol_3d_params_default(trilateration_clurol_3d_params *params);

trilateration_status
trilateration_clurol_3d(tl_point3d *restrict result, const tl_point3d *const anchor,
			const double *const d, const size_t N,
			void *params);

/* Const */

trilateration_status
trilateration_const_2d(tl_point3d *restrict result,
		       const tl_point3d *const anchor __attribute__((__unused__)),		       const double *const d __attribute__((__unused__)),
		       const size_t N __attribute__((__unused__)),
		       void *params __attribute__((__unused__)));

trilateration_status
trilateration_const_3d(tl_point3d *restrict result,
		       const tl_point3d *const anchor __attribute__((__unused__)),
		       const double *const d __attribute__((__unused__)),
		       const size_t N __attribute__((__unused__)),
		       void *params __attribute__((__unused__)));

struct trilateration_cpf_params {
     tl_point3d pre;
     struct drand48_data state;
};

typedef struct trilateration_cpf_params trilateration_cpf_params;

trilateration_status
trilateration_cpf_2d_init(void **params);

trilateration_status
trilateration_cpf_2d(tl_point3d *restrict result, const tl_point3d *const anchor,
		     const double *const d, const size_t N, void *params);

struct trilateration_cpf_3d_params {
     tl_point3d pre;
     struct drand48_data state;
};


typedef struct trilateration_cpf_3d_params trilateration_cpf_3d_params;

trilateration_status
trilateration_cpf_3d_init(void **params);

trilateration_status
trilateration_cpf_3d(tl_point3d *restrict result, const tl_point3d *const anchor,
		     const double *const d, const size_t N, void *params);

trilateration_status
trilateration_eminmax_w2_2d(tl_point3d *restrict result,
                            const tl_point3d *const anchor,
			    const double *const d, const size_t N,
			    void *params);

trilateration_status
trilateration_eminmax_w4_2d(tl_point3d *restrict result,
                            const tl_point3d *const anchor,
			    const double *const d, const size_t N,
			    void *params);

trilateration_status
trilateration_geo_3_2d(tl_point3d *restrict result, const tl_point3d *const anchor,
		       const double *const d, const size_t N,
		       void *params);

trilateration_status
trilateration_geo_3_3d(tl_point3d *restrict result, const tl_point3d *const anchor,
		       const double *const d, const size_t N,
		       void *params);

trilateration_status
trilateration_geo_n_2d(tl_point3d *restrict result, const tl_point3d *const anchor,
		       const double *const d, const size_t N,
		       void *params);

trilateration_status
trilateration_geo_n_3d(tl_point3d *restrict result, const tl_point3d *const anchor,
		       const double *const d, const size_t N,
		       void *params);


/*
 * ICLA
 */
struct trilateration_icla_params {
     double alpha;
     double move_step;
};

typedef struct trilateration_icla_params trilateration_icla_params;

trilateration_status
trilateration_icla_2d(tl_point3d *restrict result, const tl_point3d *const anchor,
		      const double *const d, const size_t N,
		      void *params);

trilateration_status
trilateration_icla_3d(tl_point3d *restrict result, const tl_point3d *const anchor,
		      const double *const d, const size_t N,
		      void *params);


/*
 * LLS
 */
trilateration_status
trilateration_lls_2d(tl_point3d *restrict result, const tl_point3d *const anchor,
		     const double *const d, const size_t N,
		     void *params);

/*
 * LLS
 */
trilateration_status
trilateration_lls_3d(tl_point3d *restrict result, const tl_point3d *const anchor,
		     const double *const d, const size_t N,
		     void *params);



trilateration_status
trilateration_lms_2d_init(void **params);

trilateration_status
trilateration_lms_2d(tl_point3d *restrict result,
                     const tl_point3d *const anchor, const double *const d,
                     const size_t N, void *params);

trilateration_status
trilateration_lms_3d_init(void **params);

trilateration_status
trilateration_lms_3d(tl_point3d *restrict result,
                     const tl_point3d *const anchor, const double *const d,
                     const size_t N, void *params);

trilateration_status
trilateration_md_minmax_2d(tl_point3d *restrict result,
                           const tl_point3d *const anchor,
			   const double *const d, const size_t N,
			   void *params);


trilateration_status
trilateration_md_minmax_abs_2d(tl_point3d *restrict result,
                               const tl_point3d *const anchor,
			       const double *const d, const size_t N,
			       void *params);

trilateration_status
trilateration_minmax_2d(tl_point3d *restrict result, const tl_point3d *const anchor,
			const double *const d, const size_t N,
			void *params);

struct trilateration_mle_gamma_params {
     double shape;
     double rate;
     double offset;
};

typedef struct trilateration_mle_gamma_params trilateration_mle_gamma_params;


trilateration_status
trilateration_mle_gamma_2d(tl_point3d *restrict result,
                           const tl_point3d *const anchor,
			   const double *const d, const size_t N,
			   void *params);

struct trilateration_mle_gauss_params {
     double median;
     double variance;
};

typedef struct trilateration_mle_gauss_params trilateration_mle_gauss_params;

trilateration_status
trilateration_mle_gauss_2d(tl_point3d *restrict result,
                           const tl_point3d *const anchor,
			   const double *const d, const size_t N,
			   void *params);

trilateration_status
trilateration_nlls_2d(tl_point3d *restrict result, const tl_point3d *const anchor,
		      const double *const d, const size_t N, void *params);

trilateration_status
trilateration_nlls_3d(tl_point3d *restrict result, const tl_point3d *const anchor,
		      const double *const d, const size_t N, void *params);

struct trilateration_rlsm_params {
     trilateration_algorithm_t algorithm;
     void *params;
     size_t i_limit;
};

typedef struct trilateration_rlsm_params trilateration_rlsm_params;

trilateration_status
trilateration_rlsm_2d(tl_point3d *restrict result, const tl_point3d *const anchor,
		      const double *const d, const size_t N,
		      void *params);

struct trilateration_rlsm_3d_params {
     trilateration_algorithm_t algorithm;
     void *params;
};

typedef struct trilateration_rlsm_3d_params trilateration_rlsm_3d_params;

trilateration_status
trilateration_rlsm_3d(tl_point3d *restrict result, const tl_point3d *const anchor,
		      const double *const d, const size_t N,
		      void *params);

struct trilateration_rwgh_params {
     trilateration_algorithm_t algorithm;
     void *params;
};

typedef struct trilateration_rwgh_params trilateration_rwgh_params;

trilateration_status
trilateration_rwgh_2d(tl_point3d *restrict result, const tl_point3d *const anchor,
		      const double *const d, const size_t N,
		      void *params);

struct trilateration_rwgh_3d_params {
     trilateration_algorithm_t algorithm;
     void *params;
};

typedef struct trilateration_rwgh_3d_params trilateration_rwgh_3d_params;

trilateration_status
trilateration_rwgh_3d(tl_point3d *restrict result, const tl_point3d *const anchor,
		      const double *const d, const size_t N,
		      void *params);

trilateration_status
trilateration_trilateration_2d(tl_point3d *restrict result,
                               const tl_point3d *const anchor,
			       const double *const d, const size_t N,
			       void *params);

trilateration_status
trilateration_trilateration_3d(tl_point3d *restrict result,
                               const tl_point3d *const anchor,
			       const double *const d, const size_t N,
			       void *params);

trilateration_status
trilateration_weighted_centroid_2d(tl_point3d *restrict result,
				   const tl_point3d *const anchor,
			           const double *const d, const size_t N,
			           void *params);

trilateration_status
trilateration_weighted_lls_2d(tl_point3d *restrict result,
			      const tl_point3d *const anchor,
		              const double *const d, const size_t N,
		              void *params);

trilateration_status
trilateration_weighted_lls_3d(tl_point3d *restrict result,
			      const tl_point3d *const anchor,
		              const double *const d, const size_t N,
		              void *params);

#endif
