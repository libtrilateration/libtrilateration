/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef LIBTRILATERATION_UTIL_H
#define LIBTRILATERATION_UTIL_H 1

double
trilateration_fselect(double const * const values, const size_t N,
		      const int k);

double
trilateration_fmedian(double const * const values, const size_t N);

void
trilateration_fsort(double *values, const size_t length);

void
trilateration_sort(int *values, const size_t length);

void
trilateration_sort2(int *values);

void
trilateration_sort3(int *values);

void
trilateration_sort4(int *values);

void
trilateration_sort5(int *values);

void
trilateration_sort6(int *values);

unsigned int
trilateration_binomial(unsigned int n, unsigned int k);

int
trilateration_first_combination(unsigned int *restrict n, const size_t k,
				const size_t N);

int
trilateration_next_combination_p(unsigned int *restrict n, const size_t k,
				 const size_t N);

int
trilateration_next_combination(unsigned int *restrict n, const size_t K,
			       const size_t N);

void
trilateration_random_subset(int *restrict r, const size_t K, const size_t N,
			    struct drand48_data *buffer);

unsigned int
trilateration_exponent(unsigned int x, unsigned int n);


#endif
