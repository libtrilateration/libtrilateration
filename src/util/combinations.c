/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include "libtrilateration/util.h"

/*!
 * Calculate the binomial coefficients.
 * 
 */
unsigned int
trilateration_binomial(const unsigned int n, unsigned int k)
{
     if (k <= n) {
          /* To save time, exploit the symmetry of the binomial
	     coeffcients. */
          if (k > n / 2U) {
	       k = n - k;
          }

          unsigned int result = 1U;
          for (unsigned int i = 1U; i <= k; i++) {
               assert(i != 0);
	       result = result * (n - k + i) / i;
          }
          return result;
     }
     return 0U;
}


/*!
 * Combinatorial numbers.
 */
int
trilateration_first_combination(unsigned int *restrict n, const size_t k,
				const size_t N __attribute__((__unused__)))
{
     assert(k <= N);
     for (size_t i = 0; i < k; i++) {
	  n[i] = i;
     }
     return 1;
}


/*!
 * Tests whether there is a next combination.
 */
int
trilateration_next_combination_p(unsigned int *restrict n, const size_t k,
				 const size_t N)
{
     for (size_t i = 0; i < k; ++i) {
	  if (n[i] < N - k + i) {
	       return 1;
	  }
     }
     return 0;
}



/*!
 * Combinatorial numbers.
 */
int
trilateration_next_combination(unsigned int *restrict n, const size_t K,
			       const size_t N)
{
     if (!trilateration_next_combination_p(n, K, N)) {
	  return 0;
     }
     for (size_t i = 0; i < K; i++) {
	  if ((i < K - 1) ? n[i] + 1 < n[i + 1] : n[i] + 1 < N) {
	       n[i]++;
	       break;
	  } else {
	       n[i] = i;
	  }
     }
     return 1;
}




/*
 * Shuffle of K elements of the first N natural numbers.
 *
 * \param r Pointer to an array of K elements.
 * \param K number of elements to draw.
 * \param N characterises the number of elements.
 * \param buffer A buffer used for generating random numbers.
 *
 * The implementation uses an inside out version of the Fisher-Yates
 * shuffle.
 */
void
trilateration_random_subset(int *restrict r, const size_t K, const size_t N,
			    struct drand48_data *buffer)
{
     double X;

     r[0] = 0;
     for (size_t i = 1; i < K; i++) {
	  /* Generate a random number from 0 to i inclusive. Note that
	   * X is from 0 inclusive to 1 exlusive. We add one to allow
	   * i as a result.
	   */
	  drand48_r(buffer, &X);
	  size_t j = (size_t) floor(X * ((double) i + 1.0));
	  assert (/* 0 <= j && */ j <= i);
	  if (j < i) {
	       r[i] = r[j];
	  }
	  r[j] = i;
     }
     for (size_t i = K; i < N; i++) {
	  drand48_r(buffer, &X);
	  size_t j = (size_t) floor(X * ((double) i + 1.0));
	  if (j < K) {
	       r[j] = i;
	  }
     }
}
