/*
 * This file is part of libtrilateration.
 *
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <stdlib.h>

#include <libtrilateration/util.h>

/* An implementation of Comb sort by Wlodzimierz Dobosiewicz (1980)
 *
 * It is a variant opf shell sort and improves bubblesort.
 */
void
trilateration_fsort(double *values, const size_t length)
{
  size_t gap = length;
  int swapped = 0;

  while ((gap > 1) || swapped)
    {
      if (gap > 1)
	{
	  gap = (gap * 10u) / 13u;
	}
      swapped = 0;
      for (size_t i = 0; i + gap < length; i++)
	{
	  if (values[i] > values[i + gap])
	    {
	      double t = values[i];
	      values[i] = values[i + gap];
	      values[i + gap] = t;
	      swapped = 1;
            }
        }
    }
}





/* An implementation of Comb sort by Wlodzimierz Dobosiewicz (1980)
 *
 * It is a variant opf shell sort and improves bubblesort.
 */
void
trilateration_sort(int *values, const size_t length)
{
  size_t gap = length;
  int swapped = 0;

  while ((gap > 1) || swapped)
    {
      if (gap > 1)
	{
	  gap = (gap * 10u) / 13u;
	}
      swapped = 0;
      for (size_t i = 0; i + gap < length; i++)
	{
	  if (values[i] > values[i + gap])
	    {
	      int t = values[i];
	      values[i] = values[i + gap];
	      values[i + gap] = t;
	      swapped = 1;
            }
        }
    }
}





static inline void
swap_int(int *values, int x, int y)
{
  if (values[x] > values[y]) {
    int t = values[x];
    values[x] = values[y];
    values[y] = t;
  }
}



/*!
 * Sort a list of two integers using optimal sorting network.
 */
void
trilateration_sort2(int *values)
{
  swap_int(values, 0, 1);
}



/*!
 * Sort a list of three integers using optimal sorting network.
 */
void
trilateration_sort3(int *values)
{
  swap_int(values, 0, 1);
  swap_int(values, 0, 2);
  swap_int(values, 1, 2);
}



/*!
 * Sort a list of four integers using optimal sorting network.
 */
void
trilateration_sort4(int *values)
{
  swap_int(values, 0, 1);
  swap_int(values, 2, 3);
  swap_int(values, 0, 2);
  swap_int(values, 1, 3);
  swap_int(values, 1, 2);
}



/*!
 * Sort a list of five integers using optimal sorting network.
 */
void
trilateration_sort5(int *values)
{
  swap_int(values, 0, 1);
  swap_int(values, 2, 3);
  swap_int(values, 0, 2);
  swap_int(values, 1, 3);
  swap_int(values, 1, 2);
  swap_int(values, 0, 4);
  swap_int(values, 1, 4);
  swap_int(values, 2, 4);
  swap_int(values, 3, 4);
}



/*!
 * Sort a list of six integers using optimal sorting network.
 */
void
trilateration_sort6(int *values)
{
  swap_int(values, 0, 1);
  swap_int(values, 2, 3);
  swap_int(values, 0, 2);
  swap_int(values, 1, 3);
  swap_int(values, 1, 2);
  swap_int(values, 4, 5);
  swap_int(values, 0, 4);
  swap_int(values, 1, 5);
  swap_int(values, 1, 4);
  swap_int(values, 2, 4);
  swap_int(values, 3, 5);
  swap_int(values, 3, 4);
}
