/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include "libtrilateration/config.h"
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif

#include <argp.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <sched.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>

#include <sys/time.h>
#include <sys/resource.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include <hdf5.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>

/* By defining the macro H5_NO_DEPRECATED_SYMBOLS, we force the use of
 * the version 1.8 API regardless of the configuration of hdf5
 */
#define H5_NO_DEPRECATED_SYMBOLS 1

#include <hdf5.h>

enum variant {
#define VARIANT(type, name, h5name) type,
#include "variant.h"
#undef VARIANT
     NUM_VARIANTS
};

static const char *hdf5_variant[] = {
#undef  VARIANT
#define VARIANT(tag, name, h5name) h5name,
#include "variant.h"
    NULL
};



static const char *
hdf5_variant_name(enum variant variant)
{
     if (variant < NUM_VARIANTS) {
	  return hdf5_variant[variant];
     } else {
	  return NULL;
     }
}




static void
hdf5_write_anchors(hid_t file_id, const tl_point3d *const anchor, size_t N)
{
    hid_t dataset, dataspace;
    hsize_t dims[2] = { N, 3 };

    dataspace = H5Screate_simple(3, dims, NULL);
    dataset = H5Dcreate(file_id, "/Anchors", H5T_NATIVE_FLOAT,
                        dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
             (double *) anchor);
    H5Sclose(dataspace);
    H5Dclose(dataset);
}



static void
hdf5_write_locbased(const char *filename, const tl_point3d *const anchor,
		    const size_t no_anchors, double **results,
		    const uint16_t width, const uint16_t height)
{
    hid_t file_id, grp, dataset, dataspace, plist_id;
    hsize_t dims[2];
    hsize_t chunk_dims[2] = { width, height };

    file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    grp = H5Gcreate(file_id, "/Result", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    hdf5_write_anchors(file_id, anchor, no_anchors);

    dims[0] = height;
    dims[1] = width;
    for (int k = 0; k < NUM_VARIANTS; k++) {
        char name[256];
        dataspace = H5Screate_simple(2, dims, NULL);
	plist_id = H5Pcreate(H5P_DATASET_CREATE);
	H5Pset_chunk(plist_id, 2, chunk_dims);
	H5Pset_deflate (plist_id, 9);
        snprintf(name, 256, "/Result/%s", hdf5_variant_name((enum variant) k));
	fprintf(stdout, "Writing %s\n", name);
        dataset = H5Dcreate(file_id, name, H5T_NATIVE_FLOAT,
                            dataspace, H5P_DEFAULT, plist_id, H5P_DEFAULT);
        H5Dwrite(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                 results[k]);
        H5Sclose(dataspace);
        H5Dclose(dataset);
    }
    H5Gclose(grp);
    H5Fclose(file_id);
}

/*!
 *
 */

/*! Parameters to the location-based simulator. */

typedef struct run_params_t {
     size_t id;
     tl_point3d const *anchor;
     size_t N;
     double * restrict * restrict results;
     uint16_t width;
     uint16_t height;
     size_t start;  /*!< Start of the slice. */
     size_t slice;  /*!< Length of the slice. */
     uint_fast64_t times; /*!< How often to test a position. */
     trilateration_algorithm_t algorithm; /*!< Reference to the localisation function. */
     gsl_rng *rng;
     double sigma; /*!< The scale of a Reyleigh distribution, our only error model. */
} run_params_t;





static void*
run(void *rr)
{
     assert(rr != NULL);
     run_params_t *params = (run_params_t *) rr;

     /*! \todo Initialize seeds. */

     for (size_t j = params->start; j < params->start + params->slice; ++j) {
	  const uint16_t x = (uint16_t) (j % params->width);
	  const uint16_t y = (uint16_t) (j / params->width);
	  const double tagx = x;
	  const double tagy = y;
          double distance[params->N];

	  // precalculate real distances
	  for (size_t k = 0; k < params->N; ++k) {
	       distance[k] = trilateration_distance(params->anchor[k].x,
                                                    params->anchor[k].y,
                                                    tagx, tagy);
	  }

	  uint_fast64_t failures = 0U; // How often did it fail (nan)?
	  double M = 0.0, M_old, S = 0.0, cnt = 0.0;
	  double MSE = 0.0, MSE_old, C_MSE = 0.0;
	  double M_X = 0.0, M_X_old, S_X = 0.0, C_X = 0.0;
	  double M_Y = 0.0, M_Y_old, S_Y = 0.0, C_Y = 0.0;
	  double min_error = DBL_MAX, max_error = 0.0;

	  // Calculate every pixel runs times 
	  for (uint_fast64_t i = 0; i < params->times; ++i) {
	       // The results of the algorithm
	       tl_point3d result[1];
	       double d[params->N];

	       /* The only error model we support here is a Rayleigh
		* error distribution with mean 50.
		*/
	       for (size_t k = 0; k < params->N; ++k) {
		    d[k] = distance[k] + gsl_ran_rayleigh(params->rng,
                                                          params->sigma) - 1.0;
	       }

	       /*! \todo Need a test to pass a random seed when needed.
		* Cannot do this unconditionally, since other algorithms
		* might use these as their parameters.
		*/
	       params->algorithm(result, params->anchor, d, params->N, NULL);

	       /* Calculate error and update the statistics. */
	       const double error =
                    trilateration_distance(result[0].x, result[0].y, tagx, tagy);

	       if (!isnan(error)) {
		    max_error = fmax(error, max_error);
		    min_error = fmin(error, min_error);

		    cnt += 1.0F;
		    M_old = M;
		    M += (error - M) / cnt;
		    S += (error - M) * (error - M_old);
	       } else {
		    failures += 1;
	       }

	       double sqerror = error * error;
	       if (!isnan(sqerror)) {
		    C_MSE += 1.0F;
		    MSE_old = MSE;
		    MSE += (sqerror - MSE_old) / C_MSE;
	       }

	       if (!isnan(result[0].x) && !isnan(result[0].y)) {
		    C_X += 1.0F;
		    M_X_old = M_X;
		    const double dx = result[0].x - x;
		    M_X += (dx - M_X_old) / C_X;
		    S_X += (dx - M_X) * (dx - M_X_old);

		    C_Y += 1.0F;
		    M_Y_old = M_Y;
		    const double dy = result[0].y - y;
		    M_Y += (dy - M_Y_old) / C_Y;
		    S_Y += (dy - M_Y) * (dy - M_Y_old);
	       }
	  }

	  const size_t pos = (size_t) (x +  y * params->width);
	  params->results[AVERAGE_ERROR][pos] = M;
	  params->results[STANDARD_DEVIATION][pos] = sqrtf(S / (cnt - 1.0));
	  params->results[MAXIMUM_ERROR][pos] = fmax(max_error, 0.0);
	  params->results[MINIMUM_ERROR][pos] = fmin(min_error, DBL_MAX);
	  params->results[FAILURES][pos] =
	       ((double) failures) / ((double) params->times);
	  params->results[ROOT_MEAN_SQUARED_ERROR][pos] = sqrtf(MSE);
	  params->results[AVERAGE_X_ERROR][pos] = M_X;
	  params->results[STANDARD_DEVIATION_X_ERROR][pos] =
	       sqrtf(S_X / (C_X - 1.0));
	  params->results[AVERAGE_Y_ERROR][pos] = M_Y;
	  params->results[STANDARD_DEVIATION_Y_ERROR][pos] =
	       sqrtf(S_Y / (C_Y - 1.0)); 
     }

     struct rusage resources;
     getrusage(RUSAGE_THREAD, &resources);
     fprintf(stderr, "Thread %zu: %d.%06d sec.\n", params->id,
	     (int)resources.ru_utime.tv_sec, (int)resources.ru_utime.tv_usec);
     fflush(stderr);

     return NULL;
}





/*!
 * \brief Estimates the position for each place on the playing field.
 *
 * \param[in] alg        A number that indicates the position estimation
 *                       algorithm.
 * \param[in] em         A number that indicates the error model.
 * \param[in] no_anchors The number of anchor nodes to use.
 * \param[in] anchors    Array of anchors nodes of length [no_anchors].
 * \param[in] width      Width of the playing field.
 * \param[in] height     Height of the playing field.
 */
void __attribute__((__nonnull__))
distribute_work(trilateration_algorithm_t alg,
		const int num_threads, const int64_t times,
		const tl_point3d* anchor, const size_t N,
		double *results[NUM_VARIANTS],
		const int width, const int height)
{
     const size_t slice = (size_t) (width * height) / num_threads;
     pthread_t *thread;
     run_params_t *params;

     params = (run_params_t *) calloc(num_threads, sizeof(run_params_t));
     if (params == NULL) {
	  perror("calloc()");
	  exit(EXIT_FAILURE);
     }

     thread = (pthread_t *) calloc(num_threads, sizeof(pthread_t));
     if (thread == NULL) {
	  perror("calloc()");
	  exit(EXIT_FAILURE);
     }

     // Set up the parameters.
     for (int t = 0; t < num_threads; t++) {
	  params[t].id = t;
	  params[t].anchor = anchor;
	  params[t].N = N;
	  params[t].width = (uint16_t) width;
	  params[t].height = (uint16_t) height;
	  params[t].results = results;
	  params[t].start = (uint32_t) (t * slice);
	  params[t].slice = (uint32_t) slice;
	  params[t].times = (uint_fast64_t) times;
	  params[t].algorithm = alg;
	  params[t].rng = gsl_rng_alloc(gsl_rng_default);
	  params[t].sigma = 50.0 / sqrt(M_PI_2);
	  gsl_rng_set(params[t].rng, random());
     }

     /* Create the threads. */
     if (num_threads > 1) {
	  for (int t = 0; t < num_threads; t++) {
	       if (pthread_create(&thread[t], NULL, run, &params[t])) {
		    perror("pthread_create()");
		    exit(EXIT_FAILURE);
	       }
	  }

	  // Sync Threads if work has been done
	  for(int t = 0; t < num_threads; t++) {
	       pthread_join(thread[t], NULL);
	  }
     } else {
	  /* Since there is only one thread, we call the run code directly.
	   * This should make debugging simpler.
	   */
	  run(&(params[0]));
     }
     free(thread);
     free(params);
}





const char *argp_program_version = "spatial-evaluation " PACKAGE_VERSION;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;
static char doc[] = "spatial evaluation of trilateration algorithms.";

static struct argp_option options[] = {
     { "verbose", 'v', 0, 0, "produce verbose output", 0 },
     { "quiet", 'q', 0, 0, "don't produce any output", 0 },
     { "silent", 's', 0, OPTION_ALIAS, 0, 0 },
     { "output", 'o', "FILE", 0, "Output to FILE instead of spatial-evaluation.h5", 0 },
     { "times", 't', "NUM", 0, "Number of times to sample each point, default: 1000", 0 },
     { "threads", 'T', "NUM", 0, "Number of threads to use, default: 16", 0 },
     { "width", 'w', "NUM", 0, "Width of the playing field, defaults: 1000", 0 },
     { "height", 'h', "NUM", 0, "Height of the playing field, defaults: 1000", 0 },
     { 0, 0, 0, 0, 0, 0 }
};


struct arguments {
     int quiet, verbose;
     int width, height;
     int num_threads, times;
     char const *output;
};

static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
     struct arguments *arguments = (struct arguments *) state->input;

     switch (key) {
     case 'q': case 's':
          arguments->quiet = 1;
          break;
     case 'v':
          arguments->verbose = 1;
          break;
     case 't':
          arguments->times = strtol(arg, NULL, 10);
          break;
     case 'T':
          arguments->num_threads = strtol(arg, NULL, 10);
          break;
     case 'w':
          arguments->width = strtol(arg, NULL, 10);
          break;
     case 'h':
          arguments->height = strtol(arg, NULL, 10);
          break;
     case 'o':
          arguments->output = arg;
          break;
     case ARGP_KEY_ARG:
          argp_usage(state);
          break;
     case ARGP_KEY_END:
          break;
     default:
          return ARGP_ERR_UNKNOWN;
     }
     return 0;
}

static struct argp argp = { options, parse_opt, 0, doc, 0, 0, 0 };


/*!
 * This is a small demo application that mimics the functionality of
 * the lateration shooter. It is used mostly for bench-marking.
 */
int
main(int argc __attribute__((__unused__)),
     char **argv __attribute__((__unused__)))
{
     tl_point3d anchor[] = { { 150, 150, 0 },
                             { 250, 175, 0 },
                             { 325, 275, 0 },
                             { 350, 375, 0 },
                             { 175, 325, 0 } };
     const size_t N = sizeof(anchor) / (3 * sizeof(anchor[0]));
     trilateration_algorithm_t algorithm = trilateration_nlls_2d;
     double *result[NUM_VARIANTS];

     struct arguments arguments;
     arguments.quiet = 0;
     arguments.verbose = 0;
     arguments.width = 1000;
     arguments.height = 1000;
     arguments.num_threads = 16;
     arguments.times = 1000;
     arguments.output = "spatial-evaluation.h5";
     argp_parse(&argp, argc, argv, 0, 0, &arguments);
     gsl_rng_env_setup();

     for (int i = 0; i < NUM_VARIANTS; ++i) {
	  result[i] = (double *) calloc(arguments.width * arguments.height,
                                        sizeof(double));
     }

     distribute_work(algorithm, arguments.num_threads, arguments.times,
                     anchor, N, result, arguments.height, arguments.width);

     hdf5_write_locbased(arguments.output, anchor, N, result,
                         arguments.width, arguments.height);

     for (int i = 0; i < NUM_VARIANTS; ++i) {
	  free(result[i]);
     }

     return EXIT_SUCCESS;
}
