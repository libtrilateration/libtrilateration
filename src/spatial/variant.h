/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef VARIANT
#define VARIANT(tag, name, label)
#endif

VARIANT(AVERAGE_ERROR, "Average Distance Error", "Average_Error")
VARIANT(STANDARD_DEVIATION, "Standard Deviation of Distance Error", "Standard_Deviation")
VARIANT(MAXIMUM_ERROR, "Maximum Distance Error", "Maximum_Error")
VARIANT(MINIMUM_ERROR, "Minimum Distance Error", "Minimum_Error")
VARIANT(FAILURES, "Failures", "Failures")
VARIANT(ROOT_MEAN_SQUARED_ERROR, "Root Mean Squared Error", "Root_mean_squared_error")
VARIANT(AVERAGE_X_ERROR, "Average Error in X", "Average_X_Error")
VARIANT(AVERAGE_Y_ERROR, "Average Error in Y", "Average_Y_Error")
VARIANT(STANDARD_DEVIATION_X_ERROR, "Standard Deviation of X Deviation", "Standard_Deviation_X_Error")
VARIANT(STANDARD_DEVIATION_Y_ERROR, "Standard Deviation of Y Deviation", "Standard_Deviation_Y_Error")
