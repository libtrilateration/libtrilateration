

#if HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <stdio.h>
#include <stdlib.h>

extern char **trilateration_algorithm_2d_meta_data(void);

int
main(int argc __attribute__((__unused__)), char **argv __attribute__((__unused__)))
{
#if ENABLE_META_DATA
	int i = 0;
        char ** meta_data = trilateration_algorithm_2d_meta_data();
        printf("[\n");
	while (meta_data[i] != NULL) {
		printf("%s", meta_data[i]);
		i++;
                if (meta_data[i] != NULL) printf(",\n");
                else printf("\n");
	}
        printf("]\n");
        free(meta_data);
#else
	printf("[\"No configuration available\"]");
#endif
	return EXIT_SUCCESS;
}
