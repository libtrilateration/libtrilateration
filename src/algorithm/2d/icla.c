/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>



/* Merge the cluster j to the cluster i. */
#define icla_merge(i, j, merged, merge_list, ml_fill, weight, attracting_boundary) \
     do { \
          assert(i < R && j < R); \
          merged[j] = 1; \
	  assert(ml_fill[i] < R); \
          merge_list[i][ml_fill[i]] = j; \
	  ++ml_fill[i]; \
	  for (size_t l = 0; l < ml_fill[j]; ++l) { \
	       assert(ml_fill[i] < R); \
	       merge_list[i][ml_fill[i]] = merge_list[j][l]; \
	       ++ml_fill[i]; \
          } \
          weight[i] += weight[j]; \
          attracting_boundary[i] = \
	       fmax(attracting_boundary[i], attracting_boundary[j]); \
     } while (0)



/*!
 * Iterative Clustering-Based Localization Algorithm
 * 
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
 
trilateration_status
trilateration_icla_2d(tl_point3d *restrict result,
                      const tl_point3d *const anchor,
                      const double *const d, const size_t N, void *params)
{
     assert (result != NULL);
     const double alpha = (params != NULL) ? ((trilateration_icla_params *) params)->alpha : 0.5;
     const double move_step = (params != NULL) ? ((trilateration_icla_params *) params)->move_step : 1.5;

     /* Step 1: Calculate all circle intersections. */
     tl_point3d ii[N * (N - 1)];
     const size_t R = trilateration_circles_intersections(ii, N * (N - 1),
                                                          anchor, d, N);

     /* If there are no intersections, we give up. */
     if (R < 1) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     /* Step 2: adapt iterative clustering model (ICM). */
     int weight[R], merged[R];
     for (size_t i = 0; i < R; ++i) {
	  weight[i] = 1;
	  merged[i] = 0;
     }
     size_t merge_list[R][R], ml_fill[R], in_range[R][R], in_range_fill[R];
     for (size_t i = 0; i < R; ++i) {
	  ml_fill[i] = 0;
	  in_range_fill[i] = 0;
     }
     double cx[R], cy[R], attracting_boundary[R];
     for (size_t i = 0; i < R; ++i) {
	  cx[i] = X(ii, i);
	  cy[i] = Y(ii, i);
     }
     double direction_x[R], direction_y[R];

     /* ICM Step 1: Compute the initial radius of each point's attracting
      * boundary as longest distance from all other points.
      */
     for (size_t i = 1; i < R; ++i) {
	  for (size_t j = 0; j < i; ++j) {
	       attracting_boundary[i] = 0.0;
	       attracting_boundary[j] = 0.0;
	       const double dist =
		    trilateration_distance(X(ii, i), Y(ii, i), X(ii, j), Y(ii, j));
	       if (attracting_boundary[i] < dist) {
		    attracting_boundary[i] = dist;
	       }
	       if (attracting_boundary[j] < dist) {
		    attracting_boundary[j] = dist;
	       }
	  }
     }


     /* Iterative clustering. */
     int iterate, iterations = 1000;
     do {
	  /* ICM Step 2: Compute the moving directions of all nodes. */
	  for (size_t i = 0; i < R; ++i) {
	       if (merged[i]) {
		    continue;
	       }
	       int vector[3][3];
	       memset(vector, 0, sizeof(vector));
	       for (size_t j = 0; j < R; ++j) {
		    if (i != j && !merged[j]) {
			 const double dist =
			      trilateration_distance(cx[i], cy[i], cx[j], cy[j]);
			 if (fabs(dist) > DBL_EPSILON &&
			     dist <= attracting_boundary[i]) {
		              int dir_x = 0, dir_y = 0;
			      dir_x = (int) floor((cx[j] - cx[i]) / dist + .5);
			      dir_y = (int) floor((cy[j] - cy[i]) / dist + .5);
			      assert(-1 <= dir_x && dir_x <= 1);
			      assert(-1 <= dir_y && dir_y <= 1);
			      vector[dir_x + 1][dir_y + 1] += weight[j];
			 } /* else {
			      vector[1][1] += weight[j];
			      } */
		    }
		    size_t x = 0, y = 0;
		    for (size_t k = 0; k < 3; k++) {
			 for (size_t m = 0; m < 3; m++) {
			      if (vector[k][m] > vector[x][y]) {
				   x = k;
				   y = m;
			      }
			 }
		    }
		    direction_x[i] = (x == 0) ? -1.0 : ((x == 1) ? 0.0 : 1.0);
		    direction_y[i] = (y == 0) ? -1.0 : ((y == 1) ? 0.0 : 1.0);
	       }
	  }

	  /* ICM Step 3:move all points according to current direction
	   * on step forward.
	   */

	  /* Collect all nodes in range. */
	  for (size_t i = 0; i < R; ++i) {
	       if (!merged[i]) {
                    
		    in_range_fill[i] = 0;
		    for (size_t j = 0; j < R; ++j) {
			 if (merged[j]) {
			      continue;
			 }
			 if (i != j &&
			     trilateration_distance(cx[i], cy[i], cx[j], cy[j]) <= attracting_boundary[i]) {
			      in_range[i][in_range_fill[i]] = j;
			      ++in_range_fill[i];
			 }
		    }
	       }
	  }
	  /* Move the points. */
	  for (size_t i = 0; i < R; ++i) {
	       if (!merged[i] && fabs(attracting_boundary[i]) > DBL_EPSILON) {
		    const double scale = hypot(direction_x[i], direction_y[i]);
		    if (fabs(scale) > DBL_EPSILON) {
			 cx[i] += direction_x[i] * (move_step / scale);
			 cy[i] += direction_y[i] * (move_step / scale);
		    }
	       }
	  }

	  /* ICM Step 4: merge points.
	   */
	  for (size_t i = 0; i < R; ++i) {
	       if (merged[i] || in_range_fill[i] == 0) {
		    continue;
	       }
	       /* If only one node is in range and not in this nodes
		* attracting boundary, remove this node, set attracting
		* boundary to zero and stop moving.
		*/
	       if (in_range_fill[i] == 1) {
                    const size_t j = in_range[i][0];
		    const double dist =
			 trilateration_distance(cx[i], cy[i], cx[j], cy[j]);
		    if (merged[j] || (dist > attracting_boundary[j])) {
			 in_range_fill[i] = 0;
			 attracting_boundary[i] = 0.0;
		    } else {
			 icla_merge(i, j, merged, merge_list,
				    ml_fill, weight, attracting_boundary);
		    }
	       } else {
		    /* Test whether the points can be merged.
		     */
		    double d_short = DBL_MAX;
		    int p_short = -1;
		    for (size_t j = 0; j < in_range_fill[i]; j++) {
                         const size_t k = in_range[i][j];
			 if (merged[k]) {
			      continue;
			 }
			 double dist =
			      trilateration_distance(cx[i], cy[i], cx[k], cy[k]);

			 if (dist <= move_step * M_SQRT2) {
			      icla_merge(i, k, merged, merge_list,
					 ml_fill, weight, attracting_boundary);
			 }

			 if (dist < d_short) {
			      d_short = dist;
			      p_short = k;
			 }
		    }
		    /* Merge points when distance between them is the
		     * shortest and both points are in the attracting
		     * boundary.
		     */
		    if (p_short != -1 && !merged[p_short] &&
			in_range_fill[p_short] != 0) {
			 double d_short_2 = DBL_MAX;
			 int p_short_2 = -1;
			 for (size_t k = 0; k < in_range_fill[p_short]; k++) {
			      if (merged[in_range[p_short][k]]) {
				   continue;
			      }
			      double dist =
				   trilateration_distance(cx[p_short],
							  cy[p_short],
							  cx[in_range[p_short][k]],
							  cy[in_range[p_short][k]]);
			      if (dist < d_short_2) {
				   d_short_2 = dist;
				   p_short_2 = in_range[p_short][k];
			      }
			 }
			 if (p_short_2 == (int) i) {
			      // merge with i?
			 }
		    }
	       }
	  }

	  /* ICM Step 6: Update ranges of points whose range radii are not
	   * zero.
	   */
	  for (size_t i = 0; i < R; ++i) {
	       double d_max = 0.0, d_min = DBL_MAX;
	       if (merged[i] || attracting_boundary[i] == 0.0) {
		    continue;
	       }
	       for (size_t j = 0; j < in_range_fill[i]; ++j) {
                    const size_t k = in_range[i][j];
		    if (merged[k]) {
			 continue;
		    }
		    double dist =
			 trilateration_distance(cx[i], cy[i], cx[k], cy[k]);
		    d_max = fmax(d_max, dist);
		    d_min = fmin(d_min, dist);
	       }
	       attracting_boundary[i] =
		    (d_min >= DBL_MAX) ? 0.0 : d_min + ((d_max - d_min) / alpha);
	  }

	  /* ICM Step 5: Check termination condition. */
	  iterate = 0;
	  for (size_t i = 0; i < R; ++i) {
	       if (!merged[i] && attracting_boundary[i] != 0.0) {
		    iterate = 1;
		    break;
	       }
	  }
	  --iterations;
     } while (iterate && iterations > 0);


     /* Step 3: Return centroid of selected intersection points.
      *
      * For some reason, the selection is based on the original
      * location of the intersection points.
      */
     int max_cluster = -1;
     for (size_t i = 0; i < R; ++i) {
	  if (!merged[i]) {
	       if ((max_cluster == -1) || (ml_fill[i] > ml_fill[max_cluster])) {
		    max_cluster = i;
	       }
	  }
     }
     assert(max_cluster >= 0);
     const size_t l_count = ml_fill[max_cluster] + 1;
     assert (l_count > 0);
     tl_point3d l[l_count];
     memset(l, 0, sizeof(l));
     X(l, 0) = X(ii, max_cluster);
     Y(l, 0) = Y(ii, max_cluster);
     for (size_t i = 0; i < ml_fill[max_cluster]; ++i) {
	  X(l, i + 1) = X(ii, merge_list[max_cluster][i]);
	  Y(l, i + 1) = Y(ii, merge_list[max_cluster][i]);
     }
     return trilateration_centre_of_mass(result, l, l_count);
}


#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_icla_meta_data =
"{\n"
"  \"name\": \"Iterative Clustering Lateration Algorithm\",\n"
"  \"symbol\": \"trilateration_icla_2d\",\n"
"  \"description\": \"Estimates position by using the ICLA algorithm\",\n"
"  \"configurable\": true,\n"
"  \"configuration\": { \"alpha\": { \"type\": \"double\", \"minimum\": 0, \"description\": \"\" }, \"move_step\": { \"type\": \"double\", \"minimum\": 0, \"description\": \"\" } }\n"
"}";
#endif

/* Reference
 *
 * @Article{haiyong11,
 *   author = {Luo Haiyong and Li Hui and Zhao Fang and Peng Jinghua},
 *   title = {An Iterative Clustering-Based Localization Algorithm for Wireless
 *	      Sensor Networks},
 *   journal = {China Communications},
 *   year = {2011},
 *   volume = {8},
 *   pages = {58--64},
 *   number = {1},
 *   eid = {58},
 *   numpages = {6},
 *   publisher = {China Communications}
 * }
 */

