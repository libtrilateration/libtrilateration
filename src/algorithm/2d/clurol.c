/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>


void
trilateration_clurol_params_default(trilateration_clurol_params *params)
{
     assert(params != NULL);
     params->d_max = 0.05;
}




trilateration_status
trilateration_clurol_2d_init(void **params)
{
     if (params != NULL) {
          trilateration_clurol_params *p =
               malloc(sizeof(trilateration_clurol_params));
          if (p == 0) {
               perror(NULL);
               exit(EXIT_FAILURE);
          }
          trilateration_clurol_params_default(p);
          *params = p;
          return TL_OK;
     } else {
          return TL_ERROR;
     }
}

struct pairwise_distance_tuple {
     int x, y;
     double d;
};

typedef struct pairwise_distance_tuple pairwise_distance_tuple;


static int
compare_pairwise_distance_tuple(const void* a, const void *b)
{
     const pairwise_distance_tuple *_a = (pairwise_distance_tuple *) a,
                                   *_b = (pairwise_distance_tuple *) b;
     if (_a->d < _b->d) {
	  return TL_ERROR;
     } else if (_a->d > _b->d) {
	  return 1;
     }
     return TL_OK;
}


static int
cluster_size(const int *cx, size_t N, int c)
{
     int r = 0;
     for (size_t i = 0; i < N; i++) {
	  if (cx[i] == c) {
	       r++;
	  }
     }
     return r;
}



static int
cluster_to_array(tl_point3d *restrict result, const tl_point3d *const ii,
                 const int *ic, const size_t N, const int c)
{
     int k = 0;

     for (size_t m = 0; m < N; m++) {
	  if (ic[m] == c) {
	       X(result, k) = X(ii, m);
	       Y(result, k) = Y(ii, m);
	       k++;
	  }
     }
     return k;
}



static int 
find_max_cluster(pairwise_distance_tuple *D, int dlength, double dth,
		 const tl_point3d *const ii, int *restrict ic, size_t R)
{
     /* Build initial cluster */
     int max_cluster = 1;
     ic[D[0].x] = ic[D[0].y] = max_cluster;

     for (int i = 1; i < dlength; i++) {
	  const double dist = D[i].d;

	  if (ic[D[i].x] == 0 && ic[D[i].y] == 0) {
	       /* test first condition, paper lines 5..7 */
	       /* x and y not in any cluster, add to a new cluster */
	       max_cluster++;
	       ic[D[0].x] = ic[D[0].y] = max_cluster;
	  } else if (ic[D[i].x] != 0 && ic[D[i].y] == 0) {
	       /* test second condition, paper lines 8..12 */
	       /* x in cluster and y does not belong to any cluster */
	       if (dist <= dth) {
                    ic[D[i].y] = ic[D[i].x];
	       }
	  } else if (ic[D[i].x] == 0 && ic[D[i].y] != 0) {
	       /* test third condition, paper lines 13..17 */
	       /* y in cluster and x does not belong to any cluster */
	       if (dist <= dth) {
                    ic[D[i].x] = ic[D[i].y];
	       }
	  } else if (ic[D[i].x] != 0 && ic[D[i].y] != 0 &&
		     ic[D[i].x] != ic[D[i].y]) {
	       // test fourth condition, paper lines 18..26
	       // need to check if Cx and Cy can be merged
	       tl_point3d x[R], y[R];
	       size_t sx, sy;
	       sx = cluster_to_array(x, ii, ic, R, ic[D[i].x]);
	       sy = cluster_to_array(y, ii, ic, R, ic[D[i].y]);

	       tl_point3d xc[1], yc[1];
	       trilateration_centre_of_mass(xc, x, sx);
	       trilateration_centre_of_mass(yc, y, sy);

	       if (trilateration_distance(X(xc, 0), Y(xc, 0), X(yc, 0),
                                          Y(yc, 0)) <= dth) {
                    // Merge both sets, remove one set from list
                    for (size_t m = 0; m < R; m++) {
			 if (ic[m] == ic[D[i].y]) {
			      ic[m] = ic[D[i].x];
			 }
                    }
	       }
	  }
     }

     // return cluster with maximum cardinality as result
     int max_c = 1;
     int max_clustersize = cluster_size(ic, R, 1);
     for (int i = 2; i < max_cluster; i++) {
	  int x = cluster_size(ic, R, i);
	  if (x > max_clustersize) {
	       max_c = i;
	       max_clustersize = x;
	  }
     }
     return max_c;
}





/*!
 * Clustering Based Robust Localisation Algorithm (CluRoL).
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_clurol_2d(tl_point3d *restrict result,
			const tl_point3d *const anchor, const double *const d,
                        const size_t N, void *params)
{
     /* Step 0b: Obtain parameters from the environment, or set them
	to default values. */
     const trilateration_clurol_params *_params = params;
     const double d_max = (_params != NULL) ? _params->d_max : 0.05;

     /* Step 2: Calculate all intersection between the circles induced
	by the range measurement and anchors. */
     const size_t K = N * (N - 1);
     tl_point3d ii[K];
     int ic[K];
     memset(ic, 0, sizeof(ic));

     int R = trilateration_circles_intersections(ii, K, anchor, d, N);

     if (R == 0) {
	  /* If there is no intersection point, CluRoL does not work,
	   * so we return the solution of linear least squares.
	   */
	  return trilateration_lls_2d(result, anchor, d, N, NULL);
     } else if (R == 1) {
	  /* If there was exactly one intersection found, return this
	   * point as the location estimate.
	   */
          tl_point3d_set_point(result, ii + 0);
	  return TL_OK;
     }

     /* Do we have enough anchors? If N < 3, beta is less than 2 and
      * CluRoL fails.
      */
     if (N < 3) {
          tl_point3d_set(result, NAN, NAN, NAN);
          return TL_2FEW;
     }

     /* Order the intersection points by their pairwise distance. */
     pairwise_distance_tuple D[R * (R - 1) / 2];
     int num = 0;
     for (int i = 1; i < R; i++) {
	  for (int j = 0; j < i; j++) {
	       D[num].x = i;
	       D[num].y = j;
	       D[num].d = trilateration_distance(X(ii, i), Y(ii, i), X(ii, j),
                                                 Y(ii, j));
	       num++;
	  }
     }

     assert (num <= R * (R - 1) / 2);
     qsort(D, num, sizeof(pairwise_distance_tuple),
	   compare_pairwise_distance_tuple);

     /* Calculate the distance threshold as the n-th precintle
	pairs distance. */
     const int alpha = trilateration_binomial(((N + 1) / 2) + 2, 2);
     const int beta = trilateration_binomial(N, 2);
     const int nth = (num * trilateration_binomial(alpha, 2) + 1) / trilateration_binomial(beta, 2);
     const double dth = D[((nth < num) ? nth : num) - 1].d;

     /* Find the largest cluster for dth. */
     int max_cluster = find_max_cluster(D, num, dth, ii, ic, K);

     /* Determine anchors for minimum squared errors method. */
     const double bound_const = (1.0 + d_max) * (1.0 + d_max);
     int anchor_taken[N];
     size_t num_anchors_taken = 0;
     memset(anchor_taken, 0, sizeof(anchor_taken));
     for (size_t k = 0; k < K; k++) {
	  if (ic[k] != max_cluster) {
	       continue;
	  }
	  for (size_t i = 0; i < N; i++) {
	       const double ub = d[i] * bound_const;
	       const double lb = d[i] / bound_const;
	       const double dist = trilateration_distance(X(ii, k), Y(ii, k),
							  AX(i), AY(i));
	       if ((lb <= dist) && (dist <= ub)) {
		    anchor_taken[i] = 1;
	       }
	  }
     }

     /* Copy anchors and ranges into a new array and estimate the position
	by minimising the mse. */
     tl_point3d ta[N];
     double td[N];
     for (size_t i = 0; i < N; i++) {
	  if (anchor_taken[i]) {
	       X(ta, num_anchors_taken) = AX(i);
	       Y(ta, num_anchors_taken) = AY(i);
	       Z(ta, num_anchors_taken) = AZ(i);
	       td[num_anchors_taken] = d[i];
	       num_anchors_taken++;
	  }
     }
     assert (/* 0 <= num_anchors_taken && */ num_anchors_taken <= N);

     if (0 < num_anchors_taken) {
	  return trilateration_lls_2d(result, ta, td, num_anchors_taken, NULL);
     } else {
	  /* Clustering does not work if we did not select any anchor into
	     the set of anchors. */
	  return trilateration_lls_2d(result, anchor, d, N, NULL);
     }
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_clurol_meta_data =
"{\n"
"  \"name\": \"CLuRoL\",\n"
"  \"symbol\": \"trilateration_centroid_2d\",\n"
"  \"description\": \"Localisation by calculating the centre of mass of all circle intersections\",\n"
"  \"configurable\": true,\n"
"  \"configuration\": { \"d_max\": { \"type\": \"double\", \"description\": \"Maximum size for clustering\", \"unit\": \"m\", \"minimum\": 0, \"configurable\": true } },\n"
"  \"citation\": {\n"
"    \"type\": \"inproceedings\",\n"
"    \"author\": [\"Misra, Satyajayant\", \"Xue, Guoliang\"],\n"
"    \"title\": \"CluRoL: Clustering based Robust Localization in Wireless Sensor Networks\",\n"
"    \"booktitle\": \"Military Communications Conference, 2007. MILCOM 2007. IEEE\",\n"
"    \"year\": 2007,\n"
"    \"volume\": \"0\",\n"
"    \"pages\": \"1--7\",\n"
"    \"doi\": \"10.1109/MILCOM.2007.4454815\",\n"
"    \"publisher\": \"IEEE\"\n"
"  }\n"
"}";
#endif
