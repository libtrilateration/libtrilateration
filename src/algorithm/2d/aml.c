/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>


/*!
 * AML parameters.
 *
 * Allocates and initializes a block for parameters. The parameters shall
 * be released using free().
 */
trilateration_status
trilateration_aml_2d_init(void **params)
{
     if (params != NULL) {
          trilateration_aml_params *p =
               malloc(sizeof(trilateration_aml_params));
          if (p == NULL) {
               return TL_ERROR;
          }
          srand48_r(42, &(p->state));
          *params = p;
          return TL_OK;
     } else {
          return TL_ERROR;
     }
}



/*!
 * Adapted Multi-lateration
 *
 * This implementation is randomized. It will use a pseudo random
 * number generator to find a first pair of intersecting circles.
 * Thus, the results of this algorithm may also be random, depending
 * on the quality of the initial choice.
 *
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param params Unused.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_aml_2d(tl_point3d *restrict result,
                     const tl_point3d *const anchor,
		     const double *const d, const size_t N,
		     void *params)
{
     if (N < 2) {
          tl_point3d_set(result, NAN, NAN, NAN);
          return TL_2FEW;
     }

     tl_point3d ii[2];
     int r = -1;
     size_t ci, cj;

     assert(params != NULL);
     trilateration_aml_params *_params = (trilateration_aml_params *) params;


     /* Step 1: Randomly select two circles that intersect. */

     for (int c = 0; c < 100; c++) {
	  double s;
	  drand48_r(&(_params->state), &s);
	  ci = (int) floor(s * N);
	  assert (/* 0 <= ci && */ ci < N);
	  drand48_r(&(_params->state), &s);
	  cj = (int) floor(s * N);
	  assert (/* 0 <= cj && */ cj < N);
	  if (ci == cj) {
	       continue;
	  } else if (ci > cj) {
	       int t = ci;
	       ci = cj;
	       cj = t;
	  }
	  r = trilateration_circle_intersections(ii, anchor + ci, d[ci],
						 anchor + cj, d[cj]);
	  if (0 < r)
	       break;
     }
 
     /* No pair of circles that intersect was guessed after 100 tries,
	thus give up and return an error. */
     if (0 == r) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     /* Step 2: Elimination Phase */

     /* px and py refer to the closer intersection point. */
     tl_point3d p[1];
     tl_point3d_set_point(p, ii + 0);

     if (r > 1) {
	  /* Find an anchor k that differs from i and j */
	  size_t k = 0;
	  while (k < N && (k == ci || k == cj)) {
	       ++k;
	  }
	  /* If there is more than one intersection, eliminate one
	     intersection point. */
	  double d1 = trilateration_distance(AX(k), AY(k), X(ii, 0), Y(ii, 0)) - d[k];
	  double d2 = trilateration_distance(AX(k), AY(k), X(ii, 1), Y(ii, 1)) - d[k];
	  if (fabs(d1) >= fabs(d2)) {
               tl_point3d_set_point(p, ii + 1);
	  }
     }

     /* Step 3: Refinement Steps */

     for (size_t k = 0; k < N; k++) {
	  if (k == ci || k == cj)
	       continue;
	  double dX, dY;
	  double dR = trilateration_distance(X(p, 0), Y(p, 0), AX(k), AY(k));
          const double dRi = dR - d[k];
	  if (dR > d[k]) {
	       const double RR = (0.5 * dRi) / (dRi + d[k]);
	       dX = (AX(k) - X(p, 0)) * RR;
	       dY = (AY(k) - Y(p, 0)) * RR;
	  } else {
	       const double RR = fabs(0.5 * dRi) / d[k];
	       const double q = 1.0 - 2.0 * RR;
	       dX = ((X(p, 0) - AX(k)) * RR) / q;
	       dY = ((Y(p, 0) - AY(k)) * RR) / q;
	  }
	  X(p, 0) += dX;
	  Y(p, 0) += dY;
     }

     tl_point3d_set_point(result, p);
     return TL_OK;
}


#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_aml_meta_data =
"{\n"
"  \"name\": \"AML\",\n"
"  \"symbol\": \"trilateration_aml_2d\",\n"
"  \"description\": \"Localisation using the adapted multi lateration algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": { \"state\": { \"description\": \"State of the drand48 random number generator\", \"type\": \"drand48_data\", \"configurable\": false } },\n"
"  \"citation\": {\n"
"    \"type\": \"article\",\n"
"    \"author\": [\"Gulnur Selda Kuruogl\", \"Melike Erol\", \"Sema Oktug\"],\n"
"    \"title\": \"Localization in Wireless Sensor Networks with Range Measurement Errors\",\n"
"    \"journal\": \"Advanced International Conference on Telecommunications\",\n"
"    \"year\": 2009,\n"
"    \"volume\": \"0\",\n"
"    \"pages\": \"261-266\",\n"
"    \"address\": \"Los Alamitos, CA, USA\",\n"
"    \"doi\": \"10.1109/AICT.2009.51\",\n"
"    \"publisher\": \"IEEE Computer Society\"\n"
"  }\n"
"}";
#endif

