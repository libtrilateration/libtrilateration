/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>


static void
build_triangle(tl_point3d *restrict result, const tl_point3d *const ii,
               const unsigned int c[])
{
     for (int i = 0; i < 3; i++) {
             X(result, i) = X(ii, c[i]);
             Y(result, i) = Y(ii, c[i]);
             Z(result, i) = 0.0;
     }
}



/*!
 * Const algorithm.
 *
 * Returns the same location at the origin.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */ 
trilateration_status
trilateration_geo_3_2d(tl_point3d *restrict result,
                       const tl_point3d *const anchor, const double *const d,
                       const size_t N, void *params __attribute__((__unused__)))
{
     assert (result != NULL);
     if (N < 3) {
             tl_point3d_set(result, NAN, NAN, NAN);
             return TL_2FEW;
     }

      /* Calculate circle intersections. */
     assert (anchor != NULL);
     /* We will use the first 3 anchors only. */
     const size_t K = 3 * (3 - 1);
     tl_point3d ii[K];
     memset(ii, 0, sizeof(ii));
     size_t R = 0;
     for (size_t i = 1; i < 3; i++) {
	  for (size_t j = 0; j < i; j++) {
	       int r;
	       r = trilateration_circle_intersections(ii + R,
                                                      anchor + i, d[i],
                                                      anchor + j, d[j]);
	       if (r < 1) {
                       r = trilateration_circle_approximate_intersection(ii + R,
                                                                         anchor + i, d[i],
                                                                         anchor + j, d[j]);
	       }
	       R += r;
	  }
     }
     assert(3 <= R && R <= K);


     /* If there are at least 3 intersection points very close
      *	together, assume that there is no ranging error. Take one of
      *	them as a result.
      */
     for (size_t i = 1; i < R; ++i) {
	  size_t m = 1;
	  for (size_t j = 0; j < R; ++j) { // Why not i, because symmetry
	       if (i != j &&
		   trilateration_distance(X(ii, i), Y(ii, i), X(ii, j), Y(ii, j)) < 0.1) {
		    m++;
	       }
	  }
	  if (m >= 3) {
               tl_point3d_set(result, X(ii, i), Y(ii, i), 0.0);
	       return TL_OK;
	  }
     }

     /* Calculate triangle with minimum perimeter that also is inside of all
	the circles. */
     unsigned int c[3], smallest[3], smallest_in_c[3];
     tl_point3d t[6];
     double smallest_perimeter = DBL_MAX;
     double smallest_perimeter_in_circle = DBL_MAX;
     
     memset(t, 0, sizeof(t));
     memset(smallest, 0, sizeof(smallest));
     memset(smallest_in_c, 0, sizeof(smallest_in_c));

     trilateration_first_combination(c, 3, R);
     do {
	  build_triangle(t, ii, c);
	  const double p = trilateration_triangle_perimeter(t);
	  if (p < smallest_perimeter) {
	       smallest_perimeter = p;
	       memcpy(smallest, c, sizeof(smallest));
	  }
	  /* Is the triangle in all circles? */
	  if ((p < smallest_perimeter_in_circle) &&
	      trilateration_point_in_discs_p(t + 0, anchor, d, 3) &&
	      trilateration_point_in_discs_p(t + 1, anchor, d, 3) &&
              trilateration_point_in_discs_p(t + 2, anchor, d, 3)) {
	       smallest_perimeter_in_circle = p;
	       memcpy(smallest_in_c, c, sizeof(smallest_in_c));
	  }
     } while (trilateration_next_combination(c, 3, R));

     /* Small optimisation: If the center of mass of the smallest triangle
	is in the centre of the triabgle formed by the anchor nodes, return
	this centre. */
     tl_point3d cc[1], ac[1];
     build_triangle(t, ii, smallest);
     trilateration_centre_of_mass(cc, t, 3);
     trilateration_centre_of_mass(ac, anchor, 3);
     const double icr = 2.0 * trilateration_triangle_area(anchor) /
	  trilateration_triangle_perimeter(anchor);
     if (trilateration_distance(X(ac, 0), Y(ac, 0), X(cc, 0), Y(cc, 0)) < icr / 2.0) {
             tl_point3d_set(result, X(cc, 0), Y(cc, 0), 0.0);
             return TL_OK;
     }

     /* If the minimum triangle does not lie in all circles, refine the
	triangle a bit more. */
     if (!isinf(smallest_perimeter_in_circle)) {
	  if (memcmp(smallest, smallest_in_c, sizeof(smallest)) != 0) {
	       build_triangle(t, ii, smallest);
	       build_triangle(t + 3, ii, smallest_in_c);
	       const double t1 = trilateration_triangle_area(t);
	       const double t2 = trilateration_triangle_area(t + 3);
	       if (t2 < 2.5 * t1) { /* Why actually 2.5? */
		    memcpy(smallest, smallest_in_c, sizeof(smallest));
	       } else {
		    double w[6];
		    w[0] = w[1] = w[2] = 1.0 / t1;
		    w[3] = w[4] = w[5] = 1.0 / t2;
		    return trilateration_weighted_geometric_median(result, t, w, 6);
	       }
	  }
     }
     build_triangle(t, ii, smallest);
     return trilateration_geometric_median(result, t, 3);
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_geo_3_meta_data =
"{\n"
"  \"name\": \"Geo-3\",\n"
"  \"symbol\": \"trilateration_geo_3_2d\",\n"
"  \"description\": \"Estimates position by using the Geo 3 algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
