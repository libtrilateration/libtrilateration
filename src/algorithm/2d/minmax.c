/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <math.h>
#include <stdlib.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>

/*!
 * Bounding Box algorithm
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_minmax_2d(tl_point3d *restrict result,
                        const tl_point3d *const anchor, const double *const d,
                        const size_t N, void *params __attribute__((__unused__)))
{
     assert (result != NULL);
     assert (anchor != NULL);
     assert (d != NULL);

     if (N < 1) {
          tl_point3d_set(result, NAN, NAN, NAN);
          return TL_2FEW;
     }

     tl_point3d g[4];

     trilateration_bounding_box_2d(g, anchor, d, N);
     return trilateration_centre_of_mass(result, g, 4);
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_minmax_meta_data =
"{\n"
"  \"name\": \"MINMAX\",\n"
"  \"symbol\": \"trilateration_minmax_2d\",\n"
"  \"description\": \"Estimates position by using the MINMAX algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
