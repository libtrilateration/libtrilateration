/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>




/*!
 * \fn trilateration_trilateration_2d
 *
 * The traditional trilateration algorithm.
 *
 * The algorithm uses exactly three anchor positions and three
 * distances and estimates a position. It proved the correct position
 * if the three anchor positions are not colinear and there are no
 * measurement errors.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 *
 * \todo Not robust, fails with division by zero if ax[1] == ax[0]
 */
trilateration_status
trilateration_trilateration_2d(tl_point3d *restrict result,
			       const tl_point3d *const anchor,
			       const double *const d, const size_t N,
			       void *params  __attribute__((__unused__)))
{
  assert (result != NULL);

  if (N < 3) {
       tl_point3d_set(result, NAN, NAN, NAN);
      return TL_2FEW;
    }

  assert (anchor != NULL && d != NULL);

  if (trilateration_collinear_p(anchor, N) != 0) {
       tl_point3d_set(result, NAN, NAN, NAN);
      return -2;      
  }

  if (fabs(AX(1) - AX(0)) < 1e-5) {
       tl_point3d ta[3*3];
       double td[3];
       X(ta, 0) = X(anchor, 2); Y(ta, 0) = Y(anchor, 2); td[0] = d[2];
       X(ta, 1) = X(anchor, 1); Y(ta, 1) = Y(anchor, 1); td[1] = d[1];
       X(ta, 2) = X(anchor, 0); Y(ta, 2) = Y(anchor, 0); td[2] = d[0];

       return trilateration_trilateration_2d(result, ta, td, 3, NULL);
  }

  const double ps = AX(2) * AX(2) - AX(1) * AX(1) +
       AY(2) * AY(2) - AY(1) * AY(1);
  const double pt = AX(0) * AX(0) - AX(1) * AX(1) +
       AY(0) * AY(0) - AY(1) * AY(1);
  const double bs = (AX(1) - AX(2)) * (AY(0) - AY(1)) -
       (AY(2) - AY(1)) * (AX(1) - AX(0));

  const double s = (ps + d[1] * d[1] - d[2] * d[2]) / 2.0;
  const double t = (pt + d[1] * d[1] - d[0] * d[0]) / 2.0;
  double y = (t * (AX(1) - AX(2)) - s * (AX(1) - AX(0))) / bs;
  double x = (y * (AY(0) - AY(1)) - t) / (AX(1) - AX(0));
  tl_point3d_set(result, x, y, 0.0);
  return TL_OK;
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_trilateration_meta_data =
"{\n"
"  \"name\": \"Trilateration\",\n"
"  \"symbol\": \"trilateration_trilateration_2d\",\n"
"  \"description\": \"Estimates position by trilateration\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
