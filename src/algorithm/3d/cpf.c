/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/algebra.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>


/*!
 * CPF parameters.
 *
 * Allocates and initializes a block for parameters. The parameters shall
 * be released using free().
 */
trilateration_status
trilateration_cpf_3d_init(void **params)
{
     if (params != NULL) {
          trilateration_cpf_params *p =
               malloc(sizeof(trilateration_cpf_params));
          if (p == NULL) {
               return TL_ERROR;
          }
          tl_point3d_set(&(p->pre), NAN, NAN, NAN);
          srand48_r(42, &(p->state));
          *params = p;
          return TL_OK;
     } else {
          return TL_ERROR;
     }
}



/*!
 * Constrained Particle Filter without anchor selection.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_cpf_3d(tl_point3d *restrict result,
                     const tl_point3d *const anchor, const double *const d,
                     const size_t N, void *params)
{
     assert (params != NULL);
     trilateration_cpf_3d_params *_params = (trilateration_cpf_3d_params *) params;

     /* Number of particles to use. */
     const size_t particles = 50;

     /* Belief in the measurements. */
     const double tau = 0.8;

     /* A non-negative bias. This is not the bias of the ranging error. */
     const double bias = 0.5;

     tl_point3d p[particles];
     double pw[particles];

     /* Sample and constrain the particles. */
     do {
         tl_point3d b[8];
         trilateration_bounding_box_3d(b, anchor, d, N);
         if (isnan(_params->pre.x) || isnan(_params->pre.y)) {
              tl_point3d pp[1];
              trilateration_centre_of_mass_3d(pp, b, 8);
              tl_point3d_set(&(_params->pre), X(pp, 0), Y(pp, 0), Z(pp, 0));
         }
         for (size_t i = 0; i < particles; ++i) {
              double r;
              /* bx[0] is west, bx[1] is east, by[0] is north, by[2] is south.
               */
              drand48_r(&(_params->state), &r);
              X(p, i) = X(b, 0) + r * (X(b, 1) - X(b, 0));
              drand48_r(&(_params->state), &r);
              Y(p, i) = Y(b, 0) + r * (Y(b, 2) - Y(b, 0));
              drand48_r(&(_params->state), &r);
              Z(p, i) = Z(b, 0) + r * (Z(b, 4) - Z(b, 0));
         }
     } while (0);

     /* weight particles. */
     memset(pw, 0, sizeof(pw));
     for (size_t j = 0; j < N; ++j) {
          const double t = trilateration_distance_3d(&(_params->pre),
                                                     anchor + j);
          const double r = t * tau + (d[j] - bias) * (1.0 - tau);
          for (size_t i = 0; i < particles; ++i) {
               const double s = trilateration_distance_3d(p + i, anchor + j);
               pw[i] += (r - s) * (r - s);
          }
     }

     /* Return particle with minimum weight */
     size_t best = 0;
     double weight = pw[0];
     for (size_t i = 1; i < particles; ++i) {
          if (weight > pw[i]) {
              weight = pw[i];
              best = i;
          }
     }

     /* Return the result. */
     tl_point3d_set(&(_params->pre), X(p, best), Y(p, best), Z(p, best));
     tl_point3d_set(result, X(p, best), Y(p, best), Z(p, best));
     return TL_OK;
}


#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_cpf_3d_meta_data =
"{\n"
"  \"name\": \"Constraint Particle Filter\",\n"
"  \"symbol\": \"trilateration_cpf_3d\",\n"
"  \"description\": \"Estimates position by using a constraint particle filter\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": { \"pre_x\" : { \"type\": \"double\", \"description\": \"Previous x coordinate\", \"configurable\": false }, \"pre_y\" : { \"type\": \"double\", \"description\": \"Previous y coordinate\", \"configurable\": false }, \"state\": { \"description\": \"State of the drand48 random number generator\", \"type\": \"drand48_data\", \"configurable\": false } }\n"
"}";
#endif
