/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>


/*!
 * Centroid algorithm.
 *
 * This implementation uses a constant amount of memory, and is more
 * stable, because it uses Welford's method to compute the centre of
 * mass. For the same reason it is slower, because it needs $O(N^2)$
 * division operations.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_weighted_centroid_3d(tl_point3d *restrict result,
                                   const tl_point3d *const anchor,
				   const double *const d, const size_t N,
                                   void *params)
{
     double (*weighter)(const tl_point3d *const, const double*, const size_t, const tl_point3d *) =
          params;
     assert (anchor != NULL && d != 0 && N > 0);
     assert (result != NULL);

     double M_x = 0.0, M_y = 0.0, M_z = 0.0, n = 0.0;
     for (size_t i = 1; i < N; i++) {
	  for (size_t j = 0; j < i; j++) {
	       tl_point3d res[2];
               double weight;
	       int r;
	       r = trilateration_circle_intersections(res, anchor + i, d[i],
                                                      anchor + j, d[j]);
	       for (int k = 0; k < r; k++) {
		    if (weighter != NULL) {
			 weight = weighter(anchor, d, N, res + k);
		    } else {
			 weight = 1.0;
		    }
		    n += weight;
		    M_x += (weight * X(res, k));
		    M_y += (weight * Y(res, k));
		    M_z += (weight * Z(res, k));
	       }
	  }
     }

     /* If all circles do not intersect anywhere, return an error. */
     if (fabs(n) > 1e-5) {
          tl_point3d_set(result, M_x / n, M_y / n, M_z / n);
	  return TL_OK;
     } else {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;      
     }
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_weighted_centroid_meta_data =
"{\n"
"  \"name\": \"Weighted centroid\",\n"
"  \"symbol\": \"trilateration_weighted_centroid_2d\",\n"
"  \"description\": \"Estimates position by calculating a weighted centre of mass of circle intersections.\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
