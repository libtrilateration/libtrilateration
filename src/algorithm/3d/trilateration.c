/*
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>


/*!
 * \fn trilateration_trilateration_3d
 *
 * The traditional trilateration algorithm.
 *
 * The algorithm uses exactly three anchor positions and three
 * distances and estimates a position. It proved the correct position
 * if the three anchor positions are not colinear and there are no
 * measurement errors.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 *
 * \todo Not robust, fails with division by zero if ax[1] == ax[0]
 */
trilateration_status
trilateration_trilateration_3d(tl_point3d *restrict result,
                               const tl_point3d *restrict anchor,
			       const double *const d, const size_t N,
			       void *params  __attribute__((__unused__)))
{
  assert (result != NULL);

  if (N < 4) {
          tl_point3d_set(result, NAN, NAN, NAN);
          return TL_ERROR;
    }

    return trilateration_lls_3d(result, anchor, d, 4, NULL);
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_trilateration_3d_meta_data =
"{\n"
"  \"name\": \"Trilateration\",\n"
"  \"symbol\": \"trilateration_trilateration_3d\",\n"
"  \"description\": \"Estimates position by trilateration\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
