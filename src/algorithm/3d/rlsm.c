/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/algebra.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>


static inline size_t
to_index(const size_t row, const size_t col, const size_t N)
{
     assert(row != col);
     if (row < col) {
	  return row * (N - 1) - (row - 1) * ((row - 1) + 1) / 2 + col - row - 1;
     } else {
	  return col * (N - 1) - (col - 1) * ((col - 1) + 1) / 2 + row - col - 1;
     }
}


static inline size_t
robust_filter(tl_point3d *restrict ii, const size_t N)
{
     assert (N > 1);
     const size_t M = N * (N - 1) / 2;
     double d[M];
     for (size_t i = 0; i < N - 1; ++i) {
	  for (size_t j = i + 1; j < N; ++j) {
	       const size_t idx = to_index(i, j, N);
	       assert (/* 0 <= idx && */ idx < M);
	       d[idx] = trilateration_distance_3d(ii + i, ii + j);
	  }
     }

     const double median_value = 2.0 * trilateration_fmedian(d, M);
     tl_point3d f[N];
     size_t filtered = 0;
     for (size_t i = 1; i < N; ++i) {
	  size_t drop_count = 0;
	  for (size_t j = 0; j < N; ++j) {
	       if (i != j && d[to_index(i, j, N)] >= median_value) {
		    ++drop_count;
	       }
	  }
	  if (drop_count <= N / 2) {
	       X(f, filtered) = X(ii, i);
               Y(f, filtered) = Y(ii, i);
	       Z(f, filtered) = Z(ii, i);
	       ++filtered;
	  }
     }
     assert (filtered > 0);

     memcpy(ii, f, 3 * sizeof(double) * filtered);
     return filtered;
}


/*!
 * RLSM
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
 
trilateration_status
trilateration_rlsm_3d(tl_point3d *restrict result,
                      const tl_point3d *const anchor,
                      const double *const d, const size_t N, void *params)
{
     /* Minimum set size. */
     const size_t S = 3;

     /* Choice of algorithm. */
     trilateration_algorithm_t algorithm = (params != NULL) ? ((trilateration_rlsm_3d_params *) params)->algorithm : trilateration_lls_3d;
     void *algorithm_params = (params != NULL) ? ((trilateration_rlsm_3d_params *) params)->params : NULL;
     size_t i_limit = (params != NULL) ? ((trilateration_rlsm_params *) params)->i_limit : 0;

     assert (result != NULL);
     if (N <= S) {
          return algorithm(result, anchor, d, N, algorithm_params);
     }

     /* Calculate 2^N, since there are this many different combinations
	of anchors. */
     if (N >= 24) {
	  /* We just cannot calculate this number of combinations. It is
	     better to just give up! */
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     /* We can omit the permutations C(N, 1) and C(N, 2) */
     const size_t K = (1 << N) -
	  (trilateration_binomial(N, 1) + trilateration_binomial(N, 2));
     tl_point3d ii[K];
     memset(ii, 0, sizeof(ii));
     size_t i_count = 0;

     for (size_t k = S; k < N; ++k) {
	  unsigned int combination[k];
	  trilateration_first_combination(combination, k, N);
	  do {
	       tl_point3d ta[3];
               double td[k];
	       for (size_t i = 0; i < k; ++i) {
		    X(ta, i) = AX(combination[i]);
		    Y(ta, i) = AY(combination[i]);
		    Z(ta, i) = AZ(combination[i]);
		    td[i] = d[combination[i]];
	       }
	       int r = algorithm(result, ta, td, k, algorithm_params);
	       if (r == 0 && !isnan(RX) && !isnan(RY) && !isnan(RZ)) {
		    assert(i_count < K);
		    X(ii, i_count) = RX;
		    Y(ii, i_count) = RY;
		    Z(ii, i_count) = RZ;
		    ++i_count;
	       }
	  } while (trilateration_next_combination(combination, k, N));
     }
     if (i_count == 0) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     /* Apply the robust median filter to the intermediate position
      * estimates.
      *
      * If we have too many intersection points, we reduce the count
      * to some sensible value.
      */
     if (i_limit > 0 && i_count > i_limit) {
          /* TODO: Should we shuffle the intersection points? */
          i_count = i_limit;
     }
     i_count = robust_filter(ii, i_count);

     return trilateration_geometric_median_3d(result, ii, i_count);
}

#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_rlsm_3d_meta_data =
"{\n"
"  \"name\": \"RLSM\",\n"
"  \"symbol\": \"trilateration_rlsm_3d\",\n"
"  \"description\": \"Estimates position by using the RLSM algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
/* REFERENCE:
 * "Hybrid RSS-RTT Localization Scheme for Indoor
 * Wireless Networks", A. Bahillo, S.Mazuelas, R. M. Lorenzo, P. Fernandez,
 * J. Prieto, R. J. Duran and E. J. Abril, 2010.
 */
