/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>


static inline size_t __attribute__((__always_inline__,__nonnull__))
trilateration_minimum_circle_containment(tl_point3d *restrict ii,
                                         double *restrict iw,
                                         const tl_point3d *const anchor,
					 const double *const d,
					 const size_t N,
					 const size_t K,
					 const size_t M)
{
     tl_point3d vv[K];
     double vw[K];
     size_t r = 0;

     memset(vv, 0, sizeof(vv));

     for (size_t i = 0; i < K; i++) {
	  size_t m = 0;
	  for (size_t j = 0; j < N; j++) {
	       if (trilateration_distance(X(ii, i), Y(ii, i), AX(j), AY(j)) <= d[j] + 1e-2) {
		    m++;
	       }
	  }
          if (m >= M || fabs(iw[i] - 1.0) < 1e-5) {
	       X(vv, r) = X(ii, i);
	       Y(vv, r) = Y(ii, i);
	       vw[r] = iw[i];
	       r++;
	  }
     }

     memcpy(ii, vv, sizeof(ii[0]) * r * 3);
     memcpy(iw, vw, sizeof(iw[0]) * r);
     return r;
}



/*!
 * The Geo-n algorithm.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_geo_n_3d(tl_point3d *restrict result,
		       const tl_point3d *const anchor,
		       const double *const d, const size_t N,
		       void *params __attribute__((__unused__)))
{
     assert (result != NULL);
     if (N < 3) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     /* Calculate circle intersections. */
     assert (anchor != NULL && d != NULL);
     const size_t K = N * (N - 1);
     tl_point3d ii[K];
     double iw[K];
     size_t k = 0;
     for (size_t i = 1; i < N; i++) {
	  for (size_t j = 0; j < i; j++) {
	       int r;
	       r = trilateration_circle_intersections(ii + k,
                                                      anchor + i, d[i],
                                                      anchor + j, d[j]);
	       if (r > 0) {
                    for (int m = 0; m < r; m++) {
		        iw[k + m] = 3.0;
		    }
	       } else {
		    r = trilateration_circle_approximate_intersection(ii + k,
                                                                      anchor + i, d[i],
                                                                      anchor + j, d[j]);
		    iw[k] = 1.0;
                    assert (r == 1);
	       }
	       k += r;
	  }
     }

     /* Filter intersection points; only keep intersection points
      * which are contained in anchor length - 2 circrles.
      */
     const size_t R =
	  trilateration_minimum_circle_containment(ii, iw, anchor, d, N, k,
                                                   N - 2);

     /* If there are at least N * (N - 1) / 2 points very close together, assume
      *	that there is no ranging error. Take one of them as a result.
      */
     for (size_t i = 0; i < R; i++) {
	  size_t m = 0;
	  for (size_t j = 0; j < R; j++) {
	       if (i != j &&
		   trilateration_distance(X(ii, i), Y(ii, i), X(ii, i), Y(ii, i)) < 0.1) {
		    m++;
	       }
	  }
	  if (m >= (N * (N - 1)) / 2) {
               tl_point3d_set(result, X(ii, i), Y(ii, i), Z(ii, i));
	       return TL_OK;
	  }
     }

     /* If there are at most three points left, return the centre of mass
	of those three points. */
     if (R < 3) {
	  return trilateration_weighted_centre_of_mass(result, ii, iw, R);
     }

     /* Apply median filter on remaining points. */
     double id[R];
     memset(id, 0, sizeof(id[0]) * R);

     for (size_t i = 0; i < R; i++) {
	  for (size_t j = 0; j < R; j++) {
	       if (i != j) {
		    id[i] += trilateration_distance(X(ii, i), Y(ii, i), X(ii, j), Y(ii, j));
	       }
	  }
     }

     const double median = trilateration_fmedian(id, R);

     tl_point3d vv[R];
     double vw[R];
     int s = 0;
     
     for (size_t i = 0; i < R; i++) {
	  if (id[i] <= median * 1.0) { // Median factor.
	       X(vv, s) = X(ii, i);
	       Y(vv, s) = Y(ii, i);
	       vw[s] = iw[i];
	       s++;
	  }
     }
     return trilateration_weighted_centre_of_mass(result, vv, vw, s);
}


#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_geo_n_meta_data =
"{\n"
"  \"name\": \"Geo-n\",\n"
"  \"symbol\": \"trilateration_geo_3_2d\",\n"
"  \"description\": \"Estimates position by using the Geo n algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
