/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>


/*!
 * 
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
 
trilateration_status
trilateration_eminmax_w4_3d(tl_point3d *restrict result,
			    const tl_point3d *const anchor,
			    const double *const d, const size_t N,
			    void *params __attribute__((__unused__)))
{
     assert (anchor != NULL);
     assert (d != NULL);
     assert (result != NULL);

     tl_point3d p[8];
     trilateration_bounding_box_3d(p, anchor, d, N);

     /* Calculate W4 weights. */
     double w[8];
     for (size_t j = 0; j < 8; j++) {
	  w[j] = 0.0;
	  for (size_t i = 0; i < N; i++) {
	       double residual =
		    trilateration_distance_squared_3d(p + j, anchor + i) -
                    d[i] * d[i];
	       w[j] += residual * residual;
	  }
          if (fabs(w[j]) >= DBL_MIN) {
	       w[j] = 1.0 / w[j];
          } else {
               /* This position does not have any residual error. Thus,
                * return this position.
                */
               tl_point3d_set(result, X(p, j), Y(p, j), Z(p, j));
               return TL_OK;
          }
     }

     return trilateration_weighted_centre_of_mass_3d(result, p, w, 8);
}


#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_eminmax_w4_3d_meta_data =
"{\n"
"  \"name\": \"Extended MINMAX (Weight 4)\",\n"
"  \"symbol\": \"trilateration_eminmax_w4_3d\",\n"
"  \"description\": \"Estimates position by using an extended MINMAX with fourth weighting function\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
