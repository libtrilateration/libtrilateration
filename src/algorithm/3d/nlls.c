/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/algebra.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>




/*!
 * Linear least squares solver.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_nlls_3d(tl_point3d *restrict result,
                      const tl_point3d *const anchor, const double *const d,
                      const size_t N, void *params __attribute__((__unused__)))
{
     trilateration_algorithm_t algorithm =
          (params != NULL) ? params : trilateration_lls_3d;
     
     if (N < 1) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     /* Select a starting point for the optimisation. Here, we chose the
	resuult of linear least squares. One should select multiple 
	starting points to avoid returning local optima. */
     algorithm(result, anchor, d, N, NULL);

     int iterations = 0;
     double e1 = trilateration_square_residual_error_3d(anchor, d, N, result);
     do {
	  double e0, b[N], a[N * 3];
	  for (size_t i = 0; i < N; i++) {
	       const double dist = trilateration_distance_3d(result, anchor + i);
	       if (dist < 1e-5) {
		    /* If dist is close to zero, it might become
		       unstable.  If dist is 0, we cannot formulate
		       the equation system. */
		    goto next;
	       }
	       a[C(i, 0, N, 3)] = (RX - AX(i)) / dist;
	       a[C(i, 1, N, 3)] = (RY - AY(i)) / dist;
	       a[C(i, 2, N, 3)] = (RZ - AZ(i)) / dist;
	       b[C(i, 0, N, 1)] = (d[i] - dist) +
		    (a[C(i, 0, N, 3)] * RX + a[C(i, 1, N, 3)] * RY +
                     a[C(i, 2, N, 3)] * RZ);
	  }
          double s[3];
	  trilateration_solve_linear_equation_3d(s, a, b, N);
          tl_point3d_set(result, s[0], s[1], s[2]);
          e0 = e1;
	  e1 = trilateration_square_residual_error_3d(anchor, d, N, result);
	  if (e0 - e1 < 1e-5) {
	       break;
	  }
	  iterations++;
     } while (iterations < 100);

next:
     return TL_OK;
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_nlls_3d_meta_data =
"{\n"
"  \"name\": \"Non-linear Least Squares\",\n"
"  \"symbol\": \"trilateration_nlls_3d\",\n"
"  \"description\": \"Estimates position by using the non-linear least squares algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
