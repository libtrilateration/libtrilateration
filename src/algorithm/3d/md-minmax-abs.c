/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>





 /*!
  * MD MinMax algorithm.
  *
  * This version uses a trapezoid membership function and assumes a
  * relative error.
  *
  * \param ax An array that contains all x coordinates of the anchors.
  * \param ay An array that contains all y coordinates of the anchors.
  * \param d An array that contains the distance to the corresponding
  * anchor.
  * \param N the size of the arrays \c ax, \c ay, and \c
  * d. Each array should be larger than N. N should be larger than 2.
  * \param rx A pointer to the resulting x coordinate.
  * \param ry A pointer to the resulting y coordinate.
  *
  * \returns 0 if a location was computed, -1 if there was an error.
  */ 
trilateration_status
trilateration_md_minmax_abs_3d(tl_point3d *restrict result,
                               const tl_point3d *const anchor,
			       const double *const d, const size_t N,
			       void *params)
{
     assert (result != NULL);
     assert (anchor != NULL);
     assert (d != NULL);

#define GET(i, d) ((params != NULL) ? ((double*) params)[i] : d)
     const double left         = GET(0, -50.0);
     const double middle_left  = GET(1,  50.0);
     const double middle_right = GET(2,  50.0);
     const double right        = GET(3, 150.0);

     const double left_rate   = (1.0 - 0.0) / (middle_left - left);
     const double left_const  = -1.0 * left_rate * left;
     const double right_rate  = (0.0 - 1.0) / (right - middle_right);
     const double right_const = -1.0 * right_rate * right;

     tl_point3d g[8];
     double gw[8];
     trilateration_bounding_box_3d(g, anchor, d, N);

     for (int i = 0; i < 8; i++) {
	  double M = 0.0, S = 0.0, k = 1.0;
	  for (size_t j = 0; j < N; j++) {
	       double x = d[i] - trilateration_distance_3d(anchor + j, g + i);
	       double down = left_rate * x + left_const;
	       double up   = right_rate * x + right_const;
	       double t = fmax(fmin(fmin(down, up), 1.0), DBL_EPSILON);

	       double M_old = M;
	       M += (t - M_old) / k;
	       S += (t - M_old) * (t - M);
	       k += 1.0;
	  }
	  double sdev = sqrt(S / (k - 1.0));
	  gw[i] = M / fmax(sdev, DBL_EPSILON);
     }

     return trilateration_weighted_centre_of_mass_3d(result, g, gw, 8);
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_md_minmax_abs_3d_meta_data =
"{\n"
"  \"name\": \"Membership Degree MINMAX with absolute errors\",\n"
"  \"symbol\": \"trilateration_md_minmax_abs_3d\",\n"
"  \"description\": \"Estimates position by using the MD-MINMAX algorithm for absolute errors\",\n"
"  \"configurable\": true,\n"
"  \"configuration\": {\n"
"    \"left\": { \"type\": \"double\", \"configurable\": true, \"description\": \"\" },\n"
"    \"middle_left\": { \"type\": \"double\", \"configurable\": true, \"description\": \"\" },\n"
"    \"middle_right\": { \"type\": \"double\", \"configurable\": true, \"description\": \"\" },\n"
"    \"right\": { \"type\": \"double\", \"configurable\": true, \"description\": \"\" } } \n"
"}";
#endif
