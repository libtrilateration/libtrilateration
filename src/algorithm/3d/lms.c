/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/algebra.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>




/*!
 * Least median of squares solver.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 * \param params A pointer a drand48_data value holding the state
 * of a random number generator.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_lms_3d(tl_point3d *restrict result,
                     const tl_point3d *restrict anchor, const double *const d,
                     const size_t N, void *params)
{
     if (N < 1) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     assert (params != NULL);
     struct drand48_data *buffer = (struct drand48_data *) params;

     /* The subset size. */
     const size_t K = 4;

     /* There is only one subset, so just do linear least squares. */
     if (N < K) {
	  return trilateration_lls_3d(result, anchor, d, N, NULL);
     }

     /* Number of subsets to chose. Note that C(6, 4) == 15 and C(7,
      * 4) == 35. Below, we should select 4 element subsets of
      * 1..N. For N <= 6, we can just use the combinatorial numbers,
      * since we have to select all of them. If N > 6, we select
      * subsets.
      */
     const size_t M = (N > 6) ? 20 : trilateration_binomial(N, K);

     /* A Threshold value. */
     const double threshold = 2.5;

     /* Randomly select M subsets of k elements of the set 1..N. */
     int subset[M][K];
     if (N <= 6) { /* 6 is good for K = 4, for other K this value
		    * should probably change.
		    */
	  /* This is a simple case, because here, all combinations are
	   * needed.
	   */
	  unsigned int num[K], m = 0;
	  trilateration_first_combination(num, K, N);
	  do {
	       for (size_t k = 0; k < K; ++k) {
		    subset[m][k] = num[k];
	       }
	       ++m;
	       trilateration_next_combination(num, K, N);
	  } while (m < M);
     } else {
	  /* This is the complex case, because here, only a subset of
	   * the possible combinations are needed. We try a
	   * Monte-Carlo implementation for finding M different random
	   * shuffles. This might be really inefficient for small N,
	   * say between 7 and 11. With N = 12 and K = 4, we chose 20
	   * out of 495 possibilities, so collisions should be seldom.
	   */
	  size_t m = 0;

	  while (m < M) {
	       trilateration_random_subset(subset[m], K, N, buffer);
	       trilateration_sort4(subset[m]);
	       /* Was this solution already seen? */
	       int seen = 0;
	       for (size_t i = 0; i < m; ++i) {
		    int all_equal = 1;
		    for (size_t k = 0; k < K; ++k) {
			 all_equal = all_equal &&
			      (subset[i][k] == subset[m][k]);
			 if (!all_equal) break;
		    }
		    if (all_equal) {
			 seen = 1;
			 break;
		    }
	       }
	       if (!seen) {
		    ++m;
	       }
	  }
     }

     /* For each subset, try to estimate a position using linear least
      * squares. Keep the one with the least median of errors.
      */
     double median = INFINITY;
     tl_point3d mm[1];
     for (size_t m = 0; m < M; ++m) {
	  tl_point3d ta[K], tr[1];
          double td[K];
          memset(ta, 0, sizeof(ta));
	  for (size_t k = 0; k < K; ++k) {
	       assert(/* 0 <= subset[m][k] && */ subset[m][k] < (int) N);
	       X(ta, k) = AX(subset[m][k]);
	       Y(ta, k) = AY(subset[m][k]);
	       td[k] = d[subset[m][k]];
	  }
	  int s = trilateration_lls_3d(tr, ta, td, K, NULL);
	  if (s == 0) {
	       double residual[N];
	       for (size_t i = 0; i < N; ++i) {
		    double t = trilateration_distance_3d(anchor + i, tr) - d[i];
		    residual[i] = t * t;
	       }
	       double med = trilateration_fmedian(residual, N);

	       /* Step 3: Find the position with the least median.
		*
		* Optimised from the paper. We actually only need the
		* value of the least median and the position that had
		* the least median of errors. The set itself is never
		* used.
		*/
	       if (med < median) {
		    median = med;
		    X(mm, 0) = X(tr, 0);
		    Y(mm, 0) = Y(tr, 0);
                    Z(mm, 0) = Z(tr, 0);
	       }
	  }
     }

     /* Step 4: Calculate s0. */
     const double s0 = 1.4826 * (1.0 + 5.0 / ((double) N - 2.0) * sqrt(median));
  
     /* Step 5: Assign weights to samples. */
     double weight[N];
     for (size_t i = 0; i < N; ++i) {
	  double r = trilateration_distance_3d(mm, anchor + i);
	  weight[i] = (fabs(r / s0) <= threshold) ? 1.0 : 0.0;
     }
     return trilateration_weighted_lls_3d(result, anchor, d, N, weight);
}


#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_lms_meta_data =
"{\n"
"  \"name\": \"Least Median of Squares\",\n"
"  \"symbol\": \"trilateration_lms_3d\",\n"
"  \"description\": \"Estimates position by using the LMS algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": { \"state\": { \"description\": \"State of the drand48 random number generator\", \"type\": \"drand48_data\", \"configurable\": false } }\n"
"}";
#endif

/* Reference:
 *
 * @InProceedings{li05:_lms,
 *   author = {Li, Zang and Trappe, W. and Zhang, Yanyong and Nath, B.},
 *   title = {Robust statistical methods for securing wireless localization
 *            in sensor networks},
 *   booktitle = {IPSN 2005. Fourth International Symposium on Information
 *                Processing in Sensor Networks, 2005.},
 *   year = {2005},
 *   pages = {91--98},
 *   publisher = {IEEE},
 *   doi = {10.1109/IPSN.2005.1440903}
 * }
 */
