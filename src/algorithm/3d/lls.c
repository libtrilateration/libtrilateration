/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/algebra.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>




/*!
 * Linear least squares solver.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_lls_3d(tl_point3d *restrict result,
                     const tl_point3d *const anchor, const double *const d,
                     const size_t N, void *params __attribute__((__unused__)))
{
     if (N < 1) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     const size_t M = N - 1;
     double b[M * 1], a[M * 3];

     for (size_t i = 0; i < M; i++) {
          a[C(i, 0, M, 3)] = 2.0 * AX(i) - 2.0 * AX(M);
          a[C(i, 1, M, 3)] = 2.0 * AY(i) - 2.0 * AY(M);
          a[C(i, 2, M, 3)] = 2.0 * AZ(i) - 2.0 * AZ(M);
          b[C(i, 0, M, 1)] = ((AX(i) * AX(i) - AX(M) * AX(M)) +
			      (AY(i) * AY(i) - AY(M) * AY(M)) +
			      (AZ(i) * AZ(i) - AZ(M) * AZ(M)) +
			      (d[M] * d[M] - d[i] * d[i]));
     }

     double r[3 * 1];
     trilateration_solve_linear_equation_3d(r, a, b, M);

     tl_point3d_set(result, r[0], r[1], r[2]);
     return TL_OK;
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_lls_3d_meta_data =
"{\n"
"  \"name\": \"Linear Least Squares\",\n"
"  \"symbol\": \"trilateration_lls_3d\",\n"
"  \"description\": \"Estimates position by using the LLS algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
