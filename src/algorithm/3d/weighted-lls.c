/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/algebra.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>



/*!
 * Weighted Linear least squares solver.
 *
 * The unweighted version above is an optimised version of this one,
 * since this one needs N^2 words more memory and two additional matrix
 * multiplications.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_weighted_lls_3d(tl_point3d *restrict result,
                              const tl_point3d *const anchor,
                              const double *const d, const size_t N,
			      void *params)
{
     double *weights = (double *) params;

     if (N < 1) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     const size_t M = N - 1;
     double b[M * 1], a[M * 3];

     for (size_t i = 0; i < M; i++) {
          a[C(i, 0, M, 2)] = AX(i) - AX(M);
          a[C(i, 1, M, 2)] = AY(i) - AY(M);
          b[C(i, 0, M, 1)] = (AX(i) * AX(i) - AX(M) * AX(M) +
			      AY(i) * AY(i) - AY(M) * AY(M) +
			      d[M] * d[M] - d[i] * d[i]) / 2.0;
     }

     /* Solve with closed form solution x = (A^T * W^2 * A)^-1 * A^T * W^2 * b */
     double w[M * M];
     for (size_t i = 0; i < M; i++) {
	  for (size_t j = 0; j < M; j++) {
	       w[C(i, j, M, M)] = (i != j) ? 0.0 :
		    ((weights != NULL) ? weights[i] * weights[i] : 1.0);
	  }
     }
     double at[2 * M];
     trilateration_matrix_transpose(at, a, M, 2);
     double tmp0[2 * M];
     trilateration_matrix_multiply(tmp0, at, 2, M, w, M, M);
     double tmp1[2 * 2];
     trilateration_matrix_multiply(tmp1, tmp0, 2, M, a, M, 2);
     double tmp2[2 * 2];
     trilateration_matrix_invert_2x2(tmp2, tmp1);
     double tmp3[2 * M];
     trilateration_matrix_multiply(tmp3, tmp2, 2, 2, at, 2, M);
     double tmp4[2 * M];
     trilateration_matrix_multiply(tmp4, tmp3, 2, M, w, M, M);
     double tmp5[3* 1];
     trilateration_matrix_multiply(tmp5, tmp4, 2, M, b, M, 1);

     tl_point3d_set(result, tmp5[0], tmp5[1], tmp5[2]);

     return TL_OK;
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_weighted_lls_3d_meta_data =
"{\n"
"  \"name\": \"Weighted linear least squares\",\n"
"  \"symbol\": \"trilateration_weightedlls_3d\",\n"
"  \"description\": \"Estimates position by calculating a weighted linear least squares method.\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
