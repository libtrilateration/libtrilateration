/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>





trilateration_status
trilateration_minmax_3d(tl_point3d *restrict result,
                        const tl_point3d *const anchor, const double *const d,
                        const size_t N, void *params __attribute__((__unused__)))
{
     assert (result != NULL);
     assert (anchor != NULL);
     assert (d != NULL);

     tl_point3d g[8];

     trilateration_bounding_box_3d(g, anchor, d, N);
     return trilateration_centre_of_mass_3d(result, g, 8);
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_minmax_3d_meta_data =
"{\n"
"  \"name\": \"MINMAX\",\n"
"  \"symbol\": \"trilateration_minmax_3d\",\n"
"  \"description\": \"Estimates position by using the MINMAX algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
