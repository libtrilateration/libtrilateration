/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>

#include <math.h>
#include <float.h>
#include <gsl/gsl_multimin.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>



/* This structure holds the parameters to the likelihood function. */
struct mle_gauss_params {
     const tl_point3d *anchor;
     const double * d;
     size_t N;
     double median;
     double variance;
     double rate;
};


static double
__attribute__((__nonnull__,__flatten__))
mle_gauss_likelihood_function(const gsl_vector *X, void *restrict params)
{
    double result = 0.0;
    const struct mle_gauss_params *const p =
        (struct mle_gauss_params *) params;
    const double deviation = sqrt(p->variance);
    const double thetaX = gsl_vector_get(X, 0);
    const double thetaY = gsl_vector_get(X, 1);

    for (size_t j = 0; j < p->N; j++) {
        const double d = 
	     trilateration_distance(thetaX, thetaY, X(p->anchor, j), Y(p->anchor, j));
        const double d2 = d * d;
        const double z = p->d[j];
        double logProbability = z * z ;
        logProbability += (-2 * p->median - 2 * d) * z;
        logProbability += p->variance * log(M_PI);
        logProbability += p->median * p->median;
        logProbability += 2 * d * p->median;
        logProbability += 2 * p->variance * log(deviation); 
        logProbability += log(2) * deviation;
        logProbability += d2; 
        logProbability /= 2 * p->variance;
        result += logProbability;
    }
    return result;
}





static void
__attribute__((__nonnull__,__flatten__))
mle_gauss_likelihood_gradient(const gsl_vector *X, void *restrict params,
                              gsl_vector *restrict g)
{
     const struct mle_gauss_params *const p =
          (struct mle_gauss_params *) params;
     const double thetaX = gsl_vector_get(X, 0);
     const double thetaY = gsl_vector_get(X, 1);
     double gradX = 0.0, gradY = 0.0;

     for (size_t j = 0; j < p->N; j++) {
          const double d =
               trilateration_distance(thetaX, thetaY, X(p->anchor, j), Y(p->anchor, j));
          const double z = p->d[j];
          double x = ((2 * p->median * (thetaX - X(p->anchor, j)) -
                       2 * z * (thetaX - Y(p->anchor, j))) / d +
                      2 * (thetaX - X(p->anchor, j))) / (2 * p->variance);
          double y = ((2 * p->median * (thetaY - Y(p->anchor, j)) -
                       2 * z * (thetaY - Y(p->anchor, j))) / d +
                      2 * (thetaY - Y(p->anchor, j))) / (2 * p->variance);
          gradX += x;
          gradY += y;
     }
     gsl_vector_set(g, 0, gradX);
     gsl_vector_set(g, 1, gradY);
}




static void
__attribute__((__nonnull__,__flatten__,__hot__,__used__))
mle_gauss_likelihood_fdf(const gsl_vector *X, void *restrict params,
                         double *restrict y, gsl_vector *restrict g)
{
    *y = mle_gauss_likelihood_function(X, params);
    mle_gauss_likelihood_gradient(X, params, g);
}



/*!
 * Maximum likelyhood estimator.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
 
trilateration_status
trilateration_mle_gauss_3d(tl_point3d *restrict result,
                           const tl_point3d *const anchor,
                           const double *const d, const size_t N, void *params)
{
     /* Step 0: Set up the likelihood function. */
     trilateration_mle_gauss_params *ap =
          (trilateration_mle_gauss_params *) params;
     struct mle_gauss_params p;
     p.anchor = anchor;
     p.d = d;
     p.N = N;
     p.median = (ap != NULL) ? ap->median : 1.78;
     p.variance = (ap != NULL) ? ap->variance : 11.0;

     const int iterations = 500;

     gsl_multimin_function_fdf fdf;
     fdf.n = 2u;
     fdf.f = &mle_gauss_likelihood_function;
     fdf.df = &mle_gauss_likelihood_gradient;
     fdf.fdf = &mle_gauss_likelihood_fdf;
     fdf.params = &p;

     /* Step: Call the optimiser. */
     const gsl_multimin_fdfminimizer_type *T =
	  gsl_multimin_fdfminimizer_vector_bfgs2;
     gsl_multimin_fdfminimizer *s = gsl_multimin_fdfminimizer_alloc(T, 2);

     gsl_vector *x = gsl_vector_alloc(2);

     tl_point3d ss[1];
     trilateration_centroid_2d(ss, anchor, d, N, NULL);

     /* Step 2a: Initialize the parameters. */
     gsl_vector_set(x, 0, X(ss, 0));
     gsl_vector_set(x, 1, Y(ss, 0));

     double likelihood = mle_gauss_likelihood_function(x, &p);
#if defined(DEBUG_TRACE_ITERATIONS)
     fprintf(stdout, "Start point: f(%f,%f) = %f\n",
	     gsl_vector_get(x, 0), gsl_vector_get(x, 1), likelihood);
#endif
     if (isinf(likelihood)) {
	  /* If this is the case, the initial guess does not have
	     a likelihood associated to it. We set the estimated coordinate
	     to an undefined value and continue. */
	  fprintf(stdout, "Unlikely place, defaulting to (%f, %f)\n",
		  X(ss, 0), Y(ss, 0));
          tl_point3d_set(result, X(ss, 0), Y(ss, 0), Z(ss, 0));
     }

     /* Step 2c: Iterate the minimization algorithm. */
     int iter = 0;
     int status;
     gsl_multimin_fdfminimizer_set(s, &fdf, x, 1e-2, 1e-4);

#if defined(DEBUG_TRACE_ITERATIONS)
     fprintf(stdout, "\n");
#endif
     do {
	  iter++;
	  status = gsl_multimin_fdfminimizer_iterate(s);
	  if (status != 0) { // Not zero if there was an error.
	       break;
	  }
	  status = gsl_multimin_test_gradient (s->gradient, 1e-5);
#if defined(DEBUG_TRACE_ITERATIONS)
	  if (status == GSL_SUCCESS) {
	       fprintf(stdout, "Minimum found at: \n");
	  }
	  fprintf(stdout, "%5d %.5f %.5f %10.5f\n", iter,
		  gsl_vector_get (s->x, 0), 
		  gsl_vector_get (s->x, 1), 
		  s->f);
#endif
     } while (status == GSL_CONTINUE && iter < iterations);

     /* Step 2c: Store the result. */
     const gsl_vector *tr = gsl_multimin_fdfminimizer_x(s);
     tl_point3d_set(result, gsl_vector_get(tr, 0), gsl_vector_get(tr, 1),
                    gsl_vector_get(tr, 2));
#if defined(DEBUG_TRACE_ITERATIONS)
     fprintf(stdout, "Residual: (%f, %f) -> %f, likelihood = %f\n", *rx, *ry,
	     residual_vs(i, ax, ay, d, N, *rx, *ry),
	     mle_gauss_likelihood_function(s->x, &p));
#endif

     /* Step 3: Clean up. */
     gsl_multimin_fdfminimizer_free(s);
     gsl_vector_free(x);

     return TL_OK;
}


#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_mle_gauss_meta_data =
"{\n"
"  \"name\": \"MLE Gauss\",\n"
"  \"symbol\": \"trilateration_mle_gauss_2d\",\n"
"  \"description\": \"Estimates position by using an Maximum Likelihood estimator assuming Gaussian distributed measurement errors.\",\n"
"  \"configurable\": true,\n"
"  \"configuration\": {\n"
"    \"median\": { \"type\": \"double\", \"unit\": \"m\", \"configurable\": true, \"description\": \"Median of the Gaussian distribution\" },\n"
"    \"variance\": { \"type\": \"double\", \"unit\": \"m²\", \"configurable\": true, \"description\": \"Variance of the Gaussian distribution\" }\n"
"  }\n"
"}";
#endif
