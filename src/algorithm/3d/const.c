/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>



/*!
 * Const algorithm.
 *
 * Returns the same location at the origin.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
 
trilateration_status
trilateration_const_3d(tl_point3d *restrict result,
		       const tl_point3d *const anchor __attribute__((__unused__)),
		       const double *const d __attribute__((__unused__)),
		       const size_t N __attribute__((__unused__)),
		       void *params __attribute__((__unused__)))
{
     assert (result != NULL);

     tl_point3d_set(result, 0.0, 0.0, 0.0);

     return TL_OK;
}


#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_const_3d_meta_data =
"{\n"
"  \"name\": \"Constant\",\n"
"  \"symbol\": \"trilateration_const_3d\",\n"
"  \"description\": \"Returns the origin. Used for debugging and profiling.\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif
