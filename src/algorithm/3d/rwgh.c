/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/algebra.h>
#include <libtrilateration/geometry.h>
#include <libtrilateration/util.h>


/*!
 * Const algorithm.
 *
 * Returns the same location at the origin.
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
 
trilateration_status
trilateration_rwgh_3d(tl_point3d *restrict result,
                      const tl_point3d *const anchor,
		      const double *const d, const size_t N,
		      void *params)
{
     trilateration_algorithm_t algorithm = (params != NULL) ? ((trilateration_rwgh_3d_params *) params)->algorithm : trilateration_nlls_3d;
     void *algorithm_params = (params != NULL) ? ((trilateration_rwgh_3d_params *) params)->params : NULL;

     /* Minimum set size. */
     const size_t S = 3;

     assert (result != NULL);

     if (N <= S) {
          /* There is no or only one combination to consider. */
          return algorithm(result, anchor, d, N, algorithm_params);
     }

     /* Calculate 2^N, since there are this many different combinations
	of anchors. */
     if (N >= 16) {
	  /* We just cannot calculate this number of combinations. It is
	     better to just give up! */
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }


     /* We can omit the permutations C(N, 1) and C(N, 2) */
     const size_t K = (1 << N) -
	  (trilateration_binomial(N, 1) + trilateration_binomial(N, 2));
     tl_point3d ii[3];
     double residual[K];
     size_t i_count = 0;

     for (size_t k = S; k < N; ++k) {
	  unsigned int combination[k];
	  trilateration_first_combination(combination, k, N);
	  do {
	       tl_point3d ta[k];
               double td[k];
	       for (size_t i = 0; i < k; ++i) {
		    X(ta, i) = AX(combination[i]);
                    Y(ta, i) = AY(combination[i]);
                    Z(ta, i) = AZ(combination[i]);
		    td[i] = d[combination[i]];
	       }
	       int r = algorithm(result, ta, td, k, algorithm_params);
	       if (r == 0 && !isnan(RX) && !isnan(RY) && !isnan(RZ)) {
		    assert(i_count < K);
		    X(ii, i_count) = RX;
		    Y(ii, i_count) = RY;
		    Z(ii, i_count) = RZ;
		    double t = trilateration_square_residual_error(ta, td, k, result);
		    residual[i_count] = (fabs(t) > DBL_EPSILON) ? ((double) k) / t : DBL_MAX;
		    ++i_count;
	       }
	  } while (trilateration_next_combination(combination, k, N));
     }
     if (i_count == 0) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return TL_ERROR;
     }

     return trilateration_weighted_centre_of_mass_3d(result, ii, residual,
                                                     i_count);
}

#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_rwgh_3d_meta_data =
"{\n"
"  \"name\": \"RWGH\",\n"
"  \"symbol\": \"trilateration_rwgh_3d\",\n"
"  \"description\": \"Estimates position by using the RWGH algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null\n"
"}";
#endif

/*
 * REFERENCE:
 *
 * "A non-line-of-sight error mitigation algorithm in
 *  * location estimation", Pi-Chun Chen, 1999.
 */
