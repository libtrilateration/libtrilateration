/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/algorithms.h>
#include <libtrilateration/geometry.h>


/*!
 * Bilateration
 *
 * \param ax An array that contains all x coordinates of the anchors.
 * \param ay An array that contains all y coordinates of the anchors.
 * \param d An array that contains the distance to the corresponding
 * anchor.
 * \param N the size of the arrays \c ax, \c ay, and \c
 * d. Each array should be larger than N. N should be larger than 2.
 * \param rx A pointer to the resulting x coordinate.
 * \param ry A pointer to the resulting y coordinate.
 *
 * \returns 0 if a location was computed, -1 if there was an error.
 */
trilateration_status
trilateration_bilateration_3d(tl_point3d *restrict result,
                              const tl_point3d *const anchor,
			      const double *const d, const size_t N,
			      void *params __attribute__((__unused__)))
{
     assert (N > 0 && anchor != NULL && d != NULL);
     assert (result != NULL);

     /* Calculate all intersections of circles, and approximate one intersection
	if there is none. */
     const size_t K = N * (N - 1) / 2;
     tl_point3d ii[K][2];
     int r[K];
     size_t R = 0;

     for (size_t i = 1; i < N; i++) {
	  for (size_t j = 0; j < i; j++) {
	       assert (R < K);
	       r[R] = trilateration_circle_intersections(ii[R],
                                                         anchor + j, d[j],
							 anchor + i, d[i]);
	       if (0 == r[R]) {
		    r[R] = trilateration_circle_approximate(ii[R],
                                                            anchor + j, d[j],
							    anchor + i, d[i]);
	       }
	       if (0 < r[R]) {
		    R++;
	       }
	  }
     }

     /* Next filter relevant intersection points from the list of
	intersections generated above. */
     tl_point3d l[2 * R];
     size_t L = 0;
     for (size_t i = 0; i < R; i++) {
	  if (r[i] == 1) {
	       X(l, L) = X(ii[i], 0);
	       Y(l, L) = Y(ii[i], 0);
	       Z(l, L) = Z(ii[i], 0);
	       L++;
	  } else if (r[i] == 2) {
	       double phi = 0.0, psi = 0.0;
	       for (size_t j = 0; j < R; j++) {
		    if (i != j) {
			 double dPhi, dPsi;
			 if (r[j] == 1) {
			      dPsi = trilateration_distance_squared(X(ii[i], 0), Y(ii[i], 0), X(ii[j], 0), Y(ii[j], 0));
			      dPhi = trilateration_distance_squared(X(ii[i], 1), Y(ii[i], 1), X(ii[j], 0), Y(ii[j], 0));
			 } else {
			      dPsi = fmin(trilateration_distance_squared(X(ii[i], 0), Y(ii[i], 0), X(ii[j], 0), Y(ii[j], 0)),
					  trilateration_distance_squared(X(ii[i], 0), Y(ii[i], 0), X(ii[j], 1), Y(ii[j], 1)));
			      dPhi = fmin(trilateration_distance_squared(X(ii[i], 1), Y(ii[i], 1), X(ii[j], 0), Y(ii[j], 0)),
					  trilateration_distance_squared(X(ii[i], 1), Y(ii[i], 1), X(ii[j], 1), Y(ii[j], 1)));
			 }
			 phi += dPhi;
			 psi += dPsi;
		    }
	       }
	       if (psi < phi) {
		    X(l, L) = X(ii[i], 0);
		    Y(l, L) = Y(ii[i], 0);
	       } else {
		    X(l, L) = X(ii[i], 1);
		    Y(l, L) = Y(ii[i], 1);
	       }
	       L++;
	  }
     }

     /* The estimated position is the centre of mass of all points selected
	above. */
     return trilateration_centre_of_mass_3d(result, l, L);
}



#if ENABLE_META_DATA
/* Meta data. */
const char *trilateration_bilateration_3d_meta_data =
"{\n"
"  \"name\": \"Bilateration\",\n"
"  \"symbol\": \"trilateration_bilateration_3d\",\n"
"  \"description\": \"Localisation using the bilateration algorithm\",\n"
"  \"configurable\": false,\n"
"  \"configuration\": null,\n"
"  \"citation\": {\n"
"    \"type\": \"article\",\n"
"    \"author\": [\"Cota-Ruiz, Juan\", \"Rosiles, Jose-Gerardo\",\n"
"                 \"Sifuentes, Ernesto\", \"Rivas-Perea, Pablo\"],\n"
"    \"title\": \"A Low-Complexity Geometric Bilateration Method for Localization in Wireless Sensor Networks and Its Comparison with Least-Squares Methods\",\n"
"    \"journal\": \"Sensors\",\n"
"    \"year\": 2012,\n"
"    \"volume\": \"12\",\n"
"    \"number\": \"1\",\n"
"    \"pages\": \"839-862\",\n"
"    \"doi\": \"10.3390/s120100839\"\n"
"  }\n"
"}";
#endif
