#! /bin/sh
#
# Index all algorithms.
#
# This file is part of libtrilateration.
#
# Copyright 2014 by Marcel Kyas
#
# libtrilateration is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libtrilateration is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libtrilateration.  If not, see <http://www.gnu.org/licenses/>.
#

# Figure out how the script was invoked.
srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

# Change into the algorithms directory and collect all algorithm names

pushd "$srcdir"/algorithm/2d >/dev/null 2>&1
algs=$(grep -h -e '^[^ ]*_2d' *.c | sed 's/^\(.*\)(.*$/\1/')
popd >/dev/null 2>&1

pushd "$srcdir"/>/dev/null 2>&1
echo '/* Automatically generated. Do not modify. */'
echo
echo '#if HAVE_CONFIG_H'
echo '#  include "libtrilateration/config.h"'
echo '#endif'
echo
echo '#include <stdlib.h>'
echo '#include <string.h>'
echo
echo 'const char *trilateration_algorithm_2d_index[] = {'
for i in $algs
do
    echo '     "'$i'",'
done
echo '     NULL'
echo '};'
echo
echo '#if ENABLE_META_DATA'
for i in $algs
do
    echo 'extern const char * const '$(echo $i | sed -e 's,2d$,meta_data,')';'
done
echo '#endif'
echo
echo 'extern char * const *trilateration_algorithm_2d_meta_data(void)'
echo '{'
echo '#if ENABLE_META_DATA'
echo '    const char * const meta_data[] = {'
for i in $algs
do
    echo '         '$(echo $i | sed -e 's,2d$,meta_data,')','
done
echo '         NULL'
echo '    };'
echo '    char **result = malloc(sizeof(meta_data));'
echo '    memcpy(result, meta_data, sizeof(meta_data));'
echo '    return result;'
echo '#else'
echo '    return NULL;'
echo '#endif'
echo '}'
