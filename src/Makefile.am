# This file is part of libtrilateration
#
# Copyright 2014 by Marcel Kyas
#
# libtrilateration is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# libtrilateration is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libtrilateration.  If not, see
# <http://www.gnu.org/licenses/>.
#

EXTRA_DIST = index.sh
noinst_SCRIPTS = index.sh

AM_CFLAGS = -Werror -Wall -Wextra -Wshadow

lib_LTLIBRARIES = libtrilateration.la

bin_PROGRAMS = tl-spatial-evaluation tl-configuration tl-batch

nobase_include_HEADERS = libtrilateration/config.h \
	libtrilateration/algorithms.h
	libtrilateration/geometry.h

libtrilateration_la_SOURCES = \
	libtrilateration/types.h \
	libtrilateration/algebra.h \
	algebra/linear-equations.c \
	algebra/matrix.c \
	libtrilateration/geometry.h \
	geometry/bounding-box.c \
	geometry/collinear.c \
	geometry/centre.c \
	geometry/circle.c \
	geometry/disc.c \
	geometry/distance.c \
	geometry/median.c \
	geometry/triangle.c \
	libtrilateration/util.h \
	util/combinations.c \
	util/exponentiation.c \
	util/select.c \
	util/sort.c \
	libtrilateration/algorithms.h \
	algorithm_index.c \
	algorithm/2d/aml.c \
	algorithm/3d/aml.c \
	algorithm/2d/bilateration.c \
	algorithm/3d/bilateration.c \
	algorithm/2d/centroid.c \
	algorithm/3d/centroid.c \
	algorithm/2d/clurol.c \
	algorithm/3d/clurol.c \
	algorithm/2d/const.c \
	algorithm/3d/const.c \
	algorithm/2d/cpf.c \
	algorithm/3d/cpf.c \
	algorithm/2d/e-minmax-w2.c \
	algorithm/3d/e-minmax-w2.c \
	algorithm/2d/e-minmax-w4.c \
	algorithm/3d/e-minmax-w4.c \
	algorithm/2d/geo-3.c \
	algorithm/3d/geo-3.c \
	algorithm/2d/geo-n.c \
	algorithm/3d/geo-n.c \
	algorithm/2d/icla.c \
	algorithm/3d/icla.c \
	algorithm/2d/lls.c \
	algorithm/3d/lls.c \
	algorithm/2d/lms.c \
	algorithm/3d/lms.c \
	algorithm/2d/md-minmax.c \
	algorithm/3d/md-minmax.c \
	algorithm/2d/md-minmax-abs.c \
	algorithm/3d/md-minmax-abs.c \
	algorithm/2d/minmax.c \
	algorithm/3d/minmax.c \
	algorithm/2d/mle-gamma.c \
	algorithm/3d/mle-gamma.c \
	algorithm/2d/mle-gauss.c \
	algorithm/3d/mle-gauss.c \
	algorithm/2d/nlls.c \
	algorithm/3d/nlls.c \
	algorithm/2d/rlsm.c \
	algorithm/3d/rlsm.c \
	algorithm/2d/rwgh.c \
	algorithm/3d/rwgh.c \
	algorithm/2d/weighted-centroid.c \
	algorithm/3d/weighted-centroid.c \
	algorithm/2d/weighted-lls.c \
	algorithm/3d/weighted-lls.c \
	algorithm/2d/trilateration.c \
	algorithm/3d/trilateration.c
libtrilateration_la_CFLAGS = $(AM_CFLAGS) $(GSL_CFLAGS)
libtrilateration_la_LDFLAGS = $(GSL_LIBS) -lm

tl_spatial_evaluation_SOURCES = spatial/main.c spatial/variant.h
tl_spatial_evaluation_CFLAGS = -pthread $(AM_CFLAGS) -I$(top_srcdir)/src $(GSL_CFLAGS)
tl_spatial_evaluation_LDFLAGS = -pthread
tl_spatial_evaluation_LDADD = $(top_builddir)/src/libtrilateration.la $(GSL_LIBS) -lhdf5 -lpopt

tl_configuration_SOURCES = configuration.c
tl_configuration_CFLAGS = $(AM_CFLAGS) -I$(top_srcdir)/src
#tl_configuration_LDFLAGS =
tl_configuration_LDADD = $(top_builddir)/src/libtrilateration.la

tl_batch_SOURCES = batch/batch.h batch/netcdf.c batch/batch.c
tl_batch_CFLAGS = $(AM_CFLAGS) $(NETCDF_CFLAGS) -I$(top_srcdir)/src
#tl_batch_LDFLAGS =
tl_batch_LDADD = $(top_builddir)/src/libtrilateration.la $(NETCDF_LIBS)

algorithm_index.c: ${srcdir}/index.sh ${srcdir}/Makefile.am
	rm -f $@
	${SHELL} ${srcdir}/index.sh > $@
