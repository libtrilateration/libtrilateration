/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <libtrilateration/algebra.h>

/*!
 * Calculate the index position of an array that refers to (row, col)
 * in a matrix of type rows x columns.
 */
int
trilateration_matrix_index(size_t row, size_t col,
                           size_t rows __attribute__((__unused__)),
                           size_t cols)
{
     assert (/* 0 <= col && */ col < cols);
     assert (/* 0 <= row && */ row < rows);
     return row * cols + col;
}


/*!
 * Transpose a matrix.
 *
 * \params dest The result of transposing.
 * \params src  The source to transpose.
 * \params rows Number of rows in the matrix.
 * \params cols number of columns in the matrix.
 */
void
trilateration_matrix_transpose(double *restrict dest,
                               const double *restrict src,
                               const size_t rows, const size_t cols)
{
     assert(dest != NULL && src != NULL);
     for (size_t row = 0; row < rows; row++) {
          for (size_t col = 0; col < cols; col++) {
	       dest[C(col, row, cols, rows)] = src[C(row, col, rows, cols)];
          }
     }
}



/*!
 * Multyply two matrixes.
 *
 * \param dest The result of the multiplication.
 * \param src1 The first matrix to multiply.
 * \param cols1 The number of columns of the first matrix.
 * \param rows1 The number of rows of the first matrix.
 * \param src2 The second matrix to multiply.
 * \param cols2 The number of columns of the second matrix.
 * \param rows2 The number of rows of the second matrix.
 */
void
trilateration_matrix_multiply(double *restrict dest,
			      const double *restrict src1,
			      const size_t rows1, const size_t cols1,
			      const double *restrict src2,
			      const size_t rows2, const size_t cols2)
{
     assert(cols1 == rows2);
     assert(dest != NULL && src1 != NULL && src2 != NULL);
     for (size_t i = 0; i < rows1; i++) {
          for (size_t j = 0; j < cols2; j++) {
	       dest[C(i, j, rows1, cols2)] = 0.0;
	       for (size_t k = 0; k < cols1; k++) {
		    dest[C(i, j, rows1, cols2)] +=
			 src1[C(i, k, rows1, cols1)] *
			 src2[C(k, j, rows2, cols2)];
              }
          }
     }
}



void
trilateration_matrix_invert_2x2(double *restrict dest,
                                const double *restrict src)
{
     assert(dest != NULL && src != NULL);
     const double det = src[C(0, 0, 2, 2)] * src[C(1, 1, 2, 2)] -
	  src[C(0, 1, 2, 2)] * src[C(1, 0, 2, 2)];

     if (fabs(det) < 1e-5) {
          /* The matrix is singular, so there is no inverse */
          dest[0] = dest[1] = dest[2] = dest[3] = NAN;
          return;
     }
     const double recip_det = 1.0 / det;
     dest[C(0, 0, 2, 2)] =  recip_det * src[C(1, 1, 2, 2)];
     dest[C(0, 1, 2, 2)] = -recip_det * src[C(0, 1, 2, 2)];
     dest[C(1, 0, 2, 2)] = -recip_det * src[C(1, 0, 2, 2)];
     dest[C(1, 1, 2, 2)] =  recip_det * src[C(0, 0, 2, 2)];
}



void
trilateration_matrix_invert_3x3(double *restrict dest,
                                const double *restrict src)
{
     assert(dest != NULL && src != NULL);
     const double det =
          src[C(0, 0, 3, 3)] * src[C(1, 1, 3, 3)] * src[C(2, 2, 3, 3)] +
	  src[C(1, 0, 3, 3)] * src[C(2, 1, 3, 3)] * src[C(0, 2, 3, 3)] +
	  src[C(2, 0, 3, 3)] * src[C(0, 1, 3, 3)] * src[C(1, 2, 3, 3)] +
          src[C(0, 0, 3, 3)] * src[C(2, 1, 3, 3)] * src[C(1, 2, 3, 3)] -
	  src[C(2, 0, 3, 3)] * src[C(1, 1, 3, 3)] * src[C(0, 2, 3, 3)] -
	  src[C(1, 0, 3, 3)] * src[C(0, 1, 3, 3)] * src[C(2, 2, 3, 3)];

     if (fabs(det) < 1e-5) {
          /* The matrix is singular, so there is no inverse */
          for (register int i = 0; i < 9; i++) {
               dest[i] = NAN;
          }
          return;
     }
     const double recip_det = 1.0 / det;
     dest[C(0, 0, 3, 3)] = recip_det *
          (src[C(1, 1, 3, 3)] * src[C(2, 2, 3, 3)] -
           src[C(1, 2, 3, 3)] * src[C(2, 1, 3, 3)]);
     dest[C(0, 1, 3, 3)] = recip_det *
          (src[C(1, 2, 3, 3)] * src[C(2, 0, 3, 3)] -
           src[C(1, 0, 3, 3)] * src[C(2, 2, 3, 3)]);
     dest[C(0, 2, 3, 3)] = recip_det *
          (src[C(1, 0, 3, 3)] * src[C(2, 1, 3, 3)] -
           src[C(1, 1, 3, 3)] * src[C(2, 0, 3, 3)]);
     dest[C(1, 0, 3, 3)] = recip_det *
          (src[C(0, 2, 3, 3)] * src[C(2, 1, 3, 3)] -
           src[C(0, 1, 3, 3)] * src[C(2, 2, 3, 3)]);
     dest[C(1, 1, 3, 3)] = recip_det *
          (src[C(0, 0, 3, 3)] * src[C(2, 2, 3, 3)] -
           src[C(0, 2, 3, 3)] * src[C(2, 0, 3, 3)]);
     dest[C(1, 2, 3, 3)] = recip_det *
          (src[C(0, 2, 3, 3)] * src[C(2, 0, 3, 3)] -
           src[C(0, 0, 3, 3)] * src[C(2, 1, 3, 3)]);
     dest[C(2, 0, 3, 3)] = recip_det *
          (src[C(0, 1, 3, 3)] * src[C(1, 2, 3, 3)] -
           src[C(0, 2, 3, 3)] * src[C(1, 0, 3, 3)]);
     dest[C(2, 1, 3, 3)] = recip_det *
          (src[C(0, 2, 3, 3)] * src[C(1, 0, 3, 3)] -
           src[C(0, 0, 3, 3)] * src[C(1, 2, 3, 3)]);
     dest[C(2, 2, 3, 3)] = recip_det *
          (src[C(0, 0, 3, 3)] * src[C(1, 1, 3, 3)] -
           src[C(0, 1, 3, 3)] * src[C(1, 0, 3, 3)]);
}



/*!
 * Display a matrix.
 */
void
trilateration_print_matrix(const double *restrict src, size_t rows, size_t cols)
{
     for (size_t row = 0; row < rows; row++) {
	  fprintf(stdout, "( ");
	  for (size_t col = 0; col < cols - 1; col++) {
	       fprintf(stdout, "%.*e   ", 5, src[C(row, col, rows, cols)]);
	  }
	  fprintf(stdout, "%.*e )\n", 5, src[C(row, cols - 1, rows, cols)]);
     }
     fprintf(stdout, "\n");
     fflush(stdout);
}
