
/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
# include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <libtrilateration/algebra.h>

void
trilateration_solve_linear_equation_2d(double *restrict result,
				       const double *restrict a,
				       const double *restrict b, size_t M)
{
     /* Solve with closed form solution x = (A^T * A)^-1 * (A^T * b) */
     double At[2 * M];
     trilateration_matrix_transpose(At, a, M, 2);
     double AtA[2 * 2];
     trilateration_matrix_multiply(AtA, At, 2, M, a, M, 2);
     double AtAi[2 * 2];
     trilateration_matrix_invert_2x2(AtAi, AtA);
     double Atb[2 * 1];
     trilateration_matrix_multiply(Atb, At, 2, M, b, M, 1);
     trilateration_matrix_multiply(result, AtAi, 2, 2, Atb, 2, 1);
}


#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_matrix.h>

void
trilateration_solve_linear_equation_3d(double *restrict result,
				       double *restrict a,
				       double *restrict b, size_t M)
{
#if 0
     trilateration_print_matrix(a, M, 3);
     /* Solve with closed form solution x = (A^T * A)^-1 * (A^T * b) */
     double At[3 * M];
     trilateration_matrix_transpose(At, a, M, 3);
     double AtA[3 * 3];
     trilateration_matrix_multiply(AtA, At, 3, M, a, M, 3);
     trilateration_print_matrix(AtA, 3, 3);
     double AtAi[3 * 3];
     trilateration_matrix_invert_3x3(AtAi, AtA);
     double Atb[3 * 1];
     trilateration_matrix_multiply(Atb, At, 3, 3, b, 3, 1);
     trilateration_matrix_multiply(result, AtAi, 3, M, Atb, 3, 1);
#else
     /* Use GSL to solve linear equations, because Cramers rule does
      * not work for singular matrixes and if all nodes are in a plane,
      * the matrix is singular.
      */
     gsl_matrix_view A = gsl_matrix_view_array(a, M, 3);
     gsl_matrix *AtA = gsl_matrix_alloc(3, 3);
     gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, &A.matrix, &A.matrix,
                    0.0, AtA);
     gsl_vector_view B = gsl_vector_view_array(b, M);
     gsl_vector *AtB = gsl_vector_alloc(3);
     gsl_blas_dgemv(CblasTrans, 1.0, &A.matrix, &B.vector,
                    0.0, AtB);
     gsl_vector *x = gsl_vector_alloc(3);
     gsl_permutation *p = gsl_permutation_alloc(3);
     int s;
     gsl_linalg_LU_decomp(AtA, p, &s);
     if (fabs(gsl_linalg_LU_det(AtA, s)) >= 1e-5) {
             gsl_linalg_LU_solve(AtA, p, AtB, x);
             result[0] = gsl_vector_get(x, 0);
             result[1] = gsl_vector_get(x, 1);
             result[2] = gsl_vector_get(x, 2);
     } else {
             /* Singular matrix ... */
             result[0] = NAN;
             result[1] = NAN;
             result[2] = NAN;
     }
     gsl_permutation_free(p);
     gsl_vector_free(AtB);
     gsl_matrix_free(AtA);
     gsl_vector_free(x);
#endif
}
