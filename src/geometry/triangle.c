/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>


double __attribute__((__pure__))
trilateration_triangle_perimeter(const tl_point3d *const t)
{
     double a = trilateration_distance(X(t, 0), Y(t, 0), X(t, 1), Y(t, 1));
     double b = trilateration_distance(X(t, 1), Y(t, 1), X(t, 2), Y(t, 2));
     double c = trilateration_distance(X(t, 2), Y(t, 2), X(t, 0), Y(t, 0));
     return a + b + c;
}



double __attribute__((__pure__))
trilateration_triangle_area(const tl_point3d *restrict t)
{
     double a = trilateration_distance(X(t, 0), Y(t, 0), X(t, 1), Y(t, 1));
     double b = trilateration_distance(X(t, 1), Y(t, 1), X(t, 2), Y(t, 2));
     double c = trilateration_distance(X(t, 2), Y(t, 2), X(t, 0), Y(t, 0));
     const double s = 0.5 * (a + b + c);
     const double u = s * (s - a) * (s - b) * (s - c);
     return sqrt(u);
}

