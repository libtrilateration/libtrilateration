/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>



/*!
 * Tests whether the point x,y is in the disc defined by
 * cx, cy, r.
 */
int
trilateration_point_in_disc_p(const tl_point3d *const p,
                              const tl_point3d *const c, const double r)
{
     return trilateration_distance(X(c, 0), Y(c, 0), X(p, 0), Y(p, 0)) < r;
}


/*!
 * Tests whether the point x,y is in the discs defined by
 * cx, cy, r.
 */
int
trilateration_point_in_discs_p(const tl_point3d *const p,
                               const tl_point3d *const c,
                               const double *const r, const size_t N)
{
     for (size_t i = 0; i < N; i++) {
	  if (!trilateration_point_in_disc_p(p, c + i, r[i])) {
	       return 0;
	  }
     }
     return 1;
}


/*
 * This function finds a point in the intersection of
 * the N given open disks with center points
 * (vx[i],vy[i]) and radii r[i].
 * If no such point exists it the result is (NAN,NAN).
 * Runtime is in O(N^3). Additional used space is in O(1).
 */
int
trilateration_point_in_discs(tl_point3d *restrict result,
                             const tl_point3d *const p, const double *const r,
                             const size_t N)
{
     // First check if two disks are disjoint,
     // or if one centerpoint is inside of all disks
     int centerinside = 1;
     double dist = 0.0;

     for(size_t i = 0; i < N; i++) {
	  centerinside = 1;
	  for(size_t j = 0; j < N; j++) {
	       if(i == j) continue;
	       dist = trilateration_distance_squared(X(p, i), Y(p, i),
						     X(p, j), Y(p, j));
	       // If and only if  
	       if(dist >= r[i] * r[i]+ 2 * r[i] * r[j]  + r[j] * r[j]) {
                    tl_point3d_set(result, NAN, NAN, NAN);
		    continue;
	       }
	       // If d(A_i,A_j) >= r_j, then
	       // A_i is not in B_j.
	       if(dist >= r[j] * r[j]) {
		    centerinside = 0;
	       }
	  }
	  // return the centerpoint if it
	  // already is in I
	  if(centerinside) {
               tl_point3d_set(result, X(p, i), Y(p, i), 0.0);
	       continue;
	  }
     }

     // From here on the two conditions before the upper
     // loop should be false. Then also no disk can be completely
     // inside all others.
     // Therefore 2 intersection points of the circles
     // are in all other closed disks iff and only if
     // the intersection of all open disks is not empty.
	
     float resultx[2], resulty[2];
     tl_point3d res[2];
     size_t found = 0;
     char insideflag;

     // Loop over each pair of circles.
     for (size_t i = 1; i < N; i++) {
	  for(size_t j = 0; j < i; j++) {
	       // find the intersection point/points
	       int num = trilateration_circle_intersections(res, p + i, r[i],
							    p + j, r[j]);
	       if(num == 0) {
		    /* One circle is inside of the other or they don't
		       intersect. */
		    continue;
	       }

	       /* check if the intersection point is inside of all other
		  closed disks */
	       for (int m = 0; m < num; m++) {
		    insideflag = 1;
		    for(size_t k = 0; k < N; k++) {
			 /* skip the circles from the outer loop */
			 if(k == i || k == j) {
			      continue;
			 }
			 dist = trilateration_distance(X(p, k), Y(p, k),
						       X(res, m), Y(res, m));
			 if(dist > r[k]) {
			      insideflag = 0;
			      break;
			 }
		    }

		    if(insideflag) {
			 // Arriving here, we have found a point.
			 // Write the result to the appropriate index.
			 resultx[found] = X(res, m);
			 resulty[found] = Y(res, m);

			 if(found == 1) {
			      double d;
			      d = trilateration_distance_squared(resultx[0],
								 resulty[0],
								 resultx[1],
								 resulty[1]);
                              if (d > 1e-5) {
                                   found++;
			      }
			 } else {
			      found++;
			 }
			 if (found == 2) {
                              goto end;
			 }
		    }
	       }
	  }
	  /* At this point, we have determined that there are not intersection
	     two points. Thus, the two circles don't intersect. */
	  continue;

	  // Here we found two according points.
	  // Then we construct a convex combination
	  // and return.
     end:
          tl_point3d_set(result, 0.5 * resultx[0] + 0.5 * resultx[1],
                         0.5 * resulty[0] + 0.5 * resulty[1], 0.0);
          assert (trilateration_point_in_discs_p(result, p, r, N));
     }
     return !(isnan(RX) || isnan(RY));
}
