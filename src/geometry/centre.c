/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdlib.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>



/*!
 * Calculate the centre of mass of a collection of points.
 *
 * \param ax X coordinates of the points.
 * \param ay Y coordinates of the points.
 * \param N the length of the arrays \c ax and \c ay.
 * \param rx X coordinate of the centre of mass.
 * \param ry Y coordinate of the centre of mass.
 *
 * \todo Not stable.
 */
int
trilateration_centre_of_mass(tl_point3d *restrict result,
                             const tl_point3d *const a, const size_t N)
{
     assert (result != NULL && a != NULL);

     if (N < 1) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return -1;
     }

     double M_x = 0.0, M_y = 0.0, n = 0.0;
     for (size_t k = 0; k < N; k++) {
	  n += 1.0;
	  M_x += (X(a, k) - M_x) / n;
	  M_y += (Y(a, k) - M_y) / n;
     }
     tl_point3d_set(result, M_x, M_y, 0.0);
     return 0;
}





/*!
 * Calculate the centre of mass of a collection of points.
 *
 * \param ax X coordinates of the points.
 * \param ay Y coordinates of the points.
 * \param N the length of the arrays \c ax and \c ay.
 * \param rx X coordinate of the centre of mass.
 * \param ry Y coordinate of the centre of mass.
 *
 * \todo Not stable.
 */
int
trilateration_centre_of_mass_3d(tl_point3d *restrict result,
                                const tl_point3d *const a, const size_t N)
{
     assert (result != NULL && a != NULL);

     if (N < 1) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return -1;
     }

     double M_x = 0.0, M_y = 0.0, M_z = 0.0, n = 0.0;
     for (size_t k = 0; k < N; k++) {
	  n += 1.0;
	  M_x += (X(a, k) - M_x) / n;
	  M_y += (Y(a, k) - M_y) / n;
	  M_z += (Z(a, k) - M_z) / n;
     }
     tl_point3d_set(result, M_x, M_y, M_z);
     return 0;
}





/*!
 * Calculate the weighted centre of mass of a collection of points.
 *
 * \param ax X coordinates of the points.
 * \param ay Y coordinates of the points.
 * \param w weight of the coordinate.
 * \param N the length of the arrays \c ax and \c ay.
 * \param rx X coordinate of the centre of mass.
 * \param ry Y coordinate of the centre of mass.
 *
 * \todo May overflow. Not stable.
 */
int
trilateration_weighted_centre_of_mass(tl_point3d *restrict result,
                                      const tl_point3d *const a,
				      const double *const w, const size_t N)
{
     assert (result != NULL);

     if (N == 0) {
	  tl_point3d_set(result, NAN, NAN, NAN);
	  return -1;
     }

     assert (a != NULL && w != NULL);

     double tx = 0.0, ty = 0.0, tw = 0.0;
     for (size_t i = 0; i < N; i++) {
	  tx += w[i] * X(a, i);
	  ty += w[i] * Y(a, i);
	  tw += w[i];
     }
     if (fabs(tw) > 0.0) {
          tl_point3d_set(result, tx / tw, ty / tw, 0.0);
          return 0;
     } else {
          tl_point3d_set(result, NAN, NAN, 0.0);
          return -1;
     }
}





/*!
 * Calculate the weighted centre of mass of a collection of points.
 *
 * \param ax X coordinates of the points.
 * \param ay Y coordinates of the points.
 * \param w weight of the coordinate.
 * \param N the length of the arrays \c ax and \c ay.
 * \param rx X coordinate of the centre of mass.
 * \param ry Y coordinate of the centre of mass.
 *
 * \todo May overflow. Not stable.
 */
int
trilateration_weighted_centre_of_mass_3d(tl_point3d *restrict result,
                                         const tl_point3d *const a,
				         const double *const w,
                                         const size_t N)
{
     assert (result != NULL);

     if (N == 0) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return -1;
     }

     assert (a != NULL && w != NULL);

     double tx = 0.0, ty = 0.0, tz = 0.0, tw = 0.0;
     for (size_t i = 0; i < N; i++) {
	  tx += w[i] * X(a, i);
	  ty += w[i] * Y(a, i);
	  tz += w[i] * Z(a, i);
	  tw += w[i];
     }
     tl_point3d_set(result, tx / tw, ty / tw, tz / tw);
     return 0;
}
