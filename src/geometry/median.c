/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>

static inline int
weiszfeld_optimum_p(const tl_point3d *restrict t, const double tw,
		    const tl_point3d *p, const double *w, const size_t N)
{
     double sum_x = 0.0, sum_y = 0.0;
     for (size_t i = 0; i < N; i++) {
	  double dist = trilateration_distance(X(t, 0), Y(t, 0),
                                               X(p, i), Y(p, i));
	  if (fabs(dist) > 1e-5) {
	       sum_x += w[i] * ((X(t, 0) - X(p, i)) / dist);
	       sum_y += w[i] * ((Y(t, 0) - Y(p, i)) / dist);
	  }
     }
     return hypot(sum_x, sum_y) <= tw;
}



static inline int
weiszfeld_optimum_3d_p(const tl_point3d *const t, const double tw,
                       const tl_point3d *const p, const double *const w,
                       const size_t N)
{
     double sum_x = 0.0, sum_y = 0.0, sum_z = 0.0;
     for (size_t i = 0; i < N; i++) {
	  double dist = trilateration_distance_3d(t, p + 3 * i);
	  if ((fabs(dist) > 1e-5)) {
	       sum_x += w[i] * ((X(t, 0) - X(p, i)) / dist);
	       sum_y += w[i] * ((Y(t, 0) - Y(p, i)) / dist);
	       sum_z += w[i] * ((Z(t, 0) - Z(p, i)) / dist);
	  }
     }

     /* Calculate the hypothenuse of the sums. */
     double m = fmax(sum_x, fmax(sum_y, sum_z));
     sum_x /= m;
     sum_y /= m;
     sum_z /= m;
     double h = m * sqrt(sum_x * sum_x + sum_y * sum_y + sum_z * sum_z);
     return h <= tw;
}



static inline double
weiszfeld_distance_sum(const tl_point3d *const p,
                       const tl_point3d *const t,
		       const size_t N, const double epsilon)
{
     double result = 0.0;
     for (size_t i = 0; i < N; i++) {
	  const double x = X(p, 0) - X(t, i);
	  const double y = Y(p, 0) - Y(t, i);
	  result += sqrt(x * x + y * y + epsilon);
     }
     return result;
}



static inline double
weiszfeld_distance_sum_3d(const tl_point3d *const p,
                          const tl_point3d *const t,
                          const size_t N, const double epsilon)
{
     double result = 0;
     for (size_t i = 0; i < N; i++) {
	  double x = X(p, 0) - X(t, i);
	  double y = Y(p, 0) - Y(t, i);
	  double z = Z(p, 0) - Z(t, i);
          const double m = fmax(x, fmax(y, z));
          x /= m;
          y /= m;
          z /= m;
	  result += m * sqrt(x * x + y * y + z * z + epsilon);
     }
     return result;
}



int
trilateration_weighted_geometric_median(tl_point3d *restrict result,
					const tl_point3d *const p,
					const double *const w, const size_t N)
{
     const double epsilon = 1e-5;
     const double hyperbola_epsilon = 1e-3;

     for (size_t i = 0; i < N; i++) {
	  if (weiszfeld_optimum_p(p + i, w[i], p, w, N)) {
               tl_point3d_set(result, X(p, i), Y(p, i), 0.0);
	       return 0;
	  }
     }

     double e0, e1;
     tl_point3d c[1], t[1];

     trilateration_weighted_centre_of_mass(t, p, w, N);
     e1 = weiszfeld_distance_sum(t, p, N, hyperbola_epsilon);

     int iterations = 0;
     do {
	  X(c, 0) = X(t, 0);
	  Y(c, 0) = Y(t, 0);
	  X(t, 0) = Y(t, 0) = 0.0;
	  double id = 0.0;
	  for (size_t i = 0; i < N; i++) {
	       const double dist = trilateration_distance(X(c, 0), Y(c, 0),
                                                          X(p, i), Y(p, i));
               if (dist > 0) {
                    X(t, 0) += w[i] * (X(p, i) / dist);
                    Y(t, 0) += w[i] * (Y(p, i) / dist);
                    id += w[i] / dist;
               }
	  }
	  X(t, 0) /= id;
	  Y(t, 0) /= id;
	  e0 = e1;
	  e1 = weiszfeld_distance_sum(t, p, N, hyperbola_epsilon);
	  iterations++;
     } while ((e1 < e0) && ((e0 - e1) / e0 >= epsilon) && (iterations <= 100));

     assert(!isnan(X(c, 0)) && !isnan(Y(c, 0)));
     tl_point3d_set(result, X(c, 0), Y(c, 0), 0.0);
     return 0;
}



int
trilateration_weighted_geometric_median_3d(tl_point3d *restrict result,
					   const tl_point3d *const p,
					   const double *const w,
                                           const size_t N)
{
     const double epsilon = 1e-5;
     const double hyperbola_epsilon = 1e-3;

     for (size_t i = 0; i < N; i++) {
	  if (weiszfeld_optimum_3d_p(p + i, w[i], p, w, N)) {
	       tl_point3d_set(result, X(p, i), Y(p, i), Z(p, i));
	       return 0;
	  }
     }

     double cx, cy, cz, e0, e1;
     tl_point3d t[1];
     trilateration_weighted_centre_of_mass_3d(t, p, w, N);

     e1 = weiszfeld_distance_sum_3d(t, p, N, hyperbola_epsilon);

     int iterations = 0;
     do {
	  cx = X(t, 0);
	  cy = Y(t, 0);
          cz = Z(t, 0);
	  X(t, 0) = Y(t, 0) = Z(t, 0) = 0.0;
	  double id = 0.0;
	  for (size_t i = 0; i < N; i++) {
	       const double dist =
                    trilateration_distance(cx, cy, X(p, i), Y(p, i));
	       X(t, 0) += w[i] * (X(p, i) / dist);
	       Y(t, 0) += w[i] * (Y(p, i) / dist);
	       Z(t, 0) += w[i] * (Z(p, i) / dist);
	       id += w[i] / dist;
	  }
	  X(t,0) /= id;
	  Y(t,0) /= id;
	  Z(t,0) /= id;
	  e0 = e1;
	  e1 = weiszfeld_distance_sum_3d(t, p, N, hyperbola_epsilon);
	  iterations++;
     } while ((e1 < e0) && ((e0 - e1) / e0 >= epsilon) && (iterations <= 100));

     tl_point3d_set(result, cx, cy, cz);
     return 0;
}



int
trilateration_geometric_median(tl_point3d *restrict result,
                               const tl_point3d *const p, const size_t N)
{
     double w[N];
     for (size_t i = 0; i < N; i++) {
	  w[i] = 1.0;
     }
     return trilateration_weighted_geometric_median(result, p, w, N);
}



int
trilateration_geometric_median_3d(tl_point3d *restrict result,
                                  const tl_point3d *const p, const size_t N)
{
     double w[N];
     for (size_t i = 0; i < N; i++) {
	  w[i] = 1.0;
     }
     return trilateration_weighted_geometric_median_3d(result, p, w, N);
}
