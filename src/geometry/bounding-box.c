/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>

#define SWAP(x, y) do { double t = x; x = y; y = t; } while (0)

/*
 * Given a set of points (ax,ay) with distances d, calculate a bounding box
 * that corresponds to the smallest intersection area.
 */
int
trilateration_bounding_box_2d(tl_point3d *restrict result,
                              const tl_point3d *const a,
			      const double *const d, const size_t N)
{
     assert (result != NULL);
     if (N > 0) {
	  assert (a != NULL && d != NULL);
	  double north = a[0].y - d[0], east = a[0].x + d[0],
	       south = a[0].y + d[0], west = a[0].x - d[0];

	  for (size_t i = 1; i < N; i++) {
	       assert (d[i] >= 0.0);
	       north = fmax(north, Y(a, i) - d[i]);
	       east = fmin(east, X(a, i) + d[i]);
	       south = fmin(south, Y(a, i) + d[i]);
	       west = fmax(west, X(a, i) - d[i]);
	  }
	  if (south < north) {
	       SWAP(north, south);
	  }
	  if (east < west) {
	       SWAP(west, east);
	  }
	  X(result, 0) = X(result, 2) = west;
	  X(result, 1) = X(result, 3) = east;
	  Y(result, 0) = Y(result, 1) = north;
	  Y(result, 2) = Y(result, 3) = south;
	  Z(result, 0) = Z(result, 1) = Z(result, 2) = Z(result, 3) = 0.0;
	  return 0;
     } else {
	  X(result, 0) = X(result, 2) = -INFINITY;
	  X(result, 1) = X(result, 3) = INFINITY;
	  Y(result, 0) = Y(result, 1) = -INFINITY;
	  Y(result, 2) = Y(result, 3) = INFINITY;
	  Z(result, 0) = Z(result, 1) = Z(result, 2) = Z(result, 3) = 0.0;
	  return -1;
     }
}


/*
 * Given a set of points (ax,ay) with distances d, calculate a
 * bounding cube that corresponds to the smallest intersection volume.
 *
 * 0: NWT, 1: NET, 2: SET, 3: SWT, 4: SWB, 5: NWB, 6: NEB, 7: SEB
 */
int
trilateration_bounding_box_3d(tl_point3d *restrict result,
                              const tl_point3d *const a,
			      const double *const d, const size_t N)
{
     assert (result != NULL);
     if (N > 0) {
	  assert (a != NULL && d != NULL);
	  double north = Y(a, 0) - d[0], east = X(a, 0) + d[0],
	       south = Y(a, 0) + d[0], west = X(a, 0) - d[0],
               bottom = Z(a, 0) - d[0], top = Z(a, 0) + d[0];

	  for (size_t i = 1; i < N; i++) {
	       assert (d[i] >= 0.0);
	       north = fmax(north, Y(a, i) - d[i]);
	       east = fmin(east, X(a, i) + d[i]);
	       south = fmin(south, Y(a, i) + d[i]);
	       west = fmax(west, X(a, i) - d[i]);
	       top = fmin(top, Z(a, i) + d[i]);
	       bottom = fmax(bottom, Z(a, i) - d[i]);
	  }
	  if (south < north) {
	       SWAP(north, south);
	  }
	  if (east < west) {
	       SWAP(west, east);
	  }
	  if (top < bottom) {
	       SWAP(bottom, top);
	  }
          /* 0: NWT, 1: NET, 2: SET, 3: SWT, 4: SWB, 5: NWB, 6: NEB, 7: SEB */
	  X(result, 0) = X(result, 3) = X(result, 4) = X(result, 5) = west;
	  X(result, 1) = X(result, 2) = X(result, 6) = X(result, 7) = east;
	  Y(result, 0) = Y(result, 1) = Y(result, 5) = Y(result, 6) = north;
	  Y(result, 2) = Y(result, 3) = Y(result, 4) = Y(result, 7) = south;
	  Z(result, 0) = Z(result, 1) = Z(result, 3) = Z(result, 4) = bottom;
	  Z(result, 4) = Z(result, 5) = Z(result, 6) = Z(result, 7) = top;
	  return 0;
     } else {
          /* 0: NWT, 1: NET, 2: SET, 3: SWT, 4: SWB, 5: NWB, 6: NEB, 7: SEB */
	  X(result, 0) = X(result, 3) = X(result, 4) = X(result, 5) = -INFINITY;
	  X(result, 1) = X(result, 2) = X(result, 6) = X(result, 7) = INFINITY;
	  Y(result, 0) = Y(result, 1) = Y(result, 5) = Y(result, 6) = -INFINITY;
	  Y(result, 2) = Y(result, 3) = Y(result, 4) = Y(result, 7) = INFINITY;
	  Z(result, 0) = Z(result, 1) = Z(result, 3) = Z(result, 4) = -INFINITY;
	  Z(result, 4) = Z(result, 5) = Z(result, 6) = Z(result, 7) = INFINITY;
	  return -1;
     }
}
