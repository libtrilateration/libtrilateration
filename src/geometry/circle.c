/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>


/*!
 * Test whether a point is on a circle.
 *
 * \param p coordinate of point to test.
 * \param c The circle's center coordinate.
 * \param r The circle's radius.
 */
int
trilateration_point_on_circle_p(const tl_point3d *const p,
                                const tl_point3d *const c, double r)
{
     const double d = trilateration_distance(p->x, p->y, c->x, c->y);
     return fabs(d - r) < 1e-5;
}



/*!
 * Calculate all intersections between a line segment and a circle.
 *
 * \param cx The circle's center X coordinate.
 * \param cy The circle's center Y coordinate.
 * \param cr The circle's radius.
 * \param x1 X coordinate of first point on line.
 * \param y1 Y coordinate of first point on line.
 * \param x2 X coordinate of second point on line.
 * \param y2 Y coordinate of second point on line.
 * \param resx An array of the resulting x coordinates. Should be an array of
 * at least 2 elements.
 * \param resy An array of the resulting y coordinates. Should be an array of
 * at least 2 elements.
 *
 * \returns The number of intersections (0, 1, or 2) or a negative
 * number on error.
 */
int
trilateration_circle_line_intersections(tl_point3d *restrict result,
                                        const tl_point3d *const p, double r,
                                        const tl_point3d *const p1,
                                        const tl_point3d *const p2)
{
     int res = 0;
     const double dx = p1->x - p2->x, dy = p1->y - p2->y;
     const double fx = p1->x - p->x, fy = p1->y - p->y;
     const double a = dx * dx + dy * dy;
     const double b = 2 * (fx * dx + fy * dy);
     const double c = (fx * fx + fy * fy) - r * r;
     double d = b * b - 4.0 * a * c;

     if (0.0 > d || 1e-5 > fabs(a)) {
	  /* There is no intersection. */
	  return res;
     }

     d = sqrt(d);
     double t = (-b + d) / (2.0 * a);

     if (0.0 <= t && 1.0 >= t) {
	  /* t is an intersection. */
	  result[res].x = p1->x + t * dx;
	  result[res].y = p1->y + t * dy;
	  assert (trilateration_point_on_circle_p(result + res, p, r));
	  res++;
     }

     if (1e-5 > d) {
	  return res;
     }

     t = (-b - d) / (2.0 * a);
     if (0.0 <= t && 1.0 >= t) {
	  /* t is an intersection. */
	  result[res].x = p1->x + t * dx;
	  result[res].y = p1->y + t * dy;
	  assert (trilateration_point_on_circle_p(result + res, p, r));
	  res++;
     }
     return res;
}




/*!
 * Calculate all intersections between circles
 *
 * \param p0x The first circle's center X coordinate.
 * \param p0y The first circle's center Y coordinate.
 * \param r0 The first circle's radius.
 * \param p1x The second circle's center X coordinate.
 * \param p1y The second circle's center Y coordinate.
 * \param r1 The second circle's radius.
 * \param resx An array of the resulting x coordinates. Should be an array of
 * at least 2 elements.
 * \param resy An array of the resulting y coordinates. Should be an array of
 * at least 2 elements.
 *
 * \returns The number of intersections (0, 1, or 2) or a negative
 * number on error.
 */
int
trilateration_circle_intersections(tl_point3d *restrict result,
                                   const tl_point3d *const p1, const double r1,
				   const tl_point3d *const p2, const double r2)
{
     double dx = X(p2, 0) - X(p1, 0), dy = Y(p2, 0) - Y(p1, 0);
     double d = hypot(dx, dy);

     if (r1 + r2 < d || fabs(r1 - r2) > d || fabs(d) < 1e-5) {
	  /* The circles are separate, or coincident, or one circle is
	     contained in the other. */
	  return 0;
     }

     double a = (r1 * r1 - r2 * r2 + d * d) / (2.0 * d);
     double h = sqrt(r1 * r1 - a * a);

     double p2x = X(p1, 0) + (dx * a / d);
     double p2y = Y(p1, 0) + (dy * a / d);

     int count = (fabs(h) < 1e-5) ? 1 : 2;

     double rx = dy * h / d;
     double ry = dx * h / d;

     tl_point3d_set(result, p2x - rx, p2y + ry, 0.0);

     if (count == 2) {
          tl_point3d_set(result + 1, p2x + rx, p2y - ry, 0.0);
     }

     return count;
}





/*!
 * Calculate all intersections between circles
 *
 * \param px[] An array of X coordinate of circle's centre.
 * \param py[] An array of Y coordinate of circle's centre.
 * \param r[]  An array of circle radii.
 * \param N The number of circles.
 * \param resx[] Array of X coordinates of intersection points. The
 * size of this array must be at least N * (N - 1) elements.
 * \param resy[] Array of Y coordinates of intersection points. The
 * size of this array must be at least N * (N - 1) elements.
 * \param N The maximum number of expected intersection points.
 *
 * \returns The number of intersections found.
 */
int
trilateration_circles_intersections(tl_point3d *restrict result, const size_t K,
                                    const tl_point3d *const p,
                                    const double *const r,
                                    const size_t N)
{
     size_t i, j, k = 0;
     assert (N * (N - 1) <= K);
     for (i = 1; i < N; i++) {
	  for (j = 0; j < i; j++) {
	       assert (k < N * (N - 1));
               k += trilateration_circle_intersections(result + k,
                                                       p + j, r[j],
                                                       p + i, r[i]);
	  }
     }
     assert (k <= K);
     if (K) { /* silence warning */ }
     return k;
}





/*!
 * Returns an approximated intersection of the two circles.
 *
 * This method might be helpful if the circles are separate or contained
 * within the other. Returns the intersection of the two circles when
 * equally growing both circles till they intersect in one point.
 *
 * \param p1 The center of the first circle.
 * \param r1 The radius of the first circle.
 * \param p2 The center of the second circle.
 * \param r2 The radius of the second circle.
 *
 * \returns An approximated intersection of the two circles or
 *          \c null if p1 equals p2.
 */
int
trilateration_circle_approximate_intersection(tl_point3d *restrict result,
                                              const tl_point3d *const p1,
                                              const double r1,
                                              const tl_point3d *const p2,
                                              const double r2)
{
     double d = trilateration_distance(X(p1, 0), Y(p1, 0), X(p2, 0), Y(p2, 0));

     if (fabs(d) < 1e-5) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return 0;
     }

     double dr1 = r1 / d;
     double dr2 = r2 / d;
     double dx = X(p2, 0) - X(p1, 0);
     double dy = Y(p2, 0) - Y(p1, 0);
     double dxp1 = dr1 * dx;
     double dyp1 = dr1 * dy;
     double dxp2 = dr2 * dx;
     double dyp2 = dr2 * dy;
     
     double p11x = X(p1, 0) + dxp1;
     double p11y = Y(p1, 0) + dyp1;
     double p12x = X(p1, 0) - dxp1;
     double p12y = Y(p1, 0) - dyp1;
     double p21x = X(p2, 0) + dxp2;
     double p21y = Y(p2, 0) + dyp2;
     double p22x = X(p2, 0) - dxp2;
     double p22y = Y(p2, 0) - dyp2;

     /* find nearest pair of intersection points belonging to different
	circles */
     double d1 = trilateration_distance(p11x,p11y,p21x,p21y);
     double n1x = p11x;
     double n1y = p11y;
     double n2x = p21x;
     double n2y = p21y;
     
     double dt = trilateration_distance(p11x, p11y, p22x, p22y);
     if (dt < d1) {
	  d1 = dt;
	  n2x = p22x;
	  n2y = p22y;
     }
     dt = trilateration_distance(p12x, p12y, p21x, p21y);
     if (dt < d1) {
	  d1 = dt;
	  n1x = p12x;
	  n1y = p12y;
	  n2x = p21x;
	  n2y = p21y;
     }
     dt = trilateration_distance(p12x, p12y, p22x, p22y);
     if (dt < d1) {
	  n1x = p12x;
	  n1y = p12y;
	  n2x = p22x;
	  n2y = p22y;
     }

     // return middle of line between two nearest points as result
     tl_point3d_set(result, 0.5 * n1x + 0.5 * n2x, 0.5 * n1y + 0.5 * n2y, 0.0);
     return 1;
}




/*!
 * Approximate a point between two circles when there is no
 * intersection.
 *
 * This is from "A Low-Complexity Geometric Bilateration Method for
 * Localization in Wireless Sensor Networks and Its Comparison with
 * Least-Squares Methods"
 *
 * \param px[] An array of X coordinate of circle's centre.
 * \param py[] An array of Y coordinate of circle's centre.
 * \param r[]  An array of circle radii.
 * \param N The number of circles.
 * \param resx[] Array of X coordinates of intersection points. The
 * size of this array must be at least N * (N - 1) elements.
 * \param resy[] Array of Y coordinates of intersection points. The
 * size of this array must be at least N * (N - 1) elements.
 *
 * \returns The number of intersections (0, 1, or 2) or a negative
 * number on error.
 */
int
trilateration_circle_approximate(tl_point3d *restrict result,
                                 const tl_point3d *const p1, const double r1,
				 const tl_point3d *const p2, const double r2)
{
     const double d =
          trilateration_distance(X(p1, 0), Y(p1, 0), X(p2, 0), Y(p2, 0));

     /* If both centres of the circles are too close, assume an infinite number
	of solutions and abort. */
     if (fabs(d) < 1e-5) {
          tl_point3d_set(result, NAN, NAN, NAN);
	  return 0;
     }

     /* Enlarge radius in case they are zero. */
     double r1n = fabs(d - fmax(r2, 1e-3)) + 1e-3 + 1e-5;
     double r2n = fabs(d - fmax(r1, 1e-3)) + 1e-3 + 1e-5;

     /* Calculate new intersections. */
     tl_point3d i1[2], i2[2];
     int n1 = trilateration_circle_intersections(i1, p1, r1n,
                                                 p2, fmax(r2, 1e-3));
     assert (n1 > 0);
     int n2 = trilateration_circle_intersections(i2, p1, fmax(r1, 1e-3),
                                                 p2, r2n);
     assert (n2 > 0);

     if (n1 || n2) { /* silence warning */ }
     tl_point3d_set(result, X(i1, 0) * 0.5 + X(i2, 0) * 0.5,
                    Y(i1, 0) * 0.5 + Y(i2, 0) * 0.5, 0.0);
     return 1;
}
