/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>



/*!
 * Tests whether three points are collinear.
 *
 * \param x An array that contains all x coordinates.
 * \param y An array that contains all y coordinates.
 * \param N the size of the arrays \c ax, \c ay. \c N should be larger
 * than 2. Only the first three points will be tested.
 *
 * \returns 1 if they are collinear, and 0 otherwise.
 */
int
trilateration_collinear_p(const tl_point3d *restrict p, const size_t N)
{
     if (N < 3) {
	  return 0;
     }

     assert (p != NULL);

     double a = X(p, 0) * (Y(p, 1) - Y(p, 2)) + X(p, 1) * (Y(p, 2) - Y(p, 0)) +
	  X(p, 2) * (Y(p, 0) - Y(p, 1));
     return fabs(a) < 1e-5;
}
