/* -*- mode: c; c-file-style: "k&r"; -*-
 *
 * This file is part of libtrilateration
 *
 * Copyright 2014 by Marcel Kyas
 *
 * libtrilateration is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libtrilateration is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libtrilateration.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <stdlib.h>
#include <math.h>

#include <libtrilateration/types.h>
#include <libtrilateration/geometry.h>


/* Swap x and y if x is larger than y. */

#define CSWAP(x, y) do { if (x > y) { double t = y; y = x; x = t; } } while (0)

/*!
 * Calculate the square of the distance between two points.
 *
 * \param ax X coordinate of the first point.
 * \param ay Y coordinate of the first point.
 * \param bx X coordinate of the second point.
 * \param by Y coordinate of the second point.
 *
 * \returns The square of the distance between the two points.
 */
double
trilateration_distance_squared(double ax, double ay, double bx, double by)
{
     double dx = ax - bx;
     double dy = ay - by;
     return dx * dx + dy * dy;
}



/*!
 * Calculate the square of the distance between two points.
 *
 * \param ax X coordinate of the first point.
 * \param ay Y coordinate of the first point.
 * \param bx X coordinate of the second point.
 * \param by Y coordinate of the second point.
 *
 * \returns The square of the distance between the two points.
 */
double
trilateration_distance_squared_3d(const tl_point3d *const p,
                                  const tl_point3d *const q)
{
     double dx = X(p, 0) - X(q, 0);
     double dy = Y(p, 0) - Y(q, 0);
     double dz = Z(p, 0) - Z(q, 0);
     return dx * dx + dy * dy + dz * dz;
}



/*!
 * Calculate the distance between two points.
 *
 * \param ax X coordinate of the first point.
 * \param ay Y coordinate of the first point.
 * \param bx X coordinate of the second point.
 * \param by Y coordinate of the second point.
 *
 * \returns The distance between the two points.
 */
double
trilateration_distance(double ax, double ay, double bx, double by)
{
     double dx = fabs(ax - bx);
     double dy = fabs(ay - by);

#if 0
     dx = dx / dy;
     return dy * sqrt(dx * dx + 1.0);
#else
     return hypot(dx, dy);
#endif
}


/*!
 * Calculate the distance between two points.
 *
 * \param ax X coordinate of the first point.
 * \param ay Y coordinate of the first point.
 * \param bx X coordinate of the second point.
 * \param by Y coordinate of the second point.
 *
 * \returns The distance between the two points.
 */
double
trilateration_distance_3d(const tl_point3d *const p, const tl_point3d *const q)
{
     double dx = fabs(X(p, 0) - X(q, 0));
     double dy = fabs(Y(p, 0) - Y(q, 0));
     double dz = fabs(Z(p, 0) - Z(q, 0));

     CSWAP(dx, dy);
     CSWAP(dy, dz);
     CSWAP(dx, dz);
     assert(dx <= dz && dy <= dz);
     dx = dx / dz;
     dy = dy / dz;
     return dz * sqrt(dx * dx + dy * dy + 1.0);
}



/*!
 * Calculate the square residual error.
 *
 * \param ax X coordinate of the first point.
 * \param ay Y coordinate of the first point.
 * \param bx X coordinate of the second point.
 * \param by Y coordinate of the second point.
 *
 * \returns The distance between the two points.
 */
double
trilateration_square_residual_error(const tl_point3d *const a,
				    const double *const d, const size_t N,
				    const tl_point3d *const p)
{
     double result = 0.0;

     for (size_t i = 0; i < N; i++) {
	  double residual =
               trilateration_distance(X(a, i), Y(a, i), X(p, 0), Y(p, 0)) -
               d[i];
	  result += residual * residual;
     }
     return result;
}



/*!
 * Calculate the square residual error.
 *
 * \param ax X coordinate of the first point.
 * \param ay Y coordinate of the first point.
 * \param bx X coordinate of the second point.
 * \param by Y coordinate of the second point.
 *
 * \returns The distance between the two points.
 */
double
trilateration_square_residual_error_3d(const tl_point3d *const anchor,
                                       const double *const d, const size_t N,
                                       const tl_point3d *const p)
{
     double result = 0.0;

     for (size_t i = 0; i < N; i++) {
	  double residual = trilateration_distance_3d(anchor + 3 * i, p) - d[i];
	  result += residual * residual;
     }
     return result;
}
