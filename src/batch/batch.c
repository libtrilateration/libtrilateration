/*
  This file is part of libtrilateration - Batch process an experiment.

  Copyright 2015 Marcel Kyas

  libtrilateration is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  libtrilateration is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with libtrilateration.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netcdf.h>

#include "batch.h"

#include "libtrilateration/types.h"
#include "libtrilateration/geometry.h"
#include "libtrilateration/algorithms.h"


struct algorithm_t {
        char *name;
        trilateration_status (*init)(void **);
        trilateration_algorithm_t algorithm;
};


static struct algorithm_t algorithms[] = {
        { "aml", trilateration_aml_2d_init, trilateration_aml_2d },
        { "bilateration", NULL, trilateration_bilateration_2d },
        { "centroid", NULL, trilateration_centroid_2d },
        { "clurol", trilateration_clurol_2d_init, trilateration_clurol_2d },
        { "const", NULL, trilateration_const_2d },
        { "cpf", trilateration_cpf_2d_init, trilateration_cpf_2d },
        { "eminmax-w2", NULL, trilateration_eminmax_w2_2d },
        { "eminmax-w4", NULL, trilateration_eminmax_w4_2d },
        { "geo-3", NULL, trilateration_geo_3_2d },
        { "geo-n", NULL, trilateration_geo_n_2d },
        { "icla", NULL, trilateration_icla_2d },
        { "lls", NULL, trilateration_lls_2d },
        { "lms", trilateration_lms_2d_init, trilateration_lms_2d },
        { "md-minmax", NULL, trilateration_md_minmax_2d },
        { "md-minmax-abs", NULL, trilateration_md_minmax_abs_2d },
        { "minmax", NULL, trilateration_minmax_2d },
        { "mle-gamma", NULL, trilateration_mle_gamma_2d },
        { "mle-gauss", NULL, trilateration_mle_gauss_2d },
        { "nlls", NULL, trilateration_nlls_2d },
        { "rlsm", NULL, trilateration_rlsm_2d },
        { "rwgh", NULL, trilateration_rwgh_2d },
        { "trilateration", NULL, trilateration_trilateration_2d },
        { "weighted-centroid", NULL, trilateration_weighted_centroid_2d },
        { "weighted-lls", NULL, trilateration_weighted_lls_2d }
};

const size_t no_algorithms = sizeof(algorithms) / sizeof(algorithms[0]);

static void
list_algorithms(FILE *file)
{
        fprintf(stderr, "Known algorithms are: ");
        for (size_t i = 0; i < no_algorithms; i++) {
                fprintf(file, algorithms[i].name);
                if (i + 1 < no_algorithms) {
                        fprintf(file, ", ");
                }
        }
        fprintf(file, "\n");
        fflush(file);
}

static int
compare_algorithm(const void *m1, const void *m2)
{
        const struct algorithm_t *alg1 = m1;
        const struct algorithm_t *alg2 = m2;
        return strcmp(alg1->name, alg2->name);
}



void
map_algorithm(tl_point3d **result, tl_point3d *anchors, double *distances,
              size_t N, double *ground_truth, size_t M,
              trilateration_algorithm_t algorithm,
              void *arguments)
{
        double d[N];
        tl_point3d res[1];

	if (result != NULL) {
		*result = calloc(M, sizeof(tl_point3d));
	}

	// To calculate average and standard deviation (Knuth, Welford)
	double n = 0, M1 = 0.0, M2 = 0.0, delta;
	
        for (size_t i = 0; i < M; i++) {
		size_t K = 0;
                for (size_t j = 0; j < N; j++) {
			if (distances[i * N + j] >= 0.0) {
                        	d[K] = distances[i * N + j];
				K++;
			}
                }
                algorithm(res, anchors, d, K, arguments);

		if (result != NULL) {
			(*result)[i].x = res[0].x;
			(*result)[i].y = res[0].y;
			(*result)[i].z = res[0].z;
		}

                double ae = trilateration_distance(res[0].x, res[0].y,
                                                   ground_truth[i * 3 + 0],
                                                   ground_truth[i * 3 + 1]);

		if (!isnan(res[0].x) && !isnan(res[0].y)) {
			n = n + 1.0;
			delta = ae - M1;
			M1 = M1 + delta / n;
			M2 = M2 + delta * (ae - M1);
		}
        }
	fprintf(stderr, "MAE = %.2f, sigma^2 = %.2f, n = %.0f\n", M1, M2 / (n - 1.0), n);
}




int
main(int argc, char **argv)
{
	if (argc >= 3) {
                const struct algorithm_t key = {
                        .name = argv[1], .init = NULL, .algorithm = NULL
                };
                struct algorithm_t *alg =
                        bsearch(&key, algorithms, no_algorithms,
                                sizeof(struct algorithm_t), compare_algorithm);
                if (alg == NULL) {
                        fprintf(stderr, "Unknown algorithm %s\n", key.name);
                        list_algorithms(stderr);
                        exit(EXIT_FAILURE);
                }
                const char *file = argv[2];
                double *anchors, *distances, *ground_truth, *time_stamp;
                size_t M, N;
                void *params;
                if (alg->init != NULL) {
                        if (alg->init(&params) != TL_OK) {
                                fprintf(stderr,
                                        "Failed allocating parameters\n");
                                exit(EXIT_FAILURE);
                        }
                } else {
                        params = NULL;
                }
                read_netcdf(file, &anchors, &N, &distances, &M,
                            &ground_truth, &time_stamp);
		tl_point3d *result;
                map_algorithm(&result, (tl_point3d *) anchors, distances, N,
                              ground_truth, M, alg->algorithm, params);
		if (argc > 3) {
		        write_netcdf(argv[3], anchors, N, distances,
                                     ground_truth, (double *) result,
                                     time_stamp, M, "Title", alg->name);
		}
		free(anchors);
		free(distances);
		free(ground_truth);
		free(result);
                free(params);
                exit(EXIT_SUCCESS);
        } else {
                fprintf(stderr, "Usage: %s algorithm input [output]\n",
                        argv[0]);
                list_algorithms(stderr);
                exit(EXIT_FAILURE);
        }
}
