/*
  This file is part of libtrilateration - Write a log to netcdf

  Copyright 2015 Marcel Kyas

  libtrilateration is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  libtrilateration is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with libtrilateration.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#  include "libtrilateration/config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netcdf.h>

#include "batch.h"

/* The variables defined by our convention. */
static const char lv_anchor[] = "anchor";
static const char lv_range[] = "measurement";
static const char lv_time[] = "time";
static const char lv_ground_truth[] = "ground_truth";

#define ERR(e) do { int __e = (e); fprintf(stderr, "Error: %s\n", nc_strerror(__e)); return __e; } while (0)

static inline int
write_cf_names(int ncid, int varid, const char *standard_name, const char *long_name)
{
        int retval;
        if ((retval = nc_put_att_text(ncid, varid, "standard_name", strlen(standard_name), standard_name)) != 0)
                return retval;
        if ((retval = nc_put_att_text(ncid, varid, "long_name", strlen(long_name), long_name)) != 0)
                return retval;
        return NC_NOERR;
}


int
write_netcdf(const char *name, const double *A, size_t N, const double *d,
             const double *G, const double *R, const double *TS, size_t M,
             const char *title, const char *algorithm)
{
        int ncid, coord_dimid, anchor_dimid, ts_dimid;
        int anchors_varid, measurements_varid, ground_truth_varid,
            estimation_varid, ts_varid;
        int retval;

        if ((retval = nc_create(name, NC_CLOBBER, &ncid)) != 0)
                ERR(retval);

        /* Define the dimensions. */
        if ((retval = nc_def_dim(ncid, "position", 3, &coord_dimid)) != 0)
                ERR(retval);
        if ((retval = nc_def_dim(ncid, lv_anchor, N, &anchor_dimid)) != 0)
                ERR(retval);
        if ((retval = nc_def_dim(ncid, lv_time, M, &ts_dimid)) != 0)
                ERR(retval);

	/* Define the variables. */
        int dimids[2] = { anchor_dimid, coord_dimid };
        if ((retval = nc_def_var(ncid, lv_anchor, NC_DOUBLE, 2, dimids, &anchors_varid)) != 0)
                ERR(retval);
        if ((retval = write_cf_names(ncid, anchors_varid, "anchor_positions", "Position of anchor")) != 0)
                ERR(retval);
        
	const char m[] = "meter";
        if ((retval = nc_put_att_text(ncid, anchors_varid, "units", strlen(m), m)) != 0)
                ERR(retval);

        dimids[0] = ts_dimid;
        dimids[1] = anchor_dimid;
        if ((retval = nc_def_var(ncid, lv_range, NC_DOUBLE, 2, dimids, &measurements_varid)) != 0)
                ERR(retval);
        if ((retval = write_cf_names(ncid, measurements_varid, "measured_distance_from_anchor", "distance measured from anchor")) != 0)
                ERR(retval);
        if ((retval = nc_put_att_text(ncid, measurements_varid, "units", strlen(m), m)) != 0)
                ERR(retval);        
	double fill_value[] = { -1.0 };
        if ((retval = nc_put_att_double(ncid, measurements_varid, "missing_value", NC_DOUBLE, 1, fill_value)) != 0)
                ERR(retval);
	double valid_range[] = { 0.0, 130.0 };
        if ((retval = nc_put_att_double(ncid, measurements_varid, "valid_range", NC_DOUBLE, 2, valid_range)) != 0)
                ERR(retval);

        dimids[0] = ts_dimid;
        dimids[1] = coord_dimid;
        if ((retval = nc_def_var(ncid, lv_ground_truth, NC_DOUBLE, 2, dimids, &ground_truth_varid)) != 0)
		ERR(retval);
        if ((retval = write_cf_names(ncid, ground_truth_varid, "ground_truth", "ground truth position of measurements")) != 0)
                ERR(retval);
        if ((retval = nc_put_att_text(ncid, ground_truth_varid, "units", strlen(m), m)) != 0)
                ERR(retval);

        dimids[0] = ts_dimid;
        dimids[1] = coord_dimid;
        if ((retval = nc_def_var(ncid, "estimation", NC_DOUBLE, 2, dimids, &estimation_varid)) != 0)
		ERR(retval);
        if ((retval = write_cf_names(ncid, estimation_varid, "estimation", "estimated position")) != 0)
                ERR(retval);
        if ((retval = nc_put_att_text(ncid, estimation_varid, "units", strlen(m), m)) != 0)
                ERR(retval);

        dimids[0] = ts_dimid;
        if ((retval = nc_def_var(ncid, lv_time, NC_DOUBLE, 1, dimids, &ts_varid)) != 0)
		ERR(retval);
        if ((retval = write_cf_names(ncid, ts_varid, "time_stamp", "time of measurement")) != 0)
                ERR(retval);

	const char epoch[] = "seconds since 1970-01-01 00:00:00";
        if ((retval = nc_put_att_text(ncid, ts_varid, "units", strlen(epoch), epoch)) != 0)
                ERR(retval);

	/* Add some useful attributes. */
	const char conventions[] = "LatViz";
        if ((retval = nc_put_att_text(ncid, NC_GLOBAL, "Conventions",
                                      strlen(conventions), conventions)) != 0)
                ERR(retval);

        if ((retval = nc_put_att_text(ncid, NC_GLOBAL, "generator",
                                      strlen(PACKAGE_NAME), PACKAGE_NAME)) != 0)
             ERR(retval);

        if ((retval = nc_put_att_text(ncid, NC_GLOBAL, "generator-version",
                                      strlen(PACKAGE_VERSION), PACKAGE_VERSION)) != 0)
             ERR(retval);

        if ((retval = nc_put_att_text(ncid, NC_GLOBAL, "generator-url",
                                      strlen(PACKAGE_URL), PACKAGE_URL)) != 0)
             ERR(retval);
	if (title != NULL) {
                if ((retval = nc_put_att_text(ncid, NC_GLOBAL, "title",
                                              strlen(title), title)) != 0)
                        ERR(retval);
	}

	if (algorithm != NULL) {
                if ((retval = nc_put_att_text(ncid, NC_GLOBAL, "algorithm",
                                              strlen(algorithm), algorithm)) != 0)
                        ERR(retval);
	}


	/* End define mode. This tells netCDF we are done defining
	 * metadata. */
        if ((retval = nc_enddef(ncid)))
           ERR(retval);

	/* write the data. */
        if ((retval = nc_put_var_double(ncid, anchors_varid, A)) != 0)
		ERR(retval);

        if ((retval = nc_put_var_double(ncid, measurements_varid, d)) != 0)
		ERR(retval);

        if ((retval = nc_put_var_double(ncid, ground_truth_varid, G)) != 0)
		ERR(retval);

        if ((retval = nc_put_var_double(ncid, estimation_varid, R)) != 0)
		ERR(retval);

        if ((retval = nc_put_var_double(ncid, ts_varid, TS)) != 0)
		ERR(retval);

        if ((retval = nc_close(ncid)) != 0)
                ERR(retval);

	return 0;
}


/* Read a log file in netcdf format. */
int
read_netcdf(const char* name, double **anchors, size_t *N, double **distances,
            size_t *M, double **ground_truth, double **time_stamp)
{
        int ncid, varid, dimid;
        int retval;

        if ((retval = nc_open(name, NC_NOWRITE, &ncid)) != NC_NOERR) {
                ERR(retval);
        }

        if ((retval = nc_inq_dimid(ncid, lv_anchor, &dimid)) != NC_NOERR) {
                ERR(retval);
        }

        if ((N != NULL) && (retval = nc_inq_dimlen(ncid, dimid, N)) != NC_NOERR) {
                ERR(retval);
        }

        if ((retval = nc_inq_dimid(ncid, lv_time, &dimid)) != NC_NOERR) {
                ERR(retval);
        }

        if ((M != NULL) && (retval = nc_inq_dimlen(ncid, dimid, M)) != NC_NOERR) {
                ERR(retval);
        }

        if (anchors != NULL) {
                *anchors = calloc((*N) * 3, sizeof(double));
                if (*anchors == NULL) {
                        perror(NULL);
                        exit(EXIT_FAILURE);
                }
                if ((retval = nc_inq_varid(ncid, lv_anchor, &varid)) != NC_NOERR) {
                        ERR(retval);
                }
                if ((retval = nc_get_var_double(ncid, varid, *anchors)) != NC_NOERR) {
                        ERR(retval);
                }
        }

        if (distances != NULL) {
                *distances = calloc((*N) * (*M), sizeof(double));
                if (*distances == NULL) {
                        perror(NULL);
                        exit(EXIT_FAILURE);
                }
                if ((retval = nc_inq_varid(ncid, lv_range, &varid)) != NC_NOERR) {
                        ERR(retval);
                }
                if ((retval = nc_get_var_double(ncid, varid, *distances)) != NC_NOERR) {
                        ERR(retval);
                }
        }

        if (ground_truth != NULL) {
                *ground_truth = calloc((*M) * 3, sizeof(double));
                if (*ground_truth == NULL) {
                        perror(NULL);
                        exit(EXIT_FAILURE);
                }
                if ((retval = nc_inq_varid(ncid, lv_ground_truth, &varid)) != NC_NOERR) {
                        ERR(retval);
                }
                if ((retval = nc_get_var_double(ncid, varid, *ground_truth)) != NC_NOERR) {
                        ERR(retval);
                }
        }

        if (time_stamp != NULL) {
                *time_stamp = calloc((*M), sizeof(double));
                if (*time_stamp == NULL) {
                        perror(NULL);
                        exit(EXIT_FAILURE);
                }
                if ((retval = nc_inq_varid(ncid, lv_time, &varid)) != NC_NOERR) {
                        ERR(retval);
                }
                if ((retval = nc_get_var_double(ncid, varid, *time_stamp)) != NC_NOERR) {
                        ERR(retval);
                }
        }

        if ((retval = nc_close(ncid)) != NC_NOERR) {
                ERR(retval);
        }
        return NC_NOERR;
}

