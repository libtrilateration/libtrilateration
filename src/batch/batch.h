/*
  This file is part of libtrilateration - Write a log to netcdf

  Copyright 2015 Marcel Kyas

  libtrilateration is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  libtrilateration is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with libtrilateration.  If not, see <http://www.gnu.org/licenses/>.
 */


int
write_netcdf(const char *name, const double *A, size_t N, const double *d,
             const double *G, const double *R, const double *TS, size_t M,
             const char *title, const char *algorithm);

int
read_netcdf(const char* name, double **anchors, size_t *N, double **distances,
            size_t *M, double **ground_truth, double **time_stamp);
